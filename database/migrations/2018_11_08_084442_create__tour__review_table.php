<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_review', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname')->nullable(true);
            $table->Text('review')->nullable(true);
            $table->integer('Accomodation')->nullable(true);
            $table->integer('Meals')->nullable(true);
            $table->integer('Value_For_Money')->nullable(true);
            $table->integer('Destination')->nullable(true);
            $table->integer('Transport')->nullable(true);
            $table->integer('active');
            $table->integer('tour_id')->nullable()->unsigned();
            $table->foreign('tour_id')->references('id')->on('tours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_review');
    }
}
