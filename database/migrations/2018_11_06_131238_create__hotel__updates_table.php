<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels', function (Blueprint $table) {      
            $table->text('Amenities')->nullable(true);
            $table->integer('availability');
            $table->integer('adults');
            $table->integer('childern');
            $table->integer('capacity');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function ($table) {
            $table->dropColumn('Amenities');
            $table->dropColumn('availability');
            $table->dropColumn('adults');
            $table->dropColumn('childern');
            $table->dropColumn('capacity');
        });
    }
}
