<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::table('hotels', function (Blueprint $table) {
            $table->string('name')->nullable(true)->change();
            $table->text('description')->nullable(true)->change();
            $table->text('services')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('services');
        });
        
    }
}
