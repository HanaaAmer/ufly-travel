<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDetailTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tourDetail_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->string('title');                    
            $table->text('description');
            $table->unique(['tour__detail_id', 'locale']);
            $table->integer('tour__detail_id')->unsigned();
       $table->foreign('tour__detail_id')->references('id')->on('tour_details')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tourDetail_translation');
    }
}
