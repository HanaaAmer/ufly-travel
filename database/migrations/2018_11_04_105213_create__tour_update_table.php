<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->string('name')->nullable(true)->change();
            $table->text('description')->nullable(true)->change();
            $table->text('included')->nullable(true)->change();
            $table->text('not_included')->nullable(true)->change();
            $table->string('image')->nullable(true)->change();
            $table->integer('price')->nullable(true)->change();
            $table->datetime('start_date')->nullable(true)->change();
            $table->datetime('end_date')->nullable(true)->change();
            $table->integer('availability')->nullable(true)->change();
            $table->integer('duration_day')->nullable(true)->change();
            $table->integer('duration_night')->nullable(true)->change();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('included');
            $table->dropColumn('not_included');
            $table->dropColumn('image');
            $table->dropColumn('price');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('availability');
            $table->dropColumn('duration_day');
            $table->dropColumn('duration_night');
        });
    }
}
