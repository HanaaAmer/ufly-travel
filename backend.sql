-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2018 at 06:13 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.1.17-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `backend`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ads_positions`
--

CREATE TABLE `ads_positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads_positions`
--

INSERT INTO `ads_positions` (`id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'main-1', NULL, NULL),
(2, 'main-2', NULL, NULL),
(3, 'sidebar', NULL, NULL),
(4, 'inner-pages-1', NULL, NULL),
(5, 'inner-pages-2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `person_id` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT '1',
  `slider` tinyint(1) DEFAULT '0',
  `breaking` tinyint(1) DEFAULT '0',
  `keywards` text COLLATE utf8mb4_unicode_ci,
  `number_of_readings` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `person_id`, `category_id`, `image`, `active`, `slider`, `breaking`, `keywards`, `number_of_readings`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 3, 'DPx8Nn7KMW1539679325.png', 1, 0, 0, 'ff', NULL, 2, 2, '2018-10-16 06:42:05', '2018-10-16 06:47:20');

-- --------------------------------------------------------

--
-- Table structure for table `blogs_translation`
--

CREATE TABLE `blogs_translation` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `long_description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `keywards` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blogs_translation`
--

INSERT INTO `blogs_translation` (`id`, `blog_id`, `locale`, `title`, `short_description`, `long_description`, `content`, `keywards`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'ff', 'ff', 'ff', NULL, NULL, '2018-10-16 06:47:20', '2018-10-16 06:47:20'),
(2, 1, 'en', 'en blog', 'en short', 'en long', NULL, NULL, '2018-10-16 06:48:28', '2018-10-16 06:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT '1',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `description`, `active`, `parent_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'football-news', 'football-news', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(2, 'football-u23-news', 'football-u23-news', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(3, 'basketball-news', 'basketball-news', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(4, 'handball-news', 'handball-news', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(5, 'volleyball-news', 'volleyball-news', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(6, 'reports', 'reports', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(7, 'interviews', 'interviews', 1, NULL, NULL, '2018-04-01 20:06:51', '2018-04-01 20:06:51'),
(8, 'socialism', 'socialism', 1, NULL, NULL, NULL, NULL),
(9, 'journey', 'journey', 1, NULL, NULL, NULL, NULL),
(10, 'godo-news', 'godo-news', 1, NULL, NULL, NULL, NULL),
(11, 'karate-news', 'karate-news', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `championships`
--

CREATE TABLE `championships` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `years` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `championships`
--

INSERT INTO `championships` (`id`, `name`, `image`, `years`, `created_at`, `updated_at`) VALUES
(19, 'الدورى الممتاز', '9gTZzYWY9c1522851254.png', '1963', '2018-04-04 19:14:14', '2018-04-04 19:14:14'),
(17, 'الدورى الممتاز', 'UniRXVVlCd1522851222.png', '1962', '2018-04-04 19:13:42', '2018-04-04 19:13:42'),
(16, 'كأس مصر', 'idg1K27AMn1522851179.png', '1954', '2018-04-04 19:12:59', '2018-04-04 19:12:59'),
(15, 'دوري منطقة القاهرة', 'Yphp4R8G6A1522851127.png', '1933', '2018-04-04 19:12:07', '2018-04-04 19:12:07'),
(13, 'كأس مصر', 'XeGSVkNMS41522851042.png', '1923', '2018-04-04 19:10:42', '2018-04-04 19:10:42'),
(14, 'كأس مصر', 'nKo8Yg8yYw1522851093.png', '1929', '2018-04-04 19:11:33', '2018-04-04 19:11:33'),
(20, 'كأس مصر', 'Yh93KmMieQ1522851286.png', '1965', '2018-04-04 19:14:46', '2018-04-04 19:14:46'),
(22, 'كأس مصر', '7TlG6wxKlA1522851316.png', '1967', '2018-04-04 19:15:16', '2018-04-04 19:15:16'),
(23, 'كأس مصر', '6SNn9d480A1522851353.png', '1986', '2018-04-04 19:15:53', '2018-04-04 19:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` text COLLATE utf8mb4_unicode_ci,
  `external_link` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`contact_id`, `name`, `email`, `phone`, `address`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Safwat Rabea', 'safwat@mahacode.com', '1224938803', NULL, 'test meesage', '2018-06-01 17:57:09', '2018-06-01 17:57:09');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `year` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `year`, `content`, `created_at`, `updated_at`) VALUES
(1, 0, '<p>تأسس النادي في عام 1921 كواحد من أوائل النوادي الرياضيه في مصر ليبدأ نشاطه في كرة القدم في نفس الموسم، ويحصل النادي علي أول بطوله في تاريخه عندما فاز ببطولة كأس مصر 1923 ليصبح ثاني نادي مصري يفوز بتلك الكأس بعد الزمالك .وكرر الترسانه إنجازه مره أخري بعد مرور 6 سنوات بعد فوزه بالكأس للمره الثانيه في العام 1929 ومن ثم بدأ النادي في الاشتراك ببطولة الدوري الممتاز منذ نشاته عام 1948 وحصل الترسانه علي المركز الثاني وراء الأهلي بعد منافسه ثنائيه بينهما وفاز الفريق مره اخري ببطولة كأس مصر عام 1954 ليحقق الثلاثيه ببطولة كأس مصر الجيل الذهبي كان الفريق دائما في قلب المنافسه علي البطوله وكان دائما في المربع الذهبي وأحيانا في المركز الثاني والثالث حتي تحقق حلمه وحقق الإنجاز بالفوز بأول بطولة دوري في تاريخه موسم 1962/1963 بقيادة هداف الفريق ونجمه الأول حسن الشاذلي ومصطفي رياض ومحمد رياض وبدوي عبد الفتاح</p>\r\n\r\n<p>فاز الترسانة بكأس مصر 6 مرات أعوام (1923-1929-1954-1965-1967-1986) ، بينما وصل للنهائي 5 مرات أعوام (1938-1950-1956-1966) لكنه لم يحالفه الحظ في الفوز بها .. وارتبط نادي الترسانة بالفوز في النهائي على أندية الزمالك والمصري بنتيجة (4-1) في نهائي كأس مصر وهي تعد أكبر نتيجة للترسانة في مشاركته في نهائي كأس مصر .. بينما تعد نتيجة مع النادي الاهلى في نهائي كأس مصر عام 1950 بالهزيمة (6-صفر) هي أكبر هزيمة في مشاركته في كأس مصر ، بينما كان أكبر فوز أحرزه نادي الترسانة على النادي الاهلى كان موسم 1953-1954 وكانت بنتيجة ثقيلة (6-2) ، وأيضاً في موسم 60-61 كرر نادي الترسانة فوزه على النادي الاهلى بنتيجة (5-1) ، أى أن نادي الترسانة كان بعبع الاهلى والزمالك في الخمسينيات والستينيات.</p>\r\n\r\n<p>كثيراً ما ينبذ لنا نادي الترسانة لاعبين فذاذ ، كما خرج لنا من قبل الاسطورتين حسن الشاذلي ومصطفى رياض فالان يخرج لنا بلاعبين هما الابرز وهم من الجيل الحالي فقد خرج علينا نادي الترسانة بلاعب خط وسط برز كثيراً في صفوفه وهو محمد عبدالواحد لاعب الترسانة والزمالك السابقين والاسماعيلي الحالي فقد لمع كثيراً ضمن صفوفه إلى أن اتجه إلى اللعب لنادي الزمالك في صفقه رائعة لنادي الزمالك ثم بعدها اتجه للنادي الاسماعيلي وهو يلمع بين صفوفه حتى الان، ثم نتجه الان بلاعب تألق كثيراً وصال وجال في ملاعبنا المصرية رغم صغر سنه وهو من اللاعبين الذين لن ولم تنساهم الجماهير المصرية أبدا فهو لاعب ذو خلق عالي ومتدين ومخلص لفانلة نادي .. فهو النجم محمد أبو تريكه الذي انتقل بموجبه للنادي الاهلى بينما كان على اعتاب الانتقال لنادي الزمالك .. لكن يستطيع النادي الاهلى للحصول على خدماته .. فتألق بين صفوفه وأحرز بطولات كبيرة وكثيرة مع النادي الاهلى بل وصعد لكأس العالم للأندية والتي أقيمت في اليابان وهو ضمن صفوفه وأحرز هداف الدوري المصري العام الماضي .. وأيضاً كان صاحب الضربة الاخيرة وضربة الفوز بكأس الأمم الأفريقية لعام 2006 مع المنتخب الوطني .</p>\r\n\r\n<p>&nbsp;</p>', NULL, '2018-04-10 14:46:06'),
(2, 2001, 'تأسس النادي في عام 1921 كواحد من أوائل النوادي الرياضيه في مصر ليبدأ نشاطه في كرة القدم في نفس الموسم، ويحصل النادي علي أول بطوله في تاريخه عندما فاز ببطولة كأس مصر 1923 ليصبح ثاني نادي مصري يفوز بتلك الكأس بعد الزمالك .وكرر الترسانه إنجازه مره أخري بعد مرور 6 سنوات بعد فوزه بالكأس للمره الثانيه في العام 1929 ومن ثم بدأ النادي في الاشتراك ببطولة الدوري الممتاز منذ نشاته عام 1948 وحصل الترسانه علي المركز الثاني وراء الأهلي بعد منافسه ثنائيه بينهما وفاز الفريق مره اخري ببطولة كأس مصر عام 1954 ليحقق الثلاثيه ببطولة كأس مصر الجيل الذهبي كان الفريق دائما في قلب المنافسه علي البطوله وكان دائما في المربع الذهبي وأحيانا في المركز الثاني والثالث حتي تحقق حلمه وحقق الإنجاز بالفوز بأول بطولة دوري في تاريخه موسم 1962/1963 بقيادة هداف الفريق ونجمه الأول حسن الشاذلي ومصطفي رياض ومحمد رياض وبدوي عبد الفتاح', '2018-04-08 13:45:07', '2018-04-10 14:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `album_title`, `images`, `created_at`, `updated_at`) VALUES
(13, 'فوز الترسانه', 'yIqtQbSJXc1526484241.png', '2018-05-15 16:40:08', '2018-05-16 20:24:01'),
(12, 'نادى ترسانة الرياضى', 'F9DAKPJaP71526383978.jpg', '2018-04-04 19:12:30', '2018-05-15 16:32:58'),
(8, 'نادى ترسانة الرياضى', 'YF9iMa0Wbc1522850976.jpg', '2018-04-04 19:09:36', '2018-04-04 19:09:36'),
(9, 'نادى ترسانة الرياضى', 'oLSdhe7pm41522851030.jpg', '2018-04-04 19:10:31', '2018-04-04 19:10:31'),
(10, 'نادى ترسانة الرياضى', 'fZlt8ANrKf1522851071.jpg', '2018-04-04 19:11:11', '2018-04-04 19:11:11'),
(11, 'نادى ترسانة الرياضى', 'xnsAV4T2RV1522851099.jpg', '2018-04-04 19:11:39', '2018-04-04 19:11:39'),
(14, 'title', 'QQOffiHTF71527935038.jpg;1NhCmDr6Uz1527935038.png;wnWHUrcAH81527935038.png;QCyMvpFFCf1527935038.png;gUpn9Pz0991527935038.png', '2018-06-02 15:23:58', '2018-06-02 15:23:58'),
(15, 'title', 'sw0Tdnchz71527935042.jpg;quQiATTJX01527935042.png;4xyTv2rnVk1527935042.png', '2018-06-02 15:24:02', '2018-06-02 15:24:02'),
(17, 'البوم تجريبي #5', '5vDRduM5Sd1528104647.png;J44G2XyMSu1528104647.png', '2018-06-04 13:34:40', '2018-06-04 14:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'top manager', NULL, NULL),
(2, 'manager', NULL, NULL),
(3, 'employee', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `link_section_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`link_id`, `link_section_id`, `title`, `route`, `active`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'home', 'getFrontHome', 1, NULL, NULL, NULL),
(2, 1, 'services', 'getFrontServices', 1, NULL, NULL, NULL),
(3, 1, 'blog', 'getFrontBlog', 1, NULL, NULL, NULL),
(4, 1, 'clients', 'getFrontClients', 1, NULL, NULL, NULL),
(5, 1, 'photos', 'getFrontPhotos', 1, NULL, NULL, NULL),
(6, 1, 'contacts', 'getContacts', 1, NULL, NULL, NULL),
(7, 2, 'مكتبة الفيديو', 'getFrontVideos', 1, NULL, '2018-05-07 18:02:32', '2018-05-07 18:02:32'),
(8, 2, 'مكتبة الصور', 'getFrontImages', 1, NULL, '2018-05-07 18:03:15', '2018-05-07 18:05:14'),
(9, 2, 'اتصل بنا', 'contactUsPage', 1, NULL, '2018-05-07 18:03:48', '2018-05-07 18:03:48');

-- --------------------------------------------------------

--
-- Table structure for table `link_sections`
--

CREATE TABLE `link_sections` (
  `link_section_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `link_sections`
--

INSERT INTO `link_sections` (`link_section_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'navbar', NULL, NULL),
(2, 'footer', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` int(10) UNSIGNED NOT NULL,
  `opposing_team` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `championship` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stadium` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `result` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `opposing_team`, `logo`, `championship`, `stadium`, `date`, `time`, `result`, `created_at`, `updated_at`) VALUES
(1, 'الاهلى', 'CFhLNjnbln1522748741.png', 'دورى', 'ستاد القاهره', '2018-02-23', '14:00:00', '0', '2018-04-03 14:45:41', '2018-04-04 19:27:38'),
(2, 'الزمالك', 'JPkoLlOGAD1523352910.jpg', 'الدورى الممتاز', 'القاهره', '2018-12-04', '14:00:00', '0-0', '2018-04-04 16:12:30', '2018-04-10 14:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2012_08_08_112606_create_user_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(35, '2017_08_08_225052_create_sidebar_items_table', 6),
(34, '2017_08_08_224302_create_sidebar_menuses_table', 6),
(6, '2017_08_12_211705_create_pages_table', 1),
(7, '2017_08_13_130548_create_service_sections_table', 1),
(8, '2017_08_13_130903_create_services_table', 1),
(9, '2017_08_14_213550_create_clients_table', 1),
(10, '2017_08_18_155345_create_trackers_table', 1),
(11, '2017_08_19_224958_create_categories_table', 1),
(12, '2017_08_19_225040_create_posts_table', 1),
(13, '2017_08_31_131050_create_sliders_table', 1),
(14, '2017_08_31_151258_create_link_sections_table', 1),
(15, '2017_08_31_151258_create_links_table', 1),
(16, '2017_09_08_195917_create_contacts_table', 1),
(31, '2017_09_15_192328_create_settings_table', 4),
(30, '2018_02_06_091604_create_ads__positions_table', 3),
(19, '2018_03_05_140819_create_ads_table', 1),
(20, '2018_03_14_130405_create_images_table', 1),
(29, '2018_03_14_130405_create_videos_table', 2),
(22, '2018_03_14_131133_create_history_table', 1),
(23, '2018_03_14_132124_create_comments_table', 1),
(24, '2018_03_17_111749_jobs', 1),
(25, '2018_03_17_114413_persons', 1),
(26, '2018_03_18_120006_sponsors', 1),
(27, '2018_03_18_124227_championships', 1),
(28, '2018_03_26_140906_matches', 1),
(32, '2018_04_15_110338_create_tags_table', 5),
(33, '2018_04_15_110606_create_tag_post_table', 5),
(36, '2018_05_14_091334_create_ticket_table', 7),
(37, '2018_05_14_132653_create-subscription-table', 7),
(38, '2018_05_15_094207_add_whatsap_to_setting_table', 8),
(39, '2018_06_02_101518_add_youtube_to_setting_table', 9),
(40, '2018_06_02_121830_add_youtube_to_setting_table', 10),
(41, '2018_06_04_143654_add_permission_to_users_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `description`, `image`, `active`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'من نحن', '<p dir="rtl"><span style="color:#299fc0"><span style="font-size:24px"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><strong>من نحن </strong></span></span></span></span></p>\n\n					<p dir="rtl"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large">أمتياز هو أول وأكبر موقع للخدمات الطلابية المتنوعة حيث نقوم بإعداد رسائل الماجستير بكافة تفاصيلها وبإحترافية عالية </span></span></span><span style="font-family:Sakkal Majalla,serif"><span style="font-size:large">, </span></span><span style="font-family:Arial"><span style="font-family:NeoSans"><span style="font-size:large">ويستهدف إمتياز الطلاب والخريجين والباحثين فى الجامعات العربية والأجنبية من خلال التعاون مع نخبة متميزة من حملة شهادة الماجستير والدكتوراة فى التخصصات المختلفة</span></span></span><span style="font-family:NeoSans"><span style="font-size:large">.</span></span></p>\n\n					<p dir="rtl">&nbsp;</p>\n\n					<p dir="rtl"><span style="font-size:24px"><span style="color:#299fc0"><span style="font-family:Arial"><span style="font-family:NeoSans"><strong>رسالتنا</strong></span></span></span></span><span style="color:#16a085"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large"><strong>&nbsp;</strong></span></span></span></span></p>\n\n					<p dir="rtl"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large">تسعى دائما إمتياز لتقديم خدمات للطلاب والباحثين ورجال الأعمال بجودة وحرفية عالية فى مجال إعداد رسائل الماجستير والأبحاث الجامعية فى كافة التخصصات وذلك من خلال نخبة متميزة من الأساتذة والمتخصصين فى مجالات الإستشارات الطلابية </span></span></span><span style="font-family:NeoSans"><span style="font-size:large">.</span></span></p>\n\n					<p dir="rtl">&nbsp;</p>\n\n					<p dir="rtl"><span style="font-size:24px"><span style="color:#299fc0"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><strong>رؤيتنا</strong></span></span></span></span></p>\n\n					<p dir="rtl"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large">أن نكون أكبر كيان متخصص بالشرق الأوسط فى مجال رسائل الماجستير والأبحاث الجامعية</span></span></span><span style="font-family:NeoSans"><span style="font-size:large">.</span></span><br />\n					&nbsp;</p>\n\n					<p dir="rtl"><span style="font-size:24px"><strong><span dir="rtl"><span style="color:#299fc0"><span style="font-family:NeoSans"><span style="font-family:NeoSans">وسائل الدفع المتاحة</span></span></span></span></strong></span></p>\n\n					<p dir="rtl"><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large">التحويل البنكى </span></span></span><span style="font-family:NeoSans"><span style="font-size:large">- </span></span><span style="font-family:NeoSans"><span style="font-family:NeoSans"><span style="font-size:large">ويسترن يونيون </span></span></span><span style="font-family:NeoSans"><span style="font-size:large">- PayPal</span></span></p>\n\n					<p dir="rtl"><br />\n					&nbsp;</p>\n					', 'aboutus.jpg', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `degree` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(10) UNSIGNED NOT NULL,
  `service_section_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT '1',
  `show_in_homepage` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_section_id`, `name`, `short_description`, `long_description`, `image`, `active`, `show_in_homepage`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, '203132tshirt.png', 1, 1, 2, '2018-10-16 18:31:32', '2018-10-16 18:31:32'),
(2, 1, NULL, NULL, NULL, '1240573264746e-2f25-49bd-965e-7bd85b881b84-original.jpeg', 1, 1, 1, '2018-10-24 10:40:57', '2018-10-24 13:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `services_translation`
--

CREATE TABLE `services_translation` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `long_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services_translation`
--

INSERT INTO `services_translation` (`id`, `service_id`, `locale`, `name`, `short_description`, `long_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'lang service', 'short desc', 'long desc', '2018-10-16 18:31:32', '2018-10-16 18:31:32'),
(2, 1, 'en', 'rrr', 'rr', 'rr', '2018-10-16 18:33:56', '2018-10-16 18:33:56'),
(3, 2, 'en', 'service english', 'dsd', '<p>sss</p>', '2018-10-24 10:40:57', '2018-10-24 13:57:23'),
(4, 2, 'ar', 'hgg', 'gg', '<p>gg</p>', '2018-10-24 10:40:57', '2018-10-24 10:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `service_sections`
--

CREATE TABLE `service_sections` (
  `service_section_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'fa fa-book',
  `active` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(10) UNSIGNED NOT NULL,
  `site_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_description` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscript_description` text COLLATE utf8mb4_unicode_ci,
  `email_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `site_title`, `site_description`, `phone`, `address`, `subscript_description`, `email_1`, `email_2`, `facebook_link`, `twitter_link`, `youtube`, `whatsapp`, `instagram_link`, `google_link`, `about`, `created_at`, `updated_at`) VALUES
(1, 'نادي الترسانة الرياضي', 'نادي الترسانة الرياضي', '0245454', 'عنوان الموقع هو ..', 'كيفية الاشتراك هى ...', 'info@tersana.com', 'support@tersana.com', 'https://www.facebook.com/AlTersanaOfficial/', '#twitter', '#youtube', '0114967170', '#instagram', '#google', 'تأسس النادي في عام 1921 كواحد من أوائل النوادي الرياضيه في مصر ليبدأ نشاطه في كرة القدم في نفس الموسم، ويحصل النادي علي أول بطوله في تاريخه عندما فاز ببطولة كأس مصر 1923 ليصبح ثاني نادي مصري يفوز بتلك الكأس بعد الزمالك .وكرر الترسانه إنجازه مره أخري بعد مرور 6 سنوات بعد فوزه بالكأس للمره الثانيه في العام 1929 ومن ثم بدأ النادي في الاشتراك ببطولة الدوري الممتاز منذ نشاته عام 1948 وحصل الترسانه علي المركز الثاني وراء الأهلي بعد منافسه ثنائيه بينهما وفاز الفريق مره اخري ببطولة كأس مصر عام 1954 ليحقق الثلاثيه ببطولة كأس مصر الجيل الذهبي كان الفريق دائما في قلب المنافسه علي البطوله وكان دائما في المربع الذهبي وأحيانا في المركز الثاني والثالث حتي تحقق حلمه وحقق الإنجاز بالفوز بأول بطولة دوري في تاريخه موسم 1962/1963 بقيادة هداف الفريق ونجمه الأول حسن الشاذلي ومصطفي رياض ومحمد رياض وبدوي عبد الفتاح', NULL, '2018-10-16 21:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_items`
--

CREATE TABLE `sidebar_items` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles_access` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sidebar_items`
--

INSERT INTO `sidebar_items` (`item_id`, `menu_id`, `name`, `route`, `roles_access`, `icon`, `ordering`, `active`, `class`, `created_at`, `updated_at`) VALUES
(1, 1, 'allUsers', 'getAllUsers', '1', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(2, 1, 'addUser', 'getAddUser', '1', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(3, 2, 'all_settings', 'getSettings', '1', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(4, 3, 'all_message', 'getAllContacts', '1', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(5, 4, 'add_ads', 'ads_management.create', '1,2,3', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(6, 4, 'all_ads', 'ads_management.index', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(7, 5, 'allPersons', 'getAllPersons', '15,', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(8, 5, 'addPerson', 'getAddPerson', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(9, 6, 'allsponsors', 'getAllSponsors', '15,', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(10, 6, 'addSponsor', 'getAddSponsor', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(11, 7, 'allChampionships', 'getAllChampionships', '1,2,3', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(12, 7, 'addChampionship', 'getAddChampionship', '1,2,3', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(13, 8, 'all_images', 'imagesIndex', '15,', 'fa fa-images', 2, 1, NULL, NULL, NULL),
(14, 8, 'add_images', 'addImages', '1,2,3', 'fa fa-images', 2, 1, NULL, NULL, NULL),
(15, 8, 'videos', 'videosIndex', '1,2,3', 'fa fa-images', 2, 1, NULL, NULL, NULL),
(16, 8, 'addVideo', 'addVideo', '15,', 'fa fa-images', 2, 1, NULL, NULL, NULL),
(17, 9, 'allMatches', 'getAllMatches', '1,2,3', 'fa fa-users', 1, 1, NULL, NULL, NULL),
(18, 9, 'addMatch', 'getAddMatch', '1,2,3', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(19, 10, 'all_news', 'getAllBlogs', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(20, 10, 'add_news', 'getAddBlog', '1,2,3', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(21, 11, 'all_links', 'getAllLinks', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(22, 11, 'add_links', 'getAddLink', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(23, 12, 'all_sliders', 'getAllSliders', '1,2,3', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(24, 12, 'add_sliders', 'getAddSlider', '15,', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(25, 13, 'all_subscription', 'getAllSubscription', '1', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(26, 14, 'all_ticket', 'getAllTicket', '1,2', 'fa fa-users', 2, 1, NULL, NULL, NULL),
(27, NULL, 'updateUser', 'getUserById', '1,2,3', NULL, 1, 1, NULL, NULL, NULL),
(29, NULL, 'getSliderById', 'getSliderById', NULL, NULL, 2, 1, NULL, NULL, NULL),
(30, 15, 'allServices', 'getAllServices', NULL, 'fa fa-sitemap', NULL, 1, NULL, NULL, NULL),
(31, 15, 'addService', 'getAddService', NULL, 'fa fa-sitemap', NULL, 1, NULL, NULL, NULL),
(32, NULL, 'updateService', 'getServiceById', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(33, 16, 'sections_services', 'getAllServiceSections', NULL, 'fa fa-columns', NULL, 1, NULL, NULL, NULL),
(34, 16, 'addSectionServices', 'getAddServiceSection', NULL, 'fa fa-columns', NULL, 1, NULL, NULL, NULL),
(35, NULL, 'updateSectionServices', 'getServiceSectionById', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(36, 1, 'getAllRoles', 'getAllRoles', NULL, NULL, 1, 1, NULL, NULL, NULL),
(37, 1, 'getAddRole', 'getAddRole', NULL, NULL, 1, 1, NULL, NULL, NULL),
(38, NULL, 'getRoleById', 'getRoleById', NULL, NULL, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_menus`
--

CREATE TABLE `sidebar_menus` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_access` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `have_header` int(11) NOT NULL DEFAULT '1',
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sidebar_menus`
--

INSERT INTO `sidebar_menus` (`menu_id`, `name`, `roles_access`, `icon`, `ordering`, `active`, `have_header`, `class`, `created_at`, `updated_at`) VALUES
(1, 'users', '1', 'fa fa-users', 1, 1, 1, NULL, NULL, NULL),
(2, 'settings', '1', 'fa fa-sitemap', 7, 0, 1, NULL, NULL, NULL),
(3, 'message', '1', 'fa fa-pencil', 6, 0, 1, NULL, NULL, NULL),
(4, 'ads', '1,2,3', 'fa fa-sitemap', 6, 0, 1, NULL, NULL, NULL),
(5, 'persons', '1', 'fa fa-users', 6, 0, 1, NULL, NULL, NULL),
(6, 'sponsors', '1', 'fa fa-sitemap', 6, 0, 1, NULL, NULL, NULL),
(7, 'championships', '1,2,3', 'fa fa-sitemap', 6, 0, 1, NULL, NULL, NULL),
(8, 'Media', '1,2,3', 'fa fa-sitemap', 6, 0, 1, NULL, NULL, NULL),
(9, 'Matches', '1,2,3', 'fa fa-sitemap', 6, 0, 1, NULL, NULL, NULL),
(10, 'news', '1,2,3', 'fa fa-pencil', 6, 0, 1, NULL, NULL, NULL),
(11, 'links', '1,2,3', 'fa fa-pencil', 6, 0, 1, NULL, NULL, NULL),
(12, 'sliders', '1,2,3', 'fa fa-pencil', 6, 1, 1, NULL, NULL, NULL),
(13, 'subscription', '1', 'fa fa-sitemap', 7, 0, 1, NULL, NULL, NULL),
(14, 'ticket', '1,2', 'fa fa-sitemap', 7, 0, 1, NULL, NULL, NULL),
(15, 'services', '', 'fa fa-sitemap', 2, 1, 1, NULL, NULL, NULL),
(16, 'sections_services', '', 'fa fa-sitemap', 3, 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `slider_id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `data_transition` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'slideup',
  `active` int(11) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`slider_id`, `image`, `data_transition`, `active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, '1426133264746e-2f25-49bd-965e-7bd85b881b84-original.jpeg', 'slideup', 1, 1, NULL, '2018-10-25 12:26:13', '2018-10-25 12:26:13'),
(3, '1341013264746e-2f25-49bd-965e-7bd85b881b84-original.jpeg', 'slideup', 1, 2, 2, '2018-10-21 11:41:01', '2018-10-21 11:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `sliders_translation`
--

CREATE TABLE `sliders_translation` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `long_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders_translation`
--

INSERT INTO `sliders_translation` (`id`, `slider_id`, `locale`, `title`, `short_description`, `long_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'arabic tittle 2', 'short desc 2', 'long desc 2', '2018-10-18 05:16:43', '2018-10-18 06:01:13'),
(2, 1, 'ar', 'عنوان عربى 2', 'وصف عربى 2', 'وصف مفصل عربى 2', '2018-10-18 05:16:43', '2018-10-18 06:01:13'),
(3, 2, 'en', 'titleedd', 'short', 'lonfgdjhb', '2018-10-21 11:06:14', '2018-10-21 11:06:14'),
(4, 2, 'ar', 'عنوان بالعربى', 'وصف', 'مفصل', '2018-10-21 11:06:14', '2018-10-21 11:06:14'),
(5, 3, 'en', 'sss', 'sss', 'sssss', '2018-10-21 11:41:01', '2018-10-21 11:41:21'),
(6, 3, 'ar', 'ee', 'dd', 'dd', '2018-10-21 11:41:01', '2018-10-21 11:41:01'),
(7, 4, 'en', 'fdffdffffff', 'ffffffffffff', 'fffffffff', '2018-10-25 12:26:13', '2018-10-25 12:26:13'),
(8, 4, 'ar', 'ssssssss', 'sssssssssss', 'sssssssss', '2018-10-25 12:26:13', '2018-10-25 12:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'جهينه', 'Vw6ZcXHTfZ1522746513.png', '2018-04-03 14:08:33', '2018-04-03 14:08:33'),
(2, 'حديد المصرين', 'o2i1IqClMa1522746534.png', '2018-04-03 14:08:54', '2018-04-03 14:08:54'),
(3, 'دومتى', 'WTgBc4K2a91522746734.png', '2018-04-03 14:12:14', '2018-04-03 14:12:14'),
(4, 'shell helix', '82UOiEA9ns1522746942.png', '2018-04-03 14:15:42', '2018-04-03 14:15:42'),
(5, 'دومينوز', 'ym2bYmltM81522746965.png', '2018-04-03 14:16:05', '2018-04-03 14:16:05'),
(6, 'راعى', 'nwcUB4MyGi1522842620.png', '2018-04-04 16:50:20', '2018-04-04 16:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `name`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'احمد محمد بكرى', 'ahmed@yahoo.com', '01149671770', '2018-05-14 20:28:02', '2018-05-14 20:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'tag', '2018-05-06 14:43:26', '2018-05-06 14:43:26'),
(2, 'كره يد', '2018-05-07 18:57:01', '2018-05-07 18:57:01'),
(3, 'مجلس الادارة', '2018-06-01 17:51:29', '2018-06-01 17:51:29'),
(4, 'جودو الفاب فرديه', '2018-06-05 14:08:50', '2018-06-05 14:08:50'),
(5, 'كارتيه', '2018-06-05 14:10:25', '2018-06-05 14:10:25'),
(6, 'يي', '2018-10-15 21:56:35', '2018-10-15 21:56:35'),
(7, 'ff', '2018-10-16 06:42:05', '2018-10-16 06:42:05'),
(8, 'vv', '2018-10-16 06:48:28', '2018-10-16 06:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `tag_post`
--

CREATE TABLE `tag_post` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_post`
--

INSERT INTO `tag_post` (`id`, `post_id`, `tag_id`) VALUES
(13, 1, 1),
(2, 29, 2),
(3, 33, 3),
(4, 33, 2),
(6, 37, 5),
(12, 1, 8),
(14, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `full_name`, `email`, `phone`, `ticket_name`, `created_at`, `updated_at`) VALUES
(1, 'أحمد محمد بكرى', 'ahmed@yahoo.com', '01149671770', 'شرم الشيخ', '2018-05-14 20:28:37', '2018-05-14 20:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `items_access` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `active` int(11) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `items_access`, `password`, `created_by`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'super', 'super@admin.com', 'a:37:{i:0;s:11:"getAllUsers";i:1;s:10:"getAddUser";i:2;s:11:"getSettings";i:3;s:14:"getAllContacts";i:4;s:21:"ads_management.create";i:5;s:20:"ads_management.index";i:6;s:13:"getAllPersons";i:7;s:12:"getAddPerson";i:8;s:14:"getAllSponsors";i:9;s:13:"getAddSponsor";i:10;s:19:"getAllChampionships";i:11;s:18:"getAddChampionship";i:12;s:11:"imagesIndex";i:13;s:9:"addImages";i:14;s:11:"videosIndex";i:15;s:8:"addVideo";i:16;s:13:"getAllMatches";i:17;s:11:"getAddMatch";i:18;s:11:"getAllBlogs";i:19;s:10:"getAddBlog";i:20;s:11:"getAllLinks";i:21;s:10:"getAddLink";i:22;s:13:"getAllSliders";i:23;s:12:"getAddSlider";i:24;s:18:"getAllSubscription";i:25;s:12:"getAllTicket";i:26;s:11:"getUserById";i:27;s:13:"getSliderById";i:28;s:14:"getAllServices";i:29;s:13:"getAddService";i:30;s:14:"getServiceById";i:31;s:21:"getAllServiceSections";i:32;s:20:"getAddServiceSection";i:33;s:21:"getServiceSectionById";i:34;s:11:"getAllRoles";i:35;s:10:"getAddRole";i:36;s:11:"getRoleById";}', '$2y$10$2IcFBQFddbE365YaLdNEDuNQ18zV7cpRW05r.WMvKgffr.hCJIIcy', 1, 1, 'D7W4IBYTsr4FsCSEthihZ59TfAn5Dkf892ntPTKpCmoZp1K1e1ZgRSkG7UBr', NULL, '2018-10-24 15:59:30'),
(2, 1, 'admin', 'admin@admin.com', 'a:28:{i:0;s:11:"getAllUsers";i:1;s:10:"getAddUser";i:2;s:11:"getSettings";i:3;s:14:"getAllContacts";i:4;s:21:"ads_management.create";i:5;s:20:"ads_management.index";i:6;s:13:"getAllPersons";i:7;s:12:"getAddPerson";i:8;s:14:"getAllSponsors";i:9;s:13:"getAddSponsor";i:10;s:19:"getAllChampionships";i:11;s:18:"getAddChampionship";i:12;s:11:"imagesIndex";i:13;s:9:"addImages";i:14;s:11:"videosIndex";i:15;s:8:"addVideo";i:16;s:13:"getAllMatches";i:17;s:11:"getAddMatch";i:18;s:11:"getAllBlogs";i:19;s:10:"getAddBlog";i:20;s:11:"getAllLinks";i:21;s:10:"getAddLink";i:22;s:13:"getAllSliders";i:23;s:12:"getAddSlider";i:24;s:18:"getAllSubscription";i:25;s:12:"getAllTicket";i:26;s:11:"getUserById";i:27;s:13:"getSliderById";}', '$2y$10$pnvfTMcGvD5NG1lWnfS5ROS8sPmhT9qRd8xwSclrZObxDCzxueR82', 1, 1, 'xOUIxn2sM1w3sdFFVJJ0LO8bmSROCnsDQb1HHotzm2zIWa4RVULissGCzvVp', NULL, NULL),
(3, 3, 'writer', 'Writer@admin.com', NULL, '$2y$10$EcTAVYZBeqkxI2rskH/R7uy6YAqOakBDBDsj0a2tv6MndB5b.HWYy', 1, 1, 'VnhZbE05ILPYp8EEEGhIaj5XpNK2DEfV9bhjx8MOFpYm77iRK64aH2OzXCLf', NULL, NULL),
(6, 3, 'ahmed', 'ahmed.adel.aly17@gmail.com', NULL, '$2y$10$B8s2Ck56bxw9QmhuzstOAu/uc67euDPfUrQAmbrJc80b7ir81cBsa', 1, 1, 'CXLOG0gcW5IxL2lCzqgw5tJZZs6orWn8Po8ZWmEdqdnKQLrsVIEW4V7v8JVJ', '2018-06-09 20:52:20', '2018-06-09 20:52:20'),
(7, 2, 'Adam', 'Adam@altarsana.com', NULL, '$2y$10$JTw.aMODlt8Np/c6xHpsMOoQ7FCcGRNgTUi8Tiv4kFrPSpaiJcwHq', 1, 1, NULL, '2018-06-09 20:56:33', '2018-06-09 20:56:33'),
(8, 1, '666', 'admin@esmaar.net', NULL, '$2y$10$HPAAzfGZJhbBX.wqzPPQl.pGtq52JoTWk6E/Elx3oevJS1tMFuNGy', 2, 1, 'YyCU86gkYYPVDgWrjAeWhB1v1ZBWEalmomWQkqmcqRGxBDHBZV6euBSosgcD', NULL, NULL),
(9, 1, 'Mahmoud Rabie', 'mahmoud@maha.net', NULL, '$2y$10$GnUWvQ2mV0q7yllyHoRoleKqvOJdkOm7XinhYzLb3TWuTKBNMErTO', 2, 1, NULL, NULL, NULL),
(10, 1, 'Mahmoud Rabie', 'mahmoud@maha.com', NULL, '$2y$10$DLTCGN.E.H8bCkDbP9GegOAK1Kj/QK.4toJgqLymCfhXctcTXxScq', 2, 1, NULL, NULL, NULL),
(11, 1, 'Mahmoud Rabie', 'mahmoudRa@maha.com', NULL, '$2y$10$zX33igB8qiv7C8dPy4l4uuIjt2hl/OsgRfRe9f507FqtQEJJ5DNtK', 2, 1, NULL, NULL, NULL),
(12, 1, 'Mahmoud Rabie3', 'mahmoud@ma.net', NULL, '$2y$10$/JHCDy.ZhI65GRHUJQIdDuMh8czyPu4onlPk4ikB2jssJVli3hsgi', 2, 1, NULL, NULL, NULL),
(13, 1, 'Mahmoud Rabie3', 'mahmoud@ma.netf', NULL, '$2y$10$my8XQZ2u5a.k./uqpANNjOXEP0p9KbLQLfk4C3lmPjGvTJZkm8.82', 2, 1, NULL, NULL, NULL),
(14, 1, 'Mahmoud Rabie3', 'madhmoud@ma.netf', NULL, '$2y$10$faec6Q.kZ74h3i93It6idO/P2/mni2Amm4kxbJv4PI5H3NG7qMuo2', 2, 1, NULL, NULL, NULL),
(15, 2, 'dd', 'ssadmin@esmaar.net', NULL, '$2y$10$c6YiwIhUqpaBTy9tfHXdg.LMNgONT4HHSD8neey/gba5HZy4nK7Nm', 2, 1, NULL, NULL, NULL),
(16, 1, 'new User', 'new@mahacode.com', NULL, '$2y$10$6QZZRo5o36IrB0ONNjTnQuOj7vSdoJz55KaFcmy2SeoNElP44Ozei', 1, 1, NULL, '2018-10-22 06:41:32', '2018-10-22 06:41:32'),
(17, 1, 'Mahmoud Rabiess', 'admin@ss.co', NULL, '$2y$10$3CEZjDrlPdSsns/eA/rk4ejmsY9axNhPnvSMh9M/jhIF2j2C6JI0q', 1, 1, NULL, '2018-10-22 06:43:07', '2018-10-22 06:43:07'),
(18, 1, 'ddd', 'adddmin@admin.com', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', '$2y$10$2NO4we696Pnhw9lJajXNbOP0K5iFc0uA4XQCF8mJqk6xajdSqD4RG', 1, 1, NULL, '2018-10-22 06:49:47', '2018-10-22 06:49:47'),
(19, 1, 'ddee', 'root@root.com', 'a:9:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"5";i:4;s:1:"7";i:5;s:2:"14";i:6;s:2:"18";i:7;s:2:"22";i:8;s:2:"26";}', '$2y$10$MLgAFPC0VRPQIAbMptvP..lPjr/mZ/47Tbcp4UNMxzDut5Rh/sqBC', 1, 1, NULL, '2018-10-22 11:13:36', '2018-10-22 11:19:41'),
(20, 1, 'بتنجانة', 'btn@admin.com', 'a:8:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"6";i:3;s:1:"8";i:4;s:1:"9";i:5;s:2:"10";i:6;s:2:"16";i:7;s:2:"18";}', '$2y$10$p8KWXUA/A5NUaPYoHpwp..DtGoXzYKdB4VhdSJ1KQ9p.Fip4lHNG6', 1, 1, 'UhFuC245fHmEx45hkUuHR9cdRCpvZl533RuLznKpAGQdlzVFlXqpG8pN1goa', '2018-10-22 13:44:49', '2018-10-22 13:44:49'),
(21, 1, 'New User', 'newuser@admin.com', 'a:1:{i:0;s:1:"1";}', '$2y$10$esxwui3iQfqB0kfTEBuqvem397E6nmZbk./zeJqGveiQzFGpjjamS', 1, 1, NULL, '2018-10-23 10:11:12', '2018-10-23 10:11:12'),
(22, 1, 'temp1', 'temp@user.com', NULL, '', 1, 1, NULL, NULL, NULL),
(23, 1, 'new .net', 'new@new.net', 'N;', '$2y$10$WHbLMYaisUcyg5xiUOArceKVMVleJiVx5ZTYNuWyjF.el5kAEwlEm', 1, 1, NULL, '2018-10-23 12:24:00', '2018-10-23 12:24:00'),
(24, 1, 'nnn', 'nnn@nn.com', 'a:1:{i:0;s:11:"getAllUsers";}', '$2y$10$MGOmC.3gL4mJfGW68Cp6T.5zUWt0esWUt7nYUNhJoYJixoplOm0rC', 1, 1, NULL, '2018-10-23 12:29:29', '2018-10-23 12:29:29'),
(25, 1, 'ffff', 'fff@ff.com', 'a:1:{i:0;s:11:"getBackendHome";}', '$2y$10$rgHMDodnQ6nrNIAfh8tpdOhrloypj3nbbyMWDfPnB1E4jqbq8Tdle', 1, 1, NULL, '2018-10-23 12:31:35', '2018-10-23 12:31:35'),
(26, 1, 'abd', 'ww@mm.com', 'a:1:{i:0;s:11:"getAllUsers";}', '$2y$10$6TOWjvpOnW3vfhg7YP2MQ.ciQUfURuHy7dI8zVTO06iWl//dOlfDm', 1, 1, NULL, '2018-10-23 12:35:05', '2018-10-23 12:35:05'),
(27, 1, 'ddd', 'dd@dd.com', 'a:1:{i:0;s:11:"getAllUsers";}', '$2y$10$Na0p0YoNTx8owAq45MKI4OWbXIgsR2P0fkvytOfW13ydXIVZL2Ujy', 1, 1, NULL, '2018-10-23 12:37:34', '2018-10-23 12:37:34'),
(28, 1, 'dhjdh', 'ss@ff.com', 'a:1:{i:0;s:11:"getAddBlog";}', '$2y$10$TCUpsVLH4vlYPqezgBVKXOM7KfUMcK7yruFEb0pEghwYHMN1LeC/W', 1, 1, NULL, '2018-10-23 12:41:09', '2018-10-23 12:41:09'),
(29, 1, 'ff', 'admin@esmaar.netff', 'a:1:{i:0;s:11:"getAddBlog";}', '$2y$10$kHwfjK.56iSbWJWlWaA2MOOsWsxwWCO65b0WfSzvVmWpJNQk.07ya', 1, 1, NULL, '2018-10-23 12:42:29', '2018-10-23 12:42:29'),
(30, 1, 'ff', 'admin@dfd.com', 'a:1:{i:0;s:11:"getAddSlider";}', '$2y$10$Pet3u1zIFSrngG7eiJaaK.EHlgqC2VmJD2Lbula.TLMkcyLvW1lfS', 1, 1, NULL, '2018-10-23 12:44:11', '2018-10-23 12:44:11'),
(31, 1, 'dfdf', 'admin@admin.comdfddd', 'a:1:{i:0;s:12:"getAddSlider";}', '$2y$10$WHbLMYaisUcyg5xiUOArceKVMVleJiVx5ZTYNuWyjF.el5kAEwlEm', 1, 1, NULL, '2018-10-23 12:49:42', '2018-10-23 12:49:42'),
(32, 1, 'bss', 'bss@bss.com', 'a:2:{i:0;s:11:"getAllUsers";i:1;s:12:"getAddSlider";}', '$2y$10$lObIKDqgavjpFWpZ2/if..enhgPJySDl9Ju2l1jRBDmHiPi.Ah2lG', 1, 1, 'MD3bm6njSfIAbUwwlJguCJOHmkN0O7tqji0DmJPAeLTrsc0JXfM8QfzQTalp', '2018-10-23 13:07:13', '2018-10-23 13:07:13'),
(33, 1, 'yasen', 'tasen@yy.com', 'a:3:{i:0;s:10:"getAddUser";i:1;s:13:"getAllSliders";i:2;s:12:"getAddSlider";}', '$2y$10$8laoQKGiP/a.lNSdUZZ5keYfivMT063SpNNzoDYs31btFSDQh3W.u', 1, 1, NULL, '2018-10-23 13:09:48', '2018-10-23 13:09:48'),
(34, 8, 'sasa', 'slider@admin.com', 'a:1:{i:0;s:12:"getAddSlider";}', '$2y$10$8LmV2lQZzMUUR6u6rf0ZJugQ0lQuNm1WF1JOv4qOlZDpZ6pYUr6mq', 1, 1, NULL, '2018-10-25 07:39:29', '2018-10-25 07:39:29'),
(36, 10, 'mostapha', 'mosta@m.com', 'a:2:{i:0;s:10:"getAddUser";i:1;s:13:"getAddService";}', '$2y$10$b2Vq3.6PLsucEOiO2sQqB.NNoXKE8KLGkP0I5FOs10Nd6V3AjGxnC', 1, 1, NULL, '2018-10-25 12:08:48', '2018-10-25 12:08:48'),
(37, 11, 'شسيسس', 'test@test.com', 'a:3:{i:0;s:14:"getAllServices";i:1;s:13:"getAddService";i:2;s:14:"getServiceById";}', '$2y$10$ZfNOf6LTxyw4QvCr4wCmzu2UexD9SeXgLMKLSpysfGig0igdCUzfy', 1, 1, NULL, '2018-10-25 12:22:47', '2018-10-25 12:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `items_access` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`role_id`, `role_name`, `items_access`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'a:37:{i:0;s:11:"getAllUsers";i:1;s:10:"getAddUser";i:2;s:11:"getSettings";i:3;s:14:"getAllContacts";i:4;s:21:"ads_management.create";i:5;s:20:"ads_management.index";i:6;s:13:"getAllPersons";i:7;s:12:"getAddPerson";i:8;s:14:"getAllSponsors";i:9;s:13:"getAddSponsor";i:10;s:19:"getAllChampionships";i:11;s:18:"getAddChampionship";i:12;s:11:"imagesIndex";i:13;s:9:"addImages";i:14;s:11:"videosIndex";i:15;s:8:"addVideo";i:16;s:13:"getAllMatches";i:17;s:11:"getAddMatch";i:18;s:11:"getAllBlogs";i:19;s:10:"getAddBlog";i:20;s:11:"getAllLinks";i:21;s:10:"getAddLink";i:22;s:13:"getAllSliders";i:23;s:12:"getAddSlider";i:24;s:18:"getAllSubscription";i:25;s:12:"getAllTicket";i:26;s:11:"getUserById";i:27;s:13:"getSliderById";i:28;s:14:"getAllServices";i:29;s:13:"getAddService";i:30;s:14:"getServiceById";i:31;s:21:"getAllServiceSections";i:32;s:20:"getAddServiceSection";i:33;s:21:"getServiceSectionById";i:34;s:11:"getAllRoles";i:35;s:10:"getAddRole";i:36;s:11:"getRoleById";}', NULL, '2018-10-25 12:07:31'),
(2, ' Admin', 'a:2:{i:0;s:10:"getAddUser";i:1;s:12:"getAddSlider";}', NULL, NULL),
(3, ' Writer', 'a:2:{i:0;s:10:"getAddUser";i:1;s:12:"getAddSlider";}', NULL, NULL),
(5, 'abdalaa', 'a:6:{i:0;s:11:"getAllUsers";i:1;s:10:"getAddUser";i:2;s:14:"getAllContacts";i:3;s:21:"ads_management.create";i:4;s:13:"getAllPersons";i:5;s:12:"getAddPerson";}', '2018-10-24 15:56:57', '2018-10-24 16:08:14'),
(6, 'test', 'a:2:{i:0;s:10:"getAddUser";i:1;s:12:"getAddSlider";}', '2018-10-25 05:53:00', '2018-10-25 05:53:00'),
(7, 'safwat', 'a:3:{i:0;s:11:"getAllUsers";i:1;s:11:"getSettings";i:2;s:21:"ads_management.create";}', '2018-10-25 05:57:00', '2018-10-25 05:57:00'),
(8, 'كاتب اسليدر', 'a:2:{i:0;s:13:"getAllSliders";i:1;s:12:"getAddSlider";}', '2018-10-25 07:27:29', '2018-10-25 07:27:29'),
(9, 'يكتب اسليدر', 'a:1:{i:0;s:12:"getAddSlider";}', '2018-10-25 10:16:38', '2018-10-25 10:18:46'),
(10, 'mostapha', 'a:37:{i:0;s:11:"getAllUsers";i:1;s:10:"getAddUser";i:2;s:11:"getSettings";i:3;s:14:"getAllContacts";i:4;s:21:"ads_management.create";i:5;s:20:"ads_management.index";i:6;s:13:"getAllPersons";i:7;s:12:"getAddPerson";i:8;s:14:"getAllSponsors";i:9;s:13:"getAddSponsor";i:10;s:19:"getAllChampionships";i:11;s:18:"getAddChampionship";i:12;s:11:"imagesIndex";i:13;s:9:"addImages";i:14;s:11:"videosIndex";i:15;s:8:"addVideo";i:16;s:13:"getAllMatches";i:17;s:11:"getAddMatch";i:18;s:11:"getAllBlogs";i:19;s:10:"getAddBlog";i:20;s:11:"getAllLinks";i:21;s:10:"getAddLink";i:22;s:13:"getAllSliders";i:23;s:12:"getAddSlider";i:24;s:18:"getAllSubscription";i:25;s:12:"getAllTicket";i:26;s:11:"getUserById";i:27;s:13:"getSliderById";i:28;s:14:"getAllServices";i:29;s:13:"getAddService";i:30;s:14:"getServiceById";i:31;s:21:"getAllServiceSections";i:32;s:20:"getAddServiceSection";i:33;s:21:"getServiceSectionById";i:34;s:11:"getAllRoles";i:35;s:10:"getAddRole";i:36;s:11:"getRoleById";}', '2018-10-25 12:08:06', '2018-10-25 12:09:37'),
(11, 'كاتب', 'a:3:{i:0;s:14:"getAllServices";i:1;s:13:"getAddService";i:2;s:14:"getServiceById";}', '2018-10-25 12:22:03', '2018-10-25 12:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `description`, `type`, `video`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 'مباراة الترسانة VS البنك الأهلى 2 / 0', '... دورى الدرجة الثانية الجولة الـ 32', 'youtube', 'https://www.youtube.com/watch?v=RRSIzWaL3Hg', 'https://img.youtube.com/vi/RRSIzWaL3Hg/hqdefault.jpg', '2018-04-04 14:58:41', '2018-04-04 15:00:03'),
(2, 'الترسانه', 'الترسانه وصف بسيط', 'youtube', 'https://www.youtube.com/watch?v=O2_pluGhO6E', 'https://img.youtube.com/vi/O2_pluGhO6E/hqdefault.jpg', '2018-04-04 17:02:51', '2018-04-04 17:02:51'),
(3, 'الترسانه ملف', 'الترسانه ملف وصف بسيط', 'file', 'dTUv7tE0Nz1522844505.mp4', 'DUY11nmnti1522844505.png', '2018-04-04 17:21:45', '2018-04-04 17:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visit_time` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ads_position_id_foreign` (`position_id`);

--
-- Indexes for table `ads_positions`
--
ALTER TABLE `ads_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `blogs_translation`
--
ALTER TABLE `blogs_translation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `championships`
--
ALTER TABLE `championships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `links_link_section_id_foreign` (`link_section_id`);

--
-- Indexes for table `link_sections`
--
ALTER TABLE `link_sections`
  ADD PRIMARY KEY (`link_section_id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persons_job_id_foreign` (`job_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`),
  ADD KEY `services_service_section_id_foreign` (`service_section_id`);

--
-- Indexes for table `services_translation`
--
ALTER TABLE `services_translation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_sections`
--
ALTER TABLE `service_sections`
  ADD PRIMARY KEY (`service_section_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sidebar_items`
--
ALTER TABLE `sidebar_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `sidebar_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `sidebar_menus`
--
ALTER TABLE `sidebar_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `sliders_translation`
--
ALTER TABLE `sliders_translation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscription_email_unique` (`email`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_post`
--
ALTER TABLE `tag_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_post_post_id_foreign` (`post_id`),
  ADD KEY `tag_post_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ticket_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ads_positions`
--
ALTER TABLE `ads_positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blogs_translation`
--
ALTER TABLE `blogs_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `championships`
--
ALTER TABLE `championships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `link_sections`
--
ALTER TABLE `link_sections`
  MODIFY `link_section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `services_translation`
--
ALTER TABLE `services_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `service_sections`
--
ALTER TABLE `service_sections`
  MODIFY `service_section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sidebar_items`
--
ALTER TABLE `sidebar_items`
  MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `sidebar_menus`
--
ALTER TABLE `sidebar_menus`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `slider_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sliders_translation`
--
ALTER TABLE `sliders_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tag_post`
--
ALTER TABLE `tag_post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
