<?php

namespace App\Http\Controllers;

use App\Repositories\ChampionshipRepositories;
use Illuminate\Http\Request;
use App\Repositories\SliderRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SponsorsRepository;
use App\Repositories\BlogRepository;
use App\Entities\BlogCategory as BlogCategoryEntity;
use Modules\Frontend\Persons\Models\Person as PersonModel;
use Modules\Frontend\Media\Models\Image as ImageModel;
use Modules\Frontend\Media\Models\Video as VideoModel;
use App\Entities\Translation;
use Lang;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    protected $sliderRepository;
    protected $serviceRepository;
    protected $sponsorRepository;
    protected $blogRepository;


    public function __construct()
    {
        $this->sliderRepository  = new SliderRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->sponsorRepository = new SponsorsRepository();
        $this->blogRepository = new BlogRepository();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PersonModel $personModel, ChampionshipRepositories $championshipRepository, BlogCategoryEntity $blogCategory, ImageModel $imageModel, VideoModel $videoModel)
    {
        $legends        = $personModel->legends()->get();
        $players        = $personModel->players()->get();
        $managers        = $personModel->managers()->get();
        $sliders        = $this->sliderRepository->getAllSliders();
        $latestNews     =  $this->blogRepository->listNews();
        $services       = $this->serviceRepository->getTopServices();
        $sponsors       = $this->sponsorRepository->getAllSponsors();
        $champions      = $championshipRepository->getAllChampions();
        $breakingNews   = $this->blogRepository->breakingNews();
        $sliderNews     = $this->blogRepository->sliderNews();
        $newestNews     = $this->blogRepository->getNewestNews()->take(10)->get();
        $handballNews   = $this->blogRepository->handballNews()->take(10)->get();
        $basketballNews = $this->blogRepository->basketballNews()->take(10)->get();
        $volleyballNews = $this->blogRepository->vollyballNews()->take(10)->get();
        $albums         = $imageModel->allAlbums(10);
        $videos         = $videoModel->allVideos();
        return view('front.tersana.index', compact(
                'sliders',
                'services',
                'legends',
                'managers',
                'players',
                'champions',
                'sponsors',
                'breakingNews',
                'sliderNews',
                'newestNews',
                'handballNews',
                'basketballNews',
                'volleyballNews',
                'blogCategory',
                'albums',
                'videos',
                'latestNews'
                ));
    }
     static public function translateWord($word)
   {
       if(is_object(Translation::where('word',$word)->where('lang',Lang::getLocale())->first()))
       {
           return Translation::where('word',$word)->where('lang',Lang::getLocale())->first()->translation;
       }
           foreach (\Config::get('languages') as $locale=>$language)
           {
               $translate=new Translation();
               $translate->word=$word;
               $translate->lang=$locale;
               $translate->translation=$word;
               $translate->save();
           }
           return Translation::where('word',$word)->where('lang',Lang::getLocale())->first()->translation;
   }
}
