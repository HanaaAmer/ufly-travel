<?php

namespace App\Repositories;

use App\Entities\Tour_Detail;

use File;

class Tour_DetailRepository extends BaseRepository
{
    protected $tour_detail;
    public function __construct()
    {
        $this->tour_detail=new Tour_Detail();
    }
    public function getAllTour_Details()
    {
        return $this->getAllItems($this->tour_detail);
    }
   
    public function postAddTour_Detail($data,$tour_detail)
    {
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            $tour_detail->fill(['image'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $tour_detail->{"title:$locale"}   = $data->input("title:{$locale}");
            $tour_detail->{"description:$locale"} = $data->input("description:{$locale}");
            
        }   
       
        $tour_detail->fill(['tour_id'=>$data->tour_id]);
        $tour_detail->fill(['city_id'=>$data->city_id]);      
        $tour_detail->save();
        return $tour_detail;
    }
    public function getTour_DetailById($tour_detailId)
    {
        return $this->getItemById($tour_detailId,$this->tour_detail);
    }
    public function updateTour_DetailById($tour_detailId,$data)
    {
        // return $data;
        $tour_detail=$this->tour_detail->find($tour_detailId);
        if ($data->hasFile('image'))
        {
            $photoName=$tour_detail->image;
            File::delete('public/assets/images/tours/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            $tour_detail->fill(['image'=>$picture]);
        }
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $tour_detail->{"title:$locale"}   = $data->input("title:{$locale}");
            $tour_detail->{"description:$locale"} = $data->input("description:{$locale}");
            
        }   
        $tour_detail->fill(['tour_id'=>$data->tour_id]); 
        $tour_detail->fill(['city_id'=>$data->city_id]);      
        $tour_detail->save();  
        return $tour_detail;
    }

    public function deleteTour_DetailById($tour_detailId)
    {
        $tour_detail=$this->tour_detail->find($tour_detailId);
        $photoName=$tour_detail->image;
        File::delete('public/assets/images/tours/'.$photoName);
        $this->deleteItemById($tour_detailId,$this->tour_detail);
    }

   
}