<?php

namespace App\Repositories;
use File;
use App\Entities\aboutus_images;


class aboutus_imagesRepository extends BaseRepository
{
    protected $aboutusImage;

    public function __construct()
    {
        $this->aboutusImage = new aboutus_images();
    }

    public function getAllaboutusImages()
    {
        return $this->getAllItems($this->aboutusImage);
    }

    public function postAddaboutusImages($data, $aboutusImage)
    {
       
        $req=[];
        if ($data->hasFile('image') )
        {
            foreach($data->file('image') as $image)
            {
            
           
            $filename = $image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/About';
            $image->move($destinationPath, $picture);
            $req[] =  $picture;  
         
        }
       
        
             
    } 
  
    $aboutusImage->image=( $req);
      
            $aboutusImage->save();

            return $aboutusImage;
             
        
    }

    public function getaboutusImageById($aboutusImageId)
    {
        return $this->getItemById($aboutusImageId, $this->aboutusImage);
    }

    public function updateaboutusImagesById($aboutusImageId, $data)
    {
       
        $req=[];
        $photoName= [];
        $aboutusImage = $this->aboutusImage->find($aboutusImageId);
          $photoName = $aboutusImage->image;
        if( $aboutusImage->image != null)
       {
        if ($data->hasFile('image')) {
           
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/About';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }
        
            $aboutusImage->image = array_merge($aboutusImage->image,$req) ;
        }
        
        elseif( $aboutusImage->image == []){
           
          foreach ($photoName as $imagepath) {
                File::delete('public/assets/images/About/'.$imagepath);
            }
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/About';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
            $aboutusImage->image = $req;
       }
        else {
            $aboutusImage->image = $photoName;
        }
    
      $aboutusImage->save();

    
     
    }

    public function deleteaboutusImageById($aboutusImageId)
    {
        $aboutusImage = $this->aboutusImage->find($aboutusImageId);
            foreach($aboutusImage->image as $imagepath)
            {
            
            File::delete('public/assets/images/About/'.$imagepath);
        }
        $this->deleteItemById($aboutusImageId, $this->aboutusImage);
        
    }
}
