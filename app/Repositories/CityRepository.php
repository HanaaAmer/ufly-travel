<?php

namespace App\Repositories;
use File;
use App\Entities\Cities;

use App\Entities\city_images;
use App\Entities\CitiesTranslation;
class CityRepository extends BaseRepository
{
    protected $City;

    public function __construct()
    {
        $this->City = new Cities();
        $this->Cityimage= new city_images();
        $this->Citytranslation= new CitiesTranslation();
    }

    public function getAllCity()
    {
        return $this->getAllItems($this->City);
    }

    public function postAddCity($data, $City)
    {
     
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/Cities';
            $file->move($destinationPath, $picture);
            $City->fill(['image'=>$picture]);
        }
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $City->{"name:$locale"}   = $data->input("name:{$locale}");
            $City->{"descrption:$locale"}   = $data->input("descrption:{$locale}");
           
            $City->{"title:$locale"}   = $data->input("title:{$locale}");
            $City->{"long_descrption:$locale"}   = $data->input("long_descrption:{$locale}");
        }
      
        $City->fill(['country_id' => $data->country_id]);
        $City->save();

        return $City;
    }

    public function getCityById($CityId)
    {
        return $this->getItemById($CityId, $this->City);
    }

    public function updateCityById($CityId, $data)
    {
      
        $City = $this->City->find($CityId);
        if ($data->hasFile('image'))
        {
            $photoName=$City->image;
            File::delete('public/assets/images/Cities/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/Cities';
            $file->move($destinationPath, $picture);
            $City->fill(['image'=>$picture]);
        }
        
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $City->{"name:$locale"}   = $data->input("name:{$locale}");
            $City->{"descrption:$locale"}   = $data->input("descrption:{$locale}");
          
            $City->{"title:$locale"}   = $data->input("title:{$locale}");
            $City->{"long_descrption:$locale"}   = $data->input("long_descrption:{$locale}");
        }
        $City->fill(['country_id' => $data->country_id]);
       
        $City->save();

        return $City;
    }

    public function deleteCityById($CityId)
    {
       
       
       
        $Cityimage = city_images::all()->where('City_id', '=', $CityId);
          foreach($Cityimage as $image){
       $this->deleteItemById($image->id, $this->Cityimage);
          }
           $Citytranslation = CitiesTranslation::all()->where('cities_id', '=', $CityId);
          foreach($Citytranslation as $translation){
       $this->deleteItemById($translation->id, $this->Citytranslation);
          }
        $City = $this->City->find($CityId);
        $this->deleteItemById($CityId, $this->City);
        
    }
      static public function getcites()
    {
        return Cities::all();
    }
     static public function getcitesfooter()
    {
        return Cities::orderBy('created_at', 'desc')->take(10)->get();
    }
}
