<?php

namespace App\Repositories;

use App\Entities\Country;
use App\Entities\CountryTranslation;

class CountryRepository extends BaseRepository
{
    protected $Country;

    public function __construct()
    {
        $this->Country = new Country();
        $this->Countrytranslation = new CountryTranslation();
    }

    public function getAllCountry()
    {
        return $this->getAllItems($this->Country);
    }

    public function postAddCountry($data, $Country)
    {
        foreach (\Config::get('languages') as $locale => $language) {
            $Country->{"name:$locale"} = $data->input("name:{$locale}");
        }

        $Country->fill(['iso' => $data->iso]);
        $Country->save();

        return $Country;
    }

    public function getCountryById($CountryId)
    {
        return $this->getItemById($CountryId, $this->Country);
    }

    public function updateCountryById($CountryId, $data)
    {
        $Country = $this->Country->find($CountryId);
        foreach (\Config::get('languages') as $locale => $language) {
            $Country->{"name:$locale"} = $data->input("name:{$locale}");
        }

        $Country->fill(['iso' => $data->iso]);
        $Country->save();

        return $Country;
    }

    public function deleteCountryById($CountryId)
    {
        $Countrytranslation = CountryTranslation::all()->where('country_id', '=', $CountryId);
        foreach ($Countrytranslation as $translation) {
            $this->deleteItemById($translation->id, $this->Countrytranslation);
        }

        $Country = $this->Country->find($CountryId);

        $this->deleteItemById($CountryId, $this->Country);
    }

    public static function getcountry()
    {
        return Country::all();
    }
}
