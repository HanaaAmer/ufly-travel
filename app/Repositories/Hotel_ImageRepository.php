<?php

namespace App\Repositories;

use App\Entities\Hotel_Image;

use File;

class Hotel_ImageRepository extends BaseRepository
{
    protected $hotel_image;
    public function __construct()
    {
        $this->hotel_image=new Hotel_Image();
    }
    public function getAllHotel_Image()
    {
        return $this->getAllItems($this->hotel_image);
    }
   
    public function postAddHotel_Image($data,$hotel_image)
    {
        $files=[];
        if ($data->hasFile('image'))
        {            
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];          
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/hotels';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);

            }    
        }
      
        $hotel_image->fill(['city_id'=>$data->city_id]);
        $hotel_image->fill(['hotel_id'=>$data->hotel_id]);
        $hotel_image->fill(['image'=>$files]);
        $hotel_image->save();
        return $hotel_image;
    }
    public function getHotel_ImageById($hotel_imageId)
    {
        return $this->getItemById($hotel_imageId,$this->hotel_image);
    }
    public function updateHotel_ImageById($hotel_imageId,$data)
    {
        // return $data;
        $hotel_image=$this->hotel_image->find($hotel_imageId);
       $files=[];
        $photoName=[];
        
            $photoName=$hotel_image->image;
            if ($data->hasFile('image'))
        {
           
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];  
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/hotels';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);
            }
        }
        $files = array_merge($files,$photoName) ;  
        $hotel_image->fill(['city_id'=>$data->city_id]);
        $hotel_image->fill(['hotel_id'=>$data->hotel_id]);
        $hotel_image->fill(['image'=>$files]);
        $hotel_image->save();  
        return $hotel_image;
    }

    public function deleteHotel_ImageById($hotel_imageId)
    {
        $hotel_image=$this->hotel_image->find($hotel_imageId);
        $photoName=$hotel_image->image;
        for($i=0; $i<count($photoName);$i++){
            File::delete('public/assets/images/hotels/'.$photoName[$i]);
            }
        $this->deleteItemById($hotel_imageId,$this->hotel_image);
    }

   
}