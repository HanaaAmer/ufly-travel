<?php

namespace App\Repositories;

use App\Entities\Hotel;
use App\Entities\Hotel_Image;
use App\Entities\HotelTranslation;
use App\Entities\Booking;
use App\Entities\Tour;

use File;

class HotelRepository extends BaseRepository
{
    protected $hotel;
    public function __construct()
    {
        $this->hotel=new Hotel();
    }
    public function getAllHotels()
    {
        return $this->getAllItems($this->hotel);
    }
   
    public function postAddHotel($data,$hotel)
    {
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/hotels';
            $file->move($destinationPath, $picture);
            $hotel->fill(['image'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $hotel->{"name:$locale"}   = $data->input("name:{$locale}");
            $hotel->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $hotel->{"long_description:$locale"} = $data->input("long_description:{$locale}");
            $hotel->{"services:$locale"} = $data->input("services:{$locale}");
            $hotel->{"Amenities:$locale"} = $data->input("Amenities:{$locale}");
        }
        $hotel->fill(['availability'=>$data->availability]);
        $hotel->fill(['adults'=>$data->adults]);
        $hotel->fill(['childern'=>$data->childern]);
        $hotel->fill(['beds'=>$data->beds]);
        $hotel->fill(['capacity'=>$data->capacity]);
         $hotel->fill(['map_lat'=>$data->map_lat]);
          $hotel->fill(['map_lng'=>$data->map_lng]);
        $hotel->fill(['price'=>$data->price]);
        $hotel->fill(['category_id'=>$data->category_id]);
        $hotel->fill(['duration_day'=>$data->duration_day]);
        $hotel->fill(['duration_night'=>$data->duration_night]);
        $hotel->fill(['city_id'=>$data->city_id]);
        $hotel->save();
        return $hotel;
    }
    public function getHotelById($hotelId)
    {
        return $this->getItemById($hotelId,$this->hotel);
    }
   
   
    public function updateHoteleById($hotelId,$data)
    {
        // return $data;
        $hotel=$this->hotel->find($hotelId);
        if ($data->hasFile('image'))
        {
            $photoName=$hotel->image;
            File::delete('public/assets/images/hotels/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/hotels';
            $file->move($destinationPath, $picture);
            $hotel->fill(['image'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $hotel->{"name:$locale"}   = $data->input("name:{$locale}");
            $hotel->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $hotel->{"long_description:$locale"} = $data->input("long_description:{$locale}");
            $hotel->{"services:$locale"} = $data->input("services:{$locale}");
            $hotel->{"Amenities:$locale"} = $data->input("Amenities:{$locale}");
        }
        $hotel->fill(['availability'=>$data->availability]);
        $hotel->fill(['adults'=>$data->adults]);
        $hotel->fill(['childern'=>$data->childern]);
        $hotel->fill(['beds'=>$data->beds]);
        $hotel->fill(['capacity'=>$data->capacity]);
        $hotel->fill(['map_lat'=>$data->map_lat]);
          $hotel->fill(['map_lng'=>$data->map_lng]);
        $hotel->fill(['price'=>$data->price]);
        $hotel->fill(['category_id'=>$data->category_id]);
        $hotel->fill(['duration_day'=>$data->duration_day]);
        $hotel->fill(['duration_night'=>$data->duration_night]);
        $hotel->fill(['city_id'=>$data->city_id]);       
        $hotel->save();  
        return $hotel;
    }

    public function deleteHotelById($hotelId)
    {
          Booking::where("tour_id","=",$hotelId)->get()->each->delete();
          Hotel_image::where("hotel_id","=",$hotelId)->get()->each->delete();
          $tour=Tour::where("hotel_id","=",$hotelId)->get();
          foreach($tour as $tou){
          $tou->hotel_id=null;
          $tou->save();
          }
        $hotel=$this->hotel->find($hotelId);
        $photoName=$hotel->image;
        File::delete('public/assets/images/hotels/'.$photoName);
        $this->deleteItemById($hotelId,$this->hotel);
    }
     
static public function gethotels()
    {
        return Hotel::all();
    }
   
}