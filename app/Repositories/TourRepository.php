<?php

namespace App\Repositories;

use App\Entities\Tour;
use App\Entities\Tour_Review;
use App\Entities\Tour_Detail;
use App\Entities\Tour_Image;
use App\Entities\tours_forign;
use App\Entities\TourTranslation;
use App\Entities\Tour_DetailTranslation;
use App\Entities\Booking;
use App\Entities\Hotel;
use File;

class TourRepository extends BaseRepository
{
    protected $tour;
    public function __construct()
    {
        $this->tour=new Tour();
    }
    public function getAllTours()
    {
        return $this->getAllItems($this->tour);
    }
   
    public function postAddTour($data,$tour)
    {
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            $tour->fill(['image'=>$picture]);
        }
        
        foreach (\Config::get('languages') as $locale=>$language) 
        {           
            $tour->{"name:$locale"}   = $data->input("name:{$locale}");
            $tour->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $tour->{"long_description:$locale"} = $data->input("long_description:{$locale}");
            $tour->{"included:$locale"} = $data->input("included:{$locale}");
            $tour->{"not_included:$locale"} = $data->input("not_included:{$locale}");
         
        }           
        $tour->fill(['price'=>$data->price]);
        $tour->fill(['category_id'=>$data->category_id]);
        $tour->fill(['start_date'=>$data->start_date]);
        $tour->fill(['end_date'=>$data->end_date]);
        $tour->fill(['availability'=>$data->availability]);
        $tour->fill(['adults'=>$data->adults]);
        $tour->fill(['childern'=>$data->childern]);
         $tour->fill(['beds'=>$data->beds]);
        $tour->fill(['duration_day'=>$data->duration_day]);
        $tour->fill(['duration_night'=>$data->duration_night]); 
        $tour->fill(['active'=>$data->active]);
         $tour->fill(['offer_id'=>$data->offer_id]);
        $tour->save();
        for ($i=0; $i < count($data->hotel_id) ; $i++) { 
            $tourforign = new tours_forign();
            $tourforign->fill(['hotel_id'=>$data->hotel_id[$i]]);
            
            $hotel=Hotel::find($data->hotel_id[$i]);
            $city_id=$hotel->city_id;
            $tourforign->fill(['City_id'=>$city_id]);
           
            $tour=Tour::all()->last();
            $tourforign->fill(['tour_id'=>$tour->id]);
          
            $tourforign->save();
         }
    }
    public function getTourById($tourId)
    {
        return $this->getItemById($tourId,$this->tour);
    }
    public function updateToureById($tourId,$data)
    {
        // return $data;
        $tour=$this->tour->find($tourId);
      
        if ($data->hasFile('image'))
        {
            $photoName=$tour->image;
            File::delete('public/assets/images/tours/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            $tour->fill(['image'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $tour->{"name:$locale"}   = $data->input("name:{$locale}");
            $tour->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $tour->{"long_description:$locale"} = $data->input("long_description:{$locale}");
            $tour->{"included:$locale"} = $data->input("included:{$locale}");
            $tour->{"not_included:$locale"} = $data->input("not_included:{$locale}");
        }    
        $tour->fill(['price'=>$data->price]);
        $tour->fill(['category_id'=>$data->category_id]);
        $tour->fill(['start_date'=>$data->start_date]);
        $tour->fill(['end_date'=>$data->end_date]);
        $tour->fill(['availability'=>$data->availability]);
        $tour->fill(['adults'=>$data->adults]);
        $tour->fill(['childern'=>$data->childern]);
         $tour->fill(['beds'=>$data->beds]);
        $tour->fill(['duration_day'=>$data->duration_day]);
        $tour->fill(['duration_night'=>$data->duration_night]);
        $tour->fill(['active'=>$data->active]);
         $tour->fill(['offer_id'=>$data->offer_id]);
        $tour->save();  
         tours_forign::where("tour_id","=",$tourId)->get()->each->delete();
        for ($i=0; $i < count($data->hotel_id) ; $i++) { 
            $tourforign = new tours_forign();
            $tourforign->fill(['hotel_id'=>$data->hotel_id[$i]]);
            
            $hotel=Hotel::find($data->hotel_id[$i]);
            $city_id=$hotel->city_id;
            $tourforign->fill(['City_id'=>$city_id]);
           
           
            $tourforign->fill(['tour_id'=>$tourId]);
          
            $tourforign->save();
         }
    }

    public function deleteTourById($tourId)
    {
       
       
        Tour_Review::where("tour_id","=",$tourId)->get()->each->delete();
       // $tour_detail=Tour_Detail::where("tour_id","=",$tourId)->first();
     //  print_r($tour_detail->id);
       // Tour_DetailTranslation::where("tour__detail_id","=",$tour_detail->id)->get()->each->delete();
         Tour_Detail::where("tour_id","=",$tourId)->get()->each->delete();
      // $tour_image=Tour_image::all()->where("tour_id","=",$tourId);
         Booking::where("tour_id","=",$tourId)->get()->each->delete();
          Tour_image::where("tour_id","=",$tourId)->get()->each->delete();
         tours_forign::where("tour_id","=",$tourId)->get()->each->delete();
         $tour=$this->tour->find($tourId);
        $photoName=$tour->image;
       File::delete('public/assets/images/tours/'.$photoName);
        $this->deleteItemById($tourId,$this->tour);
       // TourTranslation::where("tour_id","=",$tourId)->get()->each->delete();
    }

   
}