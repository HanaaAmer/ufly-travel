<?php

namespace App\Repositories;

use App\Entities\About_Goals;
use Auth;
use File;

class AboutGoalsRepository extends BaseRepository
{
    protected $aboutgoals;
    public function __construct()
    {
        $this->aboutgoals=new About_Goals();
    }
    public function getAllAboutgoals()
    {
        return $this->getAllItems($this->aboutgoals);
    }
    public function postAddaboutgoals($data,$aboutgoals)
    {
        

  
        $aboutgoals->fill(['goal'=>$data->goal]);
        
        $aboutgoals->save();
        return $aboutgoals;
    }
   
    public function getaboutgoalsById($aboutgoalsId)
    {
        return $this->getItemById($aboutgoalsId,$this->aboutgoals);
    }
    public function updateaboutgoalsgById($aboutgoalsId,$data)
    {
     
        $aboutgoals=$this->aboutgoals->find($aboutgoalsId);
      
        $aboutgoals->fill(['goal'=>$data->goal]);
       $aboutgoals->save();
       
        return $aboutgoals; 
      
    }
   
  
    public function deleteaboutgoalsById($aboutgoalsId)
    {
        $aboutgoals=$this->aboutgoals->find($aboutgoalsId);

        $this->deleteItemById($aboutgoalsId,$this->aboutgoals);
    }
    
     
    
    
}