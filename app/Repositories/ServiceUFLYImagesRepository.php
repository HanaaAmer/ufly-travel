<?php

namespace App\Repositories;

use File;
use App\Entities\TouristServices_Images;

class ServiceUFLYImagesRepository extends BaseRepository
{
    protected $ServiceImage;

    public function __construct()
    {
        $this->ServiceImage = new TouristServices_Images();
    }

    public function getAllServiceImage()
    {
        return $this->getAllItems($this->ServiceImage);
    }

    public function postAddServiceImage($data, $ServiceImage)
    {
        $req = [];
        if ($data->hasFile('image')) {
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/services';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }

        $ServiceImage->image = ($req);
        $ServiceImage->fill(['tourist_services_id' => $data->tourist_services_id]);
        $ServiceImage->save();

        return $ServiceImage;
    }

    public function getServiceImageById($ServiceImageId)
    {
        return $this->getItemById($ServiceImageId, $this->ServiceImage);
    }

    public function updateServiceImageById($ServiceImageId, $data)
    {
        $req = []; 
         $photoName = [];
      
        $ServiceImage = $this->ServiceImage->find($ServiceImageId);  
        $photoName = $ServiceImage->image;
        if( $ServiceImage->image != null)
       {
        if ($data->hasFile('image')) {
           
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/services';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }
        
            $ServiceImage->image = array_merge($ServiceImage->image,$req) ;
        }
        
        elseif( $ServiceImage->image == []){
           
          foreach ($photoName as $imagepath) {
                File::delete('public/assets/images/services/'.$imagepath);
            }
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/services';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
            $ServiceImage->image = $req;
       }
        else {
            $ServiceImage->image = $photoName;
        }
        $ServiceImage->fill(['tourist_services_id' => $data->tourist_services_id]);
        $ServiceImage->save();
    }

    public function deleteServiceImageById($ServiceImageId)
    {
        $ServiceImage = $this->ServiceImage->find($ServiceImageId);
         foreach($ServiceImage->image as $imagepath)
            {
            
            File::delete('public/assets/images/services/'.$imagepath);
        }
        $this->deleteItemById($ServiceImageId, $this->ServiceImage);
    }
}
