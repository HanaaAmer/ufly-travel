<?php

namespace App\Repositories;
use File;
use App\Entities\Offer;
use App\Entities\OfferTranslation;
class OfferRepository extends BaseRepository
{
    protected $offer;

    public function __construct()
    {
        $this->offer = new Offer();
      
        $this->offertranslation= new OfferTranslation();
    }

    public function getAlloffer()
    {
        return $this->getAllItems($this->offer);
    }

    public function postAddoffer($data, $offer)
    {
     
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $offer->{"offer:$locale"}   = $data->input("offer:{$locale}");
           
        }
      
       
        $offer->save();

        return $offer;
    }

    public function getofferById($offerId)
    {
        return $this->getItemById($offerId, $this->offer);
    }

    public function updateofferById($offerId, $data)
    {
      
        $offer = $this->offer->find($offerId);
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $offer->{"offer:$locale"}   = $data->input("offer:{$locale}");
           
        }
        
        $offer->save();

        return $offer;
    }

    public function deleteofferById($offerId)
    {
       
           $offertranslation = OfferTranslation::all()->where('offer_id', '=', $offerId);
          foreach($offertranslation as $translation){
       $this->deleteItemById($translation->id, $this->offertranslation);
          }
        $offer = $this->offer->find($offerId);
        $this->deleteItemById($offerId, $this->offer);
        
    }
      static public function getoffers()
    {
        return Offer::all();
    }
    
}
