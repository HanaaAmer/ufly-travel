<?php

namespace App\Repositories;

use App\Entities\AboutUs;
use Auth;
use File;

class AboutRepository extends BaseRepository
{
    protected $about;
    public function __construct()
    {
        $this->about=new AboutUs();
    }
    public function getAllAboutus()
    {
        return $this->getAllItems($this->about);
    }
   
    public function getaboutusById($aboutusId)
    {
        return $this->getItemById($aboutusId,$this->about);
    }
    public function updateaboutusgById($aboutusId,$data)
    {
     
        $about=new AboutUs();
       
        return $about; 
      
    }
   
    static public function getaboutusValue($aboutusName)
    {
        return AboutUs::where('id',1)->first()[$aboutusName];
    }

    
     
    
    
}