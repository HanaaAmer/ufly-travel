<?php

namespace App\Repositories;

use File;
use App\Entities\cities;
use App\Entities\tourism_places;
use App\Entities\tourism_placesTranslation;
class tourism_placesRepository extends BaseRepository
{
    protected $place;

    public function __construct()
    {
        $this->place = new tourism_places();
        $this->placetranslation = new tourism_placesTranslation();
       
    }

    public function getAllplaces()
    {
        return $this->getAllItems($this->place);
    }

    public function postAddplaces($data, $place)
    {
        foreach (\Config::get('languages') as $locale => $language) {
            $place->{"title:$locale"} = $data->input("title:{$locale}");
            $place->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $place->{"long_description:$locale"} = $data->input("long_description:{$locale}");
        }
        if ($data->hasFile('image')) {
            $file = $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/Places';
            $file->move($destinationPath, $picture);
            $place->fill(['image' => $picture]);
        }
        $req = [];
        if ($data->hasFile('image_slider')) {
            foreach ($data->file('image_slider') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/Places';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }

        $place->image_slider = ($req);

        $place->fill(['City_id' => $data->City_id]);

        $place->save();

        return $place;
    }

    public function getplacesById($placeId)
    {
        return $this->getItemById($placeId, $this->place);
    }

    public function updateplacesById($placeId, $data)
    {
        $place = $this->place->find($placeId);

        foreach (\Config::get('languages') as $locale => $language) {
            $place->{"title:$locale"} = $data->input("title:{$locale}");
            $place->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $place->{"long_description:$locale"} = $data->input("long_description:{$locale}");
        }
        if ($data->hasFile('image')) {
            $photoName = $place->image;
            File::delete('public/assets/images/Places/'.$photoName);
            $file = $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/Places';
            $file->move($destinationPath, $picture);
            $place->fill(['image' => $picture]);
        }
        $place->fill(['place' => $data->place]);
        $place->fill(['City_id' => $data->City_id]);
        $req=[];
        $photoName = $place->image_slider;
        if ($place->image_slider != null) {
            if ($data->hasFile('image_slider')) {
                foreach ($data->file('image_slider') as $image) {
                    $filename = $image->getClientOriginalName();
                    $extension = $image->getClientOriginalExtension();
                    $picture = date('His').$filename;
                    $destinationPath = 'public/assets/images/Places';
                    $image->move($destinationPath, $picture);
                    $req[] = $picture;
                }
            }

            $place->image_slider = array_merge($place->image_slider, $req);
        } elseif ($place->image_slider == []) {
            foreach ($photoName as $imagepath) {
                File::delete('public/assets/images/Places/'.$imagepath);
            }
            foreach ($data->file('image_slider') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/Places';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
            $place->image_slider = $req;
        } else {
            $place->image_slider = $photoName;
        }
        $place->save();

        return $place;
    }

    public function deleteplacesById($placeId)
    {
        $place = $this->place->find($placeId);
        $placetranslation = tourism_placesTranslation::all()->where('tourism_places_id', '=', $placeId);
        foreach($placetranslation as $translation){
     $this->deleteItemById($translation->id, $this->placetranslation);
        }
        File::delete('public/assets/images/Places/'.$place->image);
        foreach($place->image_slider as $imagepath)
        {
        
        File::delete('public/assets/images/Places/'.$imagepath);
    }

        $this->deleteItemById($placeId, $this->place);
    }
}
