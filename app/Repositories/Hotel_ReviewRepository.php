<?php

namespace App\Repositories;

use App\Entities\Hotel_Review;

use File;

class Hotel_ReviewRepository extends BaseRepository
{
    protected $hotel_review;
    public function __construct()
    {
        $this->hotel_review=new Hotel_Review();
    }
    public function getAllHotel_Review()
    {
        return $this->getAllItems($this->hotel_review);
    }
   
    public function postAddHotel_Review($data,$hotel_review)
    {
        
        $hotel_review->fill(['fullname'=>$data->fullname]);
        $hotel_review->fill(['review'=>$data->review]); 
        $hotel_review->fill(['Accomodation'=>$data->Accomodation]);      
        $hotel_review->fill(['Meals'=>$data->Meals]); 
        $hotel_review->fill(['Value_For_Money'=>$data->Value_For_Money]); 
        $hotel_review->fill(['Destination'=>$data->Destination]); 
        $hotel_review->fill(['Transport'=>$data->Transport]);
        $hotel_review->fill(['active'=>$data->active]);
        $hotel_review->fill(['hotel_id'=>$data->hotel_id]);
        $hotel_review->save();
        return $hotel_review;
    }
    public function getHotel_ReviewById($hotel_reviewId)
    {
        return $this->getItemById($hotel_reviewId,$this->hotel_review);
    }
   
    public function deleteHotel_ReviewById($hotel_reviewId)
    {
        $hotel_review=$this->hotel_review->find($hotel_reviewId);      
        $this->deleteItemById($hotel_reviewId,$this->hotel_review);
    }

   
}