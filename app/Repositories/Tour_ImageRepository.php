<?php

namespace App\Repositories;

use App\Entities\Tour_Image;

use File;

class Tour_ImageRepository extends BaseRepository
{
    protected $tour_image;
    public function __construct()
    {
        $this->tour_image=new Tour_Image();
    }
    public function getAllTour_Image()
    {
        return $this->getAllItems($this->tour_image);
    }
   
    public function postAddTour_Image($data,$tour_image)
    {
        $files=[];
        if ($data->hasFile('image'))
        {            
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];          
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);

            }    
        }
      
     //   $tour_image->fill(['city_id'=>$data->city_id]);
        $tour_image->fill(['tour_id'=>$data->tour_id]);
        $tour_image->fill(['image'=>$files]);
        $tour_image->save();
        return $tour_image;
    }
    public function getTour_ImageById($tour_imageId)
    {
        return $this->getItemById($tour_imageId,$this->tour_image);
    }
    public function updateTour_ImageById($tour_imageId,$data)
    {
        // return $data;
        $tour_image=$this->tour_image->find($tour_imageId);
        $files=[];
       
            $photoName=[];
            $photoName=$tour_image->image;
            if ($data->hasFile('image'))
        {
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];  
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/tours';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);
            }
        }
          $files = array_merge($files,$photoName) ;       
      //  $tour_image->fill(['city_id'=>$data->city_id]);
        $tour_image->fill(['tour_id'=>$data->tour_id]);
        $tour_image->fill(['image'=>$files]);
        $tour_image->save();  
        return $tour_image;
    }

    public function deleteTour_ImageById($tour_imageId)
    {
        $tour_image=$this->tour_image->find($tour_imageId);
        $photoName=$tour_image->image;
        for($i=0; $i<count($photoName);$i++){
            File::delete('public/assets/images/tours/'.$photoName[$i]);
            }
        $this->deleteItemById($tour_imageId,$this->tour_image);
    }

   
}