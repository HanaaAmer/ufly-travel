<?php

namespace App\Repositories;

use App\Entities\Tour_Review;

use File;

class Tour_ReviewRepository extends BaseRepository
{
    protected $tour_review;
    public function __construct()
    {
        $this->tour_review=new Tour_Review();
    }
    public function getAllTour_Review()
    {
        return $this->getAllItems($this->tour_review);
    }
   
    public function postAddTour_Review($data,$tour_review)
    {
        
        $tour_review->fill(['fullname'=>$data->fullname]);
        $tour_review->fill(['review'=>$data->review]); 
        $tour_review->fill(['Accomodation'=>$data->Accomodation]);      
        $tour_review->fill(['Meals'=>$data->Meals]); 
        $tour_review->fill(['Value_For_Money'=>$data->Value_For_Money]); 
        $tour_review->fill(['Destination'=>$data->Destination]); 
        $tour_review->fill(['Transport'=>$data->Transport]);
        $tour_review->fill(['active'=>$data->active]);
        $tour_review->fill(['tour_id'=>$data->tour_id]);
        $tour_review->save();
        return $tour_review;
    }
    public function getTour_ReviewById($tour_reviewId)
    {
        return $this->getItemById($tour_reviewId,$this->tour_review);
    }
    public function updateTour_ReviewById($tour_reviewId,$data)
    {
        $tour_review=$this->tour_review->find($tour_reviewId);
        $tour_review->fill(['active'=>$data->active]);
        $tour_review->save();
        return $tour_review;
    }
    public function deleteTour_ReviewById($tour_reviewId)
    {
        $tour_review=$this->tour_review->find($tour_reviewId);
        $photoName=$tour_review->image;
        File::delete('public/assets/images/tours/'.$photoName);
        $this->deleteItemById($tour_reviewId,$this->tour_review);
    }

   
}