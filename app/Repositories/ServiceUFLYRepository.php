<?php

namespace App\Repositories;

use File;
use App\Entities\TouristServices;
use App\Entities\TouristServicesTranslation;
use App\Entities\TouristServices_Images;
class ServiceUFLYRepository extends BaseRepository
{
    protected $Service;

    public function __construct()
    {
        $this->Service = new TouristServices();
        $this->Serviceimage= new TouristServices_Images();
        $this->Servicetranslation= new TouristServicesTranslation();
    }

    public function getAllServicesufly()
    {
        return $this->getAllItems($this->Service);
    }

    public function postAddServiceufly($data, $Service)
    {
        if ($data->hasFile('image')) {
            $file = $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/services';
            $file->move($destinationPath, $picture);
            $Service->fill(['image' => $picture]);
        }
        if ($data->hasFile('image_services')) {
            $file = $data->file('image_services');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/services';
            $file->move($destinationPath, $picture);
            $Service->fill(['image_services' => $picture]);
        }
        foreach (\Config::get('languages') as $locale => $language) {
            $Service->{"name:$locale"} = $data->input("name:{$locale}");
            $Service->{"descrption:$locale"} = $data->input("descrption:{$locale}");
            $Service->{"services:$locale"} = $data->input("services:{$locale}");
            $Service->{"title:$locale"} = $data->input("title:{$locale}");
            $Service->{"long_descrption:$locale"} = $data->input("long_descrption:{$locale}");
        }

        $Service->save();

        return $Service;
    }

    public function getServiceByIdufly($ServiceId)
    {
        return $this->getItemById($ServiceId, $this->Service);
    }

    public function updateServiceByIdufly($ServiceId, $data)
    {
        $Service = $this->Service->find($ServiceId);
        if ($data->hasFile('image')) {
            $photoName = $Service->image;
            File::delete('public/assets/images/services/'.$photoName);
            $file = $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/services';
            $file->move($destinationPath, $picture);
            $Service->fill(['image' => $picture]);
        }
        if ($data->hasFile('image_services')) {
            $photoName = $Service->image_services;
            File::delete('public/assets/images/services/'.$photoName);
            $file = $data->file('image_services');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = 'public/assets/images/services';
            $file->move($destinationPath, $picture);
            $Service->fill(['image_services' => $picture]);
        }
        foreach (\Config::get('languages') as $locale => $language) {
            $Service->{"name:$locale"} = $data->input("name:{$locale}");
            $Service->{"descrption:$locale"} = $data->input("descrption:{$locale}");
            $Service->{"services:$locale"} = $data->input("services:{$locale}");
            $Service->{"title:$locale"} = $data->input("title:{$locale}");
            $Service->{"long_descrption:$locale"} = $data->input("long_descrption:{$locale}");
        }

        $Service->save();

        return $Service;
    }

    public function deleteServiceByIdufly($ServiceId)
    {
          $Serviceimage = TouristServices_Images::all()->where('tourist_services_id', '=', $ServiceId);
          foreach($Serviceimage as $image){
       $this->deleteItemById($image->id, $this->Serviceimage);
          }
           $Servicetranslation = TouristServicesTranslation::all()->where('tourist_services_id', '=', $ServiceId);
          foreach($Servicetranslation as $translation){
       $this->deleteItemById($translation->id, $this->Servicetranslation);
          }
        $Service = $this->Service->find($ServiceId);

        $this->deleteItemById($ServiceId, $this->Service);
    }
      static public function getservicesufly()
    {
        return TouristServices::all();
    }
}
