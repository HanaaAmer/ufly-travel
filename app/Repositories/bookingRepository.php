<?php
namespace App\Repositories;
use App\Entities\Booking;
use App\Entities\IndividualBooking;
use App\Entities\individual_bookingsforign;
use App\Entities\Hotel;
use Carbon\Carbon;
use File;
use DB;
/**
 * Class UserRepository
 * @package App\Repositories
 */
class bookingRepository extends BaseRepository {
    protected $booking;
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->booking=new Booking();
         $this->individualbooking=new IndividualBooking();
    }
    /**
     * @return array of all Ads
     */
    public function getAllbooking(){ 
        return $this->getAllItems($this->booking);
    }
  
    public function postAddbooking($data,$booking){
         
        $booking->fill(['fullname'=>$data->fullname]);
        $booking->fill(['email'=>$data->email]);
        $booking->fill(['phone'=>$data->phone]);
        $booking->fill(['Adult_Number'=>$data->Adult_Number]);  
        $booking->fill(['Childerns_Number'=>$data->Childerns_Number]);
        $booking->fill(['Infant_Number'=>$data->Infant_Number]);
      
        $booking->fill(['note'=>$data->note]);
        if($data->tour_id != null){
          $booking->fill(['tour_id'=>$data->tour_id]);
        }
        elseif($data->hotel_id != null){
          $booking->fill(['hotel_id'=>$data->hotel_id]);
        }
        $booking->save();
           return $booking;
    }
     public function postAddindividualbooking($data,$individualbooking){
         
        $individualbooking->fill(['fullname'=>$data->fullname]);
        $individualbooking->fill(['email'=>$data->email]);
        $individualbooking->fill(['phone'=>$data->phone]);
        $individualbooking->fill(['Travelers_Number'=>$data->Travelers_Number]);  
        $individualbooking->fill(['Number_of_Days'=>$data->Number_of_Days]);
        $individualbooking->fill(['Arrival_Date'=>$data->Arrival_Date]);
      
        $individualbooking->fill(['note'=>$data->note]);
       
          
        
        $individualbooking->save();
        for ($i=0; $i < count($data->hotel_id) ; $i++) { 
            $individual = new individual_bookingsforign();
            $individual->fill(['hotel_id'=>$data->hotel_id[$i]]);
            
            $hotel=Hotel::find($data->hotel_id[$i]);
            $city_id=$hotel->city_id;
            $individual->fill(['City_id'=>$city_id]);
           
            $booking=IndividualBooking::all()->last();
            $individual->fill(['individual_id'=>$booking->id]);
          
            $individual->save();
         }
    }
   
 public function deleteBookingById($bookingId)
    {
        $booking =  $this->booking->find($bookingId);

        $this->deleteItemById($bookingId, $this->booking);
    }
     public function deleteindividualBookingById($individualbookingId)
    {
        
        IndividualBooking::where("individual_id","=",$individualbookingId)->get()->each->delete();
        $individualbooking =  $this->individualbooking->find($individualbookingId);

        $this->deleteItemById($individualbookingId, $this->individualbooking);
    }
   
  
}