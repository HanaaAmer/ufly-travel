<?php

namespace App\Repositories;
use File;
use App\Entities\Cities;
use App\Entities\city_images;

class city_imagesRepository extends BaseRepository
{
    protected $CityImage;

    public function __construct()
    {
        $this->CityImage = new city_images();
    }

    public function getAllCityImages()
    {
        return $this->getAllItems($this->CityImage);
    }

    public function postAddCityImages($data, $CityImage)
    {
       
        $req=[];
        if ($data->hasFile('image') )
        {
            foreach($data->file('image') as $image)
            {
            
           
            $filename = $image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/Cities';
            $image->move($destinationPath, $picture);
            $req[] =  $picture;  
         
        }
       
        
             
    } 
  
    $CityImage->image=( $req);
        $CityImage->fill(['City_id' => $data->City_id]);
            $CityImage->save();

            return $CityImage;
             
        
    }

    public function getCityImagesById($CityImageId)
    {
        return $this->getItemById($CityImageId, $this->CityImage);
    }

    public function updateCityImagesById($CityImageId, $data)
    {
       
        $req=[];
        $photoName= [];
        $CityImage = $this->CityImage->find($CityImageId);
          $photoName = $CityImage->image;
        if( $CityImage->image != null)
       {
        if ($data->hasFile('image')) {
           
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/Cities';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }
        
            $CityImage->image = array_merge($CityImage->image,$req) ;
        }
        
        elseif( $CityImage->image == []){
           
          foreach ($photoName as $imagepath) {
                File::delete('public/assets/images/Cities/'.$imagepath);
            }
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/Cities';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
            $CityImage->image = $req;
       }
        else {
            $CityImage->image = $photoName;
        }
       
      $CityImage->fill(['City_id' => $data->City_id]);
      $CityImage->save();

    
     
    }

    public function deleteCityImagesById($CityImageId)
    {
        $CityImage = $this->CityImage->find($CityImageId);
            foreach($CityImage->image as $imagepath)
            {
            
            File::delete('public/assets/images/Cities/'.$imagepath);
        }
        $this->deleteItemById($CityImageId, $this->CityImage);
        
    }
}
