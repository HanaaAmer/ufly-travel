<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class Tour_DetailTranslation extends Model
{
    protected $fillable=['title', 'description'];
    protected $table='tourdetail_translation';
   
   
}
