<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable=['id', 'fullname', 'email', 'phone', 'Adult_Number','Childerns_Number','Infant_Number','tour_id','hotel_id','note'];
    protected $primaryKey='id';
    protected $table='booking';
    public function Tour(){
        return $this->belongsTo(Tour::class,'tour_id','id');
    }
    public function Hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
}
