<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Hotel_Review;

class Hotel_Review extends Model
{

    protected $fillable = [ 'fullname','review','Accomodation','Meals','Value_For_Money','Destination','Transport','active','hotel_id'];
    protected $table = 'hotel_review';
    protected $primaryKey = 'id';
   
    public function Hotels(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
  
   
}