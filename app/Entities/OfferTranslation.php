<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{
    protected $fillable=['offer'];
    protected $table='tours_offers_translation';
   
   
}