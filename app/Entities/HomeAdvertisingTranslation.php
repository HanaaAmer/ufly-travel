<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Home_AdvertisingTranslation extends Model
{
    protected $fillable=['descrption'];
    
    
    protected $table='home_advertising_translation';
   
}