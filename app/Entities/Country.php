<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];
    protected $fillable = [
      'id',
        'iso'      
    ];
    protected $table='countries';
    protected $primaryKey='id';
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%");
                
});            
        }
        return $query;
    }
}    