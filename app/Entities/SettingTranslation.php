<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{

    protected $fillable=['contactus_description','description'];
    protected $table='contactinfo_translation';
   
}
