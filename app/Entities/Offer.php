<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use Translatable;
    public $translatedAttributes = ['offer'];
    protected $fillable=['id'];
    protected $primaryKey='id';
    protected $table='tours_offers';
    
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("offer", "LIKE","%$keyword%");
                
});            
        }
        return $query;
    }
   
}
