<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class TourTranslation extends Model
{
    protected $fillable=['name', 'short_description','long_description','included','not_included'];
    protected $table='tour_translation';
    protected $casts = [
        'included' => 'array',
        'not_included'=>'array',
    ];
   
}
