<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Hotel;
use Dimsav\Translatable\Translatable;
use App\Entities\Category;
use App\Entities\Offer;

class Tour extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'short_description', 'long_description','included','not_included'];
    protected $fillable = [ 'image','price','category_id','start_date','end_date','availability','adults','childern','duration_day','duration_night','city_id','hotel_id','beds','active','offer_id'];
    protected $table = 'tours';
    protected $primaryKey = 'id';
    protected $casts = [
        'included' => 'array',
        'not_included'=>'array',
    ];
    public function hotels(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
    public function cites(){
        return $this->belongsTo(Cities::class,'city_id','id');
        
    }
    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
     public function offer(){
        return $this->belongsTo(Offer::class,'offer_id','id');
    }
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%")
                    ->orWhere("services", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}