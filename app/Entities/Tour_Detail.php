<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Entities\Tour_Detail;

class Tour_Detail extends Model
{
    use Translatable;
    public $translatedAttributes = ['title','description'];
    protected $fillable = [ 'image','city_id','tour_id'];
    protected $table = 'tour_details';
    protected $primaryKey = 'id';
   
    public function Tours(){
        return $this->belongsTo(Tour::class,'tour_id','id');
    }
    public function cites(){
        return $this->belongsTo(Cities::class,'city_id','id');
        
    }
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
                    
            });
        }
        return $query;
    }
   
}