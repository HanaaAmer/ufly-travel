<?php

namespace App\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class TouristServices extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'descrption', 'title', 'services', 'long_descrption'];
    protected $fillable = ['id', 'image', 'image_services'];
    protected $primaryKey = 'id';
    protected $table = 'tourist_services';

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $query->where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', "%$keyword%")
                ->orWhere('descrption', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('services', 'LIKE', "%$keyword%")
                ->orWhere('long_descrption', 'LIKE', "%$keyword%");
            });
        }

        return $query;
    }

    protected $casts = [
        'services' => 'array', ];
}
