<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class IndividualBooking extends Model
{
    protected $fillable=['id', 'fullname', 'email', 'phone', 'Travelers_Number','Number_of_Days','Arrival_Date','note'];
    protected $primaryKey='id';
    protected $table='individual_bookings';
 
}
