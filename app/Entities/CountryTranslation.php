<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{
    protected $fillable=['name'];
    protected $table='country_translation';
   
}
