<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    use Translatable;
    public $translatedAttributes = ['title',  'description',  'vision','about_goals'];
    protected $fillable = [
        'id',
       'image_description',
       'image_vision',
       'image_goals'
    ];
    protected $table='about_us';
    protected $primaryKey='id';
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                ->orWhere("description", "LIKE", "%$keyword%")
                ->orWhere("vision_mission", "LIKE", "%$keyword%")
                ->orWhere("about_goals", "LIKE", "%$keyword%");
               
});            
        }
        return $query;
    }
    protected $casts = [
        'about_goals'=>'array', ];
}    