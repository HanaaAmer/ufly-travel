<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Home_explooreTranslation extends Model
{
    protected $fillable=['title',  'description',  'EXPLOORE'];
    
    
    protected $table='home_exploore_translation';
    protected $casts = [
        'EXPLOORE'=>'array', ];
}