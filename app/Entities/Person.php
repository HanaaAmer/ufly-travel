<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'number',
        'birth_date',
        'joining_date',
        'job_id',
        ];
    protected $table = "persons";
            
    public function Job() 
    {
        return $this->belongsTo(Job::class);
    }
}
