<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Home_exploore extends Model
{
    use Translatable;
    public $translatedAttributes = ['title',  'description',  'EXPLOORE'];
    protected $fillable = [
        'id',
       'image',
    ];
    protected $table='home_exploore';
    protected $primaryKey='id';
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                ->orWhere("description", "LIKE", "%$keyword%")
              
                ->orWhere("EXPLOORE", "LIKE", "%$keyword%");
               
});            
        }
        return $query;
    }
    protected $casts = [
        'EXPLOORE'=>'array', ];
}    