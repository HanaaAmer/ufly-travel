<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class tourism_placesTranslation extends Model
{
    protected $fillable = ['title', 'short_description', 'long_description'];
    protected $table = 'tourism_places_translation';
    protected $casts = [
        'image_slider' => 'array', ];
}
