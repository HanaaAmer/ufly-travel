<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Tour_Image;

class Tour_Image extends Model
{
    protected $fillable = [ 'image','tour_id','city_id'];
    protected $table = 'tour_images';
    protected $primaryKey = 'id';
    protected $casts = [
        'image' => 'array',
    ];
    public function Tours(){
        return $this->belongsTo(Tour::class,'tour_id','id');
    }
    public function cites(){
        return $this->belongsTo(Cities::class,'city_id','id');
        
    }
   
}