<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Tour_Review;

class Tour_Review extends Model
{

    protected $fillable = [ 'fullname','review','Accomodation','Meals','Value_For_Money','Destination','Transport','active','tour_id'];
    protected $table = 'tour_review';
    protected $primaryKey = 'id';
   
    public function Tours(){
        return $this->belongsTo(Tour::class,'tour_id','id');
    }
  
   
}