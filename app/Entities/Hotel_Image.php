<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Hotel_Image;

class Hotel_Image extends Model
{
    protected $fillable = [ 'image','hotel_id','city_id'];
    protected $table = 'hotel_images';
    protected $primaryKey = 'id';
    protected $casts = [
        'image' => 'array',
    ];
    public function hotels(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
    public function cites(){
        return $this->belongsTo(Cities::class,'city_id','id');
        
    }
   
}