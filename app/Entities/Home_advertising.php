<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Home_advertising extends Model
{
    use Translatable;
    public $translatedAttributes = ['descrption'];
    protected $fillable = [
        'id',
       'link',
     
    ];
    protected $table='home_advertising';
    protected $primaryKey='id';
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("descrption", "LIKE","%$keyword%");
               
               
});            
        }
        return $query;
    }
   
}    