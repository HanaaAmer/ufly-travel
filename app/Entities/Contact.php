<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable=['id', 'firstname','lastname', 'email', 'phone', 'message'];
    protected $primaryKey='id';
    protected $table='contactus';
}
