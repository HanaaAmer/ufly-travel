<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class HotelTranslation extends Model
{
    protected $fillable=['name', 'short_description','long_description', 'services','Amenities'];
    protected $table='hotels_translation';
    protected $casts = [
        'services' => 'array',
        'Amenities'=>'array',
    ];
   
}
