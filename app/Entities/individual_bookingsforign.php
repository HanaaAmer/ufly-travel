<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class individual_bookingsforign extends Model
{
    protected $fillable=['id','City_id','hotel_id','individual_id'];
    protected $primaryKey='id';
    protected $table='individual_bookingsforign';
    public function individual_bookings(){
        return $this->belongsTo(individual_bookings::class,'individual_id','id');
    } 
 public function Cities(){
        return $this->belongsTo(Cities::class,'City_id','id');
    } 
    
    public function Hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
 
}
