<?php

namespace App\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class tourism_places extends Model
{
    use Translatable;
    public $translatedAttributes = ['title', 'short_description', 'long_description'];
    protected $fillable = ['id', 'image', 'image_slider', 'City_id'];
    protected $primaryKey = 'id';
    protected $table = 'tourism_places';

    public function cities()
    {
        return $this->belongsTo(Cities::class, 'City_id', 'id');
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $query->where(function ($query) use ($keyword) {
                $query->where('title', 'LIKE', "%$keyword%")
                ->orWhere('short_description', 'LIKE', "%$keyword%")
                ->orWhere('long_description', 'LIKE', "%$keyword%");
            });
        }

        return $query;
    }

    protected $casts = [
        'image_slider' => 'array', ];
}
