<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'descrption', 'title','long_descrption'];
    protected $fillable=['id', 'image', 'country_id'];
    protected $primaryKey='id';
    protected $table='cities';
    public function Country(){
        return $this->belongsTo(Country::class,'country_id','id');
    }
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                ->orWhere("descrption", "LIKE", "%$keyword%")
                ->orWhere("title", "LIKE", "%$keyword%")
                ->orWhere("long_descrption", "LIKE", "%$keyword%");
});            
        }
        return $query;
    }
   
}
