<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TouristServices_Images extends Model
{
    protected $fillable = ['id', 'image', 'tourist_services_id'];

    protected $primaryKey = 'id';
    protected $casts = [
        'image' => 'array', ];
    protected $table = 'tourist_services_images';

    public function TouristServices()
    {
        return $this->belongsTo(TouristServices::class, 'tourist_services_id', 'id');
    }
}
