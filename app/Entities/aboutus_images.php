<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class aboutus_images extends Model
{ 
  
    protected $fillable=['id', 'image'];
   
    protected $primaryKey='id';
    protected $casts = [
        'image'=>'array', ];
      protected $table='aboutus_images';
   
   
}
