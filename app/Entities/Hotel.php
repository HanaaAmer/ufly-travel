<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Entities\Cities;
use App\Entities\Category;


class Hotel extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'short_description', 'long_description', 'services','Amenities'];
    protected $fillable = ['image','price','category_id','duration_day','duration_night','city_id','availability','adults','childern','capacity','beds','map_lat','map_lng'];
    protected $table = 'hotels';
    protected $primaryKey = 'id';
    protected $casts = [
        'services' => 'array',
        'Amenities'=>'array',
    ];
    public function cites(){
        return $this->belongsTo(Cities::class,'city_id','id');
    }
    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%")
                    ->orWhere("services", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
   
}