<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class AboutUsTranslation extends Model
{
    protected $fillable=['title','description','vision','about_goals'];
    
    
    protected $table='aboutus_translation';
    protected $casts = [
        'about_goals'=>'array', ];
}