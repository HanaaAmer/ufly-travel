<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TouristServicesTranslation extends Model
{
    protected $fillable = ['name', 'descrption', 'title', 'services', 'long_descrption'];
    protected $table = 'tourist_services_translation';
    protected $casts = [
        'services' => 'array', ];
}
