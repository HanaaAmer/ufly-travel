<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class home_image extends Model
{ 
  
    protected $fillable=['id', 'image_slider','image_tour','image_hotel'];
   
    protected $primaryKey='id';
   
      protected $table='home_image';
   
   
}
