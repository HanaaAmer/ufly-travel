<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class About_Goals extends Model
{
    protected $fillable = [
      
       'goal',
      
    ];
    protected $table='about_goals';
    protected $primaryKey='id';
}    