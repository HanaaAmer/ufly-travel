<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class city_images extends Model
{ 
  
    protected $fillable=['id', 'image','City_id'];
   
    protected $primaryKey='id';
    protected $casts = [
        'image'=>'array', ];
      protected $table='city_images';
   
    public function Cities(){
        return $this->belongsTo(Cities::class,'City_id','id');
    } 
    
}
