<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class tours_forign extends Model
{
    protected $fillable=['id','City_id','hotel_id','tour_id'];
    protected $primaryKey='id';
    protected $table='tours_forign';
    public function Tour(){
        return $this->belongsTo(Tour::class,'tour_id','id');
    } 
 public function Cities(){
        return $this->belongsTo(Cities::class,'City_id','id');
    } 
    
    public function Hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id','id');
    }
 
}
