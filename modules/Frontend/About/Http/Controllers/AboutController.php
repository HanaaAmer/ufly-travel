<?php

namespace Modules\Frontend\About\Http\Controllers;
use App\Entities\aboutus_images;
use App\Entities\AboutUs;
use App\Entities\About_Goals;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Frontend\About\Requests\AboutRequest;
/**
 * Class AboutController
 * @package App\Http\Controllers
 */
class AboutController extends Controller
{
    
    public function index()
    {
        $about = AboutUs::first();
      $aboutusimage= aboutus_images::first();
        return view('frontend.About.AboutUs', compact('about','aboutusimage'));

    }
    
}
