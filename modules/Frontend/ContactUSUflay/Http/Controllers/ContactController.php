<?php

namespace Modules\Frontend\ContactUSUflay\Http\Controllers;

use App\Entities\Setting;
use App\Entities\Contact;
use Illuminate\Http\Request;
use App\Repositories\ContactsRepository;
use App\Http\Controllers\Controller;
use Modules\Frontend\ContactUSUflay\Requests\ContactRequest;
/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class ContactController extends Controller
{
  
    public $contactRepo;
   
    public function __construct()
    {
     
        $this->contactRepo      = new ContactsRepository();

    }
    
    public function index()
    {
      
        $Contact=Setting::first();
        return view('frontend.ContactUSUflay.ContactUS', compact('Contact'));

    }
  
    public function store(ContactRequest $request,Contact $contact ){
        
        $this->contactRepo->AddContactUs($request,$contact);
        return redirect()->back()->with('addContactUS','Message Sended');
    }
    
}
