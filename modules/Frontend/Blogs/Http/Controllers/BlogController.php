<?php

namespace Modules\Frontend\Blogs\Http\Controllers;

use App\Entities\Blog;
use App\Entities\BlogCategory;
use App\Entities\SubscriptionTrainingTime;
use App\Entities\ChampionDesc;
use Illuminate\Http\Request;
use App\Repositories\BlogRepository;
use App\Repositories\TagRepository;
use App\Repositories\BlogCategoryRepository;
use App\Http\Controllers\Controller;
use App\Repositories\SponsorsRepository;
use App\Repositories\TicketRepository;
use App\Repositories\SubscriptionGameRepository;
use App\Repositories\ChampionshipRepositories;
use App\Entities\Ticket;
use App\Entities\SubscriptionGame;
use Modules\Frontend\Blogs\Requests\TicketRequest;
use Modules\frontend\Blogs\Requests\SubscriptRequest;
use DB;
use Session;
/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    public $blogRepo;
    public $contactRepo;
    
    /** 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function news($id, $title, BlogRepository $blogRepo,BlogCategoryRepository $blogCategoryRepo, SponsorsRepository $sponsorModel ,ChampionshipRepositories $champRepo)
    {
        $category     = $blogCategoryRepo->getBlogCategoryById($id);
        $blogs        = $blogRepo->getBlogsByCategoryId($id); 
        $breakingNews = $blogRepo->breakingNews();
        $sponsors     = $sponsorModel->getAllSponsors();
        $mostReadable = $blogRepo->mostReadableNews();
        $subscript_trinig = SubscriptionTrainingTime::where('category',$category->name)->first();
        $champion_desc    = ChampionDesc::where('category',$category->name)->first();
        $categories = BlogCategory::all();
        return view('front.blogs.news-category', compact('category', 'blogs', 'breakingNews', 'sponsors', 'mostReadable','subscript_trinig','champion_desc','cat'));
    }
    public function latestNews(BlogRepository $blogRepo){
        $listNews      = $blogRepo->listNews();
       
         $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();
        return view('front.blogs.latestNews',compact('listNews','breakingNews', 'mostReadable'));
        
    }

    public function tags($tagId, $title, BlogRepository $blogRepo,BlogCategoryRepository $blogCategoryRepo, SponsorsRepository $sponsorModel, TagRepository $tagRepo)
    {
        $tag          = $tagRepo->getTagById($tagId);
        $breakingNews = $blogRepo->breakingNews();
        $sponsors     = $sponsorModel->getAllSponsors();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.blogs.news-tags', compact('tag', 'breakingNews', 'sponsors', 'mostReadable'));
    } 

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newsView($id, BlogRepository $blogRepo, SponsorsRepository $sponsorModel)
    {
        $blog                     = $blogRepo->getBlogById($id);
        
        $blogKey                  = "blog ".$blog->id;

        if(!Session::has($blogKey)){
            $blog->increment('number_of_readings');
            Session::put($blogKey,1);
        } 
        

        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();
        return view('front.blogs.news-single', compact('blog', 'breakingNews', 'mostReadable'));
    }
    public function GetTickeBooking(BlogRepository $blogRepo){
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();
        return view('front.blogs.booking',compact('breakingNews', 'mostReadable'));
    }
    public function PostTickeBooking(TicketRequest $request,Ticket $ticket,TicketRepository $ticketrebo){
       
        $ticketrebo->postAddTicket($request,$ticket);
        return redirect()->back()->with('booking','تم اشتراكك بالرحله بنجاح');

    }
    public function subscript_game_training(Request $request, SubscriptionGameRepository $subscriptRebo,SubscriptionGame $subscript){
      $subscriptRebo->postAddSubscription($request,$subscript);
        Session::flash('done','تم اشتراكك بنجاح');
      return Redirect()->back();
    } 
}
