<?php

Route::get('/Blogs', ['uses' => 'BlogController@news', 'as' => 'getFrontBlog']);
Route::get('/section/{id}/{title}', ['uses' => 'BlogController@news', 'as'=>'getBlogCategoryById']);

Route::get('/articleDetails/{id}/{title}/{desc?}', ['uses' => 'BlogController@newsView'])->name("getArticleDetails");

Route::get('/tag/{id}/{title}', ['uses' => 'BlogController@tags', 'as'=>'getBlogCategoryByTagId']);

// Comments Api
Route::get('/api/comments/{post_id}', ['uses' => 'CommentController@getPostComments', 'as' => 'getPostComments']);
Route::post('/api/comments/{post_id}', ['uses' => 'CommentController@postComment', 'as' => 'postComment']);


Route::get('/ticket/booking', ['uses' => 'BlogController@GetTickeBooking', 'as' => 'getGetTickeBooking']);


Route::post('/subscript/game-training', ['uses' => 'BlogController@subscript_game_training', 'as' => 'postSubscriptGameTraining']);

Route::get('/latest/news', ['uses' => 'BlogController@latestNews', 'as' => 'getLatestNews']);

