<?php

namespace Modules\Frontend\Cities\Http\Controllers;

use App\Entities\Cities;
use App\Entities\city_images;
use App\Entities\tourism_places;
use App\Entities\tourism_placesTranslation;
use paginate;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Frontend\Cities\Requests\CitiesRequest;
/**
 * Class CitiesController
 * @package App\Http\Controllers
 */
class CitiesController extends Controller
{
    
    public function index()
    {
      
        $Cities=Cities::paginate(12);
        return view('frontend.Cities.Cities', compact('Cities'));

    }
    public function citydetails($Cityid,$countryid)
    {
      
        $Cities=Cities::all()->where('id', '=', $Cityid);
        $place=tourism_places::where('city_id','=',$Cityid)->paginate(6);
       // $tourism=tourism_places::where('City_id', '=', $Cityid)->get();
        $images=city_images::where('City_id', '=', $Cityid)->get()->first();
     
        $similercity=Cities::where([
            ['country_id', '=', $countryid],
            ['id', '!=', $Cityid] ])->get();
         
       
        return view('frontend.Cities.city_details', compact('Cities','images','similercity','place'));

    }
    public function placesdetails($placeid)
    {
      
        
        $place=tourism_places::find($placeid);
      
         
       
        return view('frontend.Cities.placsdetail', compact('place'));

    }
    
    
}
