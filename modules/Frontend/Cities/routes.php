<?php

Route::get('/Cities',['uses'=>'CitiesController@index','as'=>'cities']);
Route::get('/city_details/{CityId}/{countryId}',['uses'=>'CitiesController@citydetails','as'=>'citydetails']);
Route::get('/place_details/{placeId}',['uses'=>'CitiesController@placesdetails','as'=>'placesdetails']);
?>