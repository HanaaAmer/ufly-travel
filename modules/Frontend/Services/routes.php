<?php

//Route::match(['get','post'],'/search',['uses'=>'ServiceController@searchByKeyword','as'=>'getKeyword']);
Route::get('/services',['uses'=>'ServiceController@indexFront','as'=>'getFrontServices']);
Route::get('/service/{serviceId}/{serviceName}',['uses'=>'ServiceController@show','as'=>'getFrontServiceById']);