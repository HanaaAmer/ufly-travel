<?php

namespace Modules\Frontend\Services\Http\Controllers;

use App\Repositories\ServiceRepository;
use App\Entities\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class serviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFront(ServiceRepository $serviceRepo)
    {
        $sections = $serviceRepo->getAllServiceSections();
        $services = $serviceRepo->getAllservices();
        return view('front.services.all_services', compact('services','sections'));
    }

    public function show($serviceId, ServiceRepository $serviceRepo)
    {
        $services = $serviceRepo->getAllServices();
        $service  = $serviceRepo->getServiceDetailsByServiceId($serviceId);
        return view('front.services.service_details', compact('service','services'));
    }

    //Search By Keyword
    public function searchByKeyword(Request $request, ServiceRepository $serviceRepo)
    {
        $keyword  = $request->keyword;
        $services = Service::SearchByKeyword($keyword)->get();
        return view('front.services.search_services', compact('services','keyword'));
    }
}
