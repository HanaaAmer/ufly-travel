<?php

namespace Modules\Frontend\ServiceUFLY\Http\Controllers;

use App\Entities\TouristServices;
use App\Entities\TouristServices_Images;

use paginate;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Frontend\ServiceUFLY\Requests\ServiceUFLYRequest;
/**
 * Class ServiceUFLYController
 * @package App\Http\Controllers
 */
class ServiceUFLYController extends Controller
{
    
    public function index()
    {
      
        $Services=TouristServices::paginate(12);
        return view('frontend.ServiceUFLY.Services', compact('Services'));

    }
    public function Servicesdetails($Serviceid)
    {
      
        $Services=TouristServices::all()->where('id', '=', $Serviceid);
      
      
        $images=TouristServices_Images::where('tourist_services_id', '=', $Serviceid)->get()->first();

         
       
        return view('frontend.ServiceUFLY.Service_details', compact('Services','images'));

    }
    
    
}
