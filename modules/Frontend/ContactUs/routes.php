<?php
Route::get('/contactus',['uses'=>'ContactUsController@index','as'=>'contactUsPage']);
Route::post('/contactus',['uses'=>'ContactUsController@store','as'=>'AddContactUsFrontend']);
Route::get('/subscript',['uses'=>'ContactUsController@getSubscription','as'=>'GetSubscription']);
Route::post('/subscript',['uses'=>'ContactUsController@postSubscription','as'=>'postSubscription']);
?>