<?php

Route::get('/persons/{id}/image', 'PersonsController@personImage')->name("getPersonImage");


Route::get('/managers', 'PersonsController@managersPage')->name('managersPage');
Route::get('/teamplayers', 'PersonsController@playersPage')->name('playersPage');
Route::get('/last_managers', 'PersonsController@last_mangers')->name('last_managers');
