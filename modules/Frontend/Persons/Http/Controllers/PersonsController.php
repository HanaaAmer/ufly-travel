<?php

namespace Modules\Frontend\Persons\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Frontend\Persons\Models\Person as PersonModel;
use App\Repositories\BlogRepository;

class PersonsController extends Controller
{
    //
    public function managersPage(PersonModel $personModel, BlogRepository $blogRepo)
    {
        $managers     = $personModel->managers()->get();
        $boards       = $personModel->boards()->get();
        $ceo          = $personModel->ceo()->get();
        $vice_ceo     = $personModel->vice_ceo()->get();
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

   return view('front.tersana.managers', compact('managers','ceo', 'vice_ceo','breakingNews', 'mostReadable','boards'));
    }

    public function playersPage(PersonModel $personModel, BlogRepository $blogRepo)
    {
        $players      = $personModel->players()->get();
        $coaches      = $personModel->coaches()->get();
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.tersana.team-players', compact('players','coaches', 'breakingNews', 'mostReadable'));
    }

    public function last_mangers(PersonModel $personModel,BlogRepository $blogRepo){
        $managers     = $personModel->managers()->get();
        $breakingNews = $blogRepo->breakingNews();
        return view('front.tersana.last_managers',compact('managers','breakingNews'));
    }

    public function personImage($id, PersonModel $personModel)
    {
        $person  = $personModel->find($id);
        $imagePath = base_path('uploads/persons/'.$person->image);
        ob_end_clean();
        return response()->file($imagePath);
    }
}
