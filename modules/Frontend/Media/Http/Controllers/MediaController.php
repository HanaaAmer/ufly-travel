<?php

namespace Modules\Frontend\Media\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Frontend\Media\Models\Image as ImageModel;
use Modules\Frontend\Media\Models\Video as VideoModel;
use App\Http\Controllers\Controller;
use App\Repositories\BlogRepository;

class MediaController extends Controller
{
    public function indexImages(ImageModel $imageModel, BlogRepository $blogRepo)
    {
        $albums = $imageModel->allAlbums();
       
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.media.gallery-category', compact('albums', 'breakingNews', 'mostReadable'));
    }

    public function getAlbum($id, BlogRepository $blogRepo)
    {
        $album = ImageModel::find($id);
        $blog                     = $blogRepo->getBlogById($id);
       
        $album->images = explode(";", $album->images);
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.media.album', compact('album', 'breakingNews', 'mostReadable','blog'));
    }

    public function viewImage($id, $index, ImageModel $imageModel)
    {
        $image  = $imageModel->find($id);
        $images = explode(";", $image->images);
        $imagePath = base_path('uploads/images/'.$images[$index]);
        return response()->file($imagePath);
    }

    public function indexVideos(VideoModel $videoModel, BlogRepository $blogRepo)
    {
        $videos = $videoModel->allVideos(10);
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.media.videos-category', compact('videos', 'breakingNews', 'mostReadable','blog'));
    }

    public function getVideo($id, BlogRepository $blogRepo)
    {
        $video = VideoModel::find($id);
        $breakingNews = $blogRepo->breakingNews();
        $mostReadable = $blogRepo->mostReadableNews();

        return view('front.media.video', compact('video', 'breakingNews', 'mostReadable','blog'));
    }

    public function viewVideo($id, VideoModel $videoModel)
    {
        $video  = $videoModel->find($id);
        $videoPath = base_path('uploads/videos/'.$video->video);
        ob_end_clean();
        return response()->file($videoPath);
    }

    public function getVideoThumbnail($id, VideoModel $videoModel)
    {
        $video  = $videoModel->find($id);
        if ($video->type == "youtube") {
            return redirect($video->thumbnail);
        }
        $image = $video->thumbnail;
        $imagePath = base_path('uploads/videos/thumbs/'.$image);
        ob_end_clean();
        return response()->file($imagePath);
        
    }
}
