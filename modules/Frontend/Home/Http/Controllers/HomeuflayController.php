<?php

namespace Modules\Frontend\Home\Http\Controllers;
use App\Entities\Hotel;
use App\Entities\home_image;
use App\Entities\Tour;
use App\Entities\Tour_Image;
use App\Entities\Home_exploore;
use App\Entities\Cities;
use App\Entities\Booking;
use App\Entities\IndividualBooking;
use App\Entities\Home_advertising;
use Illuminate\Http\Request;
use App\Repositories\bookingRepository;
use App\Entities\Category;
use App\Http\Controllers\Controller;
use Modules\Frontend\Home\Requests\HomeuflayRequest;
/**
 * Class HomeuflayController
 * @package App\Http\Controllers
 */
class HomeuflayController extends Controller
{
    
    public function index(bookingRepository $bookingrebo)
    {
        
        $tour=Tour_Image::all()->where('tours.active','=',1);
        $exploore=Home_exploore::first();
        $Cities=Cities::all();
        $tourview=Tour::all();
        $hotelview=Hotel::all();
         $items = Cities::all(['id', 'name']);
        $hotels = Hotel::all(['id', 'name','city_id']);
         $Advertising = Home_advertising::first();
           $homeimage = home_image::first();
       return view('frontend.Home.home', compact('tour','exploore','Cities','tourview','hotelview','items','hotels','Advertising','homeimage'));

    }
    public function PostBooking(HomeuflayRequest $request , Booking $booking,  bookingRepository $bookingrebo)
    {
      
       $bookingrebo->postAddbooking($request,$booking);

      return redirect()->back()->with('sucess', 'Booking successfully!');
    }
    public function PostindividualBooking(HomeuflayRequest $request , IndividualBooking $individualbooking,  bookingRepository $bookingrebo)
    {
      
       $bookingrebo->postAddindividualbooking($request,$individualbooking);

      return redirect()->back()->with('sucess', 'Booking successfully!');
    }
}
