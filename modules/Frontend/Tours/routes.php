<?php 
Route::get('/TourView', ['uses' => 'Tour_FController@index', 'as' => 'gettours']);
Route::post('/tour_front',['uses'=>'Tour_FController@getAddTour','as'=>'getTour']);
Route::get('/tour_front/{tourId}',['uses'=>'Tour_FController@getTourById','as'=>'getTour_dById']);
Route::post('/add-tourReview/{tourId}',['uses'=>'Tour_ReviewFController@store','as'=>'postAddTourReview']);
Route::get('/tour_offer/{offerId}/{countryId}/show',['uses'=>'Tour_FController@touroffers','as'=>'touroffers']);
Route::get('/tour_offer/{offerId}',['uses'=>'Tour_FController@tourgetoffers','as'=>'tourgetoffers']);