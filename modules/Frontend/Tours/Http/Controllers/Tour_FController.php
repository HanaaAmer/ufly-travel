<?php

namespace Modules\Frontend\Tours\Http\Controllers;

use App\Repositories\TourRepository;
use App\Entities\Tour_Image;
use App\Entities\Tour_Detail;
use App\Entities\Tour_Review;
use App\Entities\Hotel;
use App\Entities\Tour;
use App\Entities\Cities;
use App\Entities\Offer;
use App\Entities\tours_forign;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Entities\Category;
use App\Http\Controllers\Controller;
use Modules\Frontend\Tours\Request\Tour_FRequest;
use DB;
class Tour_FController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TourRepository $TourRepo)
    {
        
        $tours = $TourRepo->getAllTours();
        $cities=Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
        $link = Tour::paginate(15); 
        $selectedcity=0 ;
        $selectedcategory=0 ;
        $selectprice=15;
        $selectst_date=date("20y-m-d");
        $selecten_date=date("20y-m-d");
        return view('frontend.Tours.tours', compact('tours','cities','categories','link','selectedcity','selectedcategory','selectprice','selectDate','selectst_date','selecten_date'));
    }
  
  
    public function getAddTour(Tour_FRequest $request)
    {
        $tours = Tour::where([["city_id","=",$request->city_id ],["category_id","=",$request->category_id]])->get(); 
         $cities=Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
        $link = Tour::paginate(15);
        $selectedcity=$request->city_id ;
        $selectedcategory=$request->category_id ; 
        $selectprice=$request->price;
        $selectst_date=$request->start_date;
        $selecten_date=$request->end_date;
       return view('frontend.Tours.tours', compact('tours','cities','categories','link','selectedcity','selectedcategory','selectprice','selectst_date','selecten_date'));
    }
    public function getTourById($tourId, TourRepository $TourRepo)
    {
        $tour  = $TourRepo->getTourById($tourId);
        $hotel =Hotel::where("id","=",$tour->hotel_id )->get()->first();
        $images  = Tour_Image::where("tour_id","=",$tourId )->get()->first();
        $Tour_Details  = Tour_Detail::where("tour_id","=",$tourId )->get()->all();
        $Tour_Reviews  = Tour_Review::where(['tour_id'=>$tourId ,'active'=>1])->orderBy('created_at', 'desc')->take(5)->get()->all();
        $Tour_city  = Tour::where([["city_id","=",$tour->city_id ],["id","!=",$tourId]])->get()->all();
         $tourforgin=tours_forign::where('tour_id','=',$tourId)->get();
      return view('frontend.Tours.selectedTour', compact('tour','images','Tour_Details','Tour_Reviews','Tour_city','hotel','tourforgin'));
    }
    public function touroffers($offerid,$countryid)
    {
        $offer=Offer::find($offerid);
        //  $tours = Tour::where("offer_id","=",$offerid)->all(); 
          // $tours = tours_forign::groupBy('tour_id')->join('cities', 'cities.id', '=', 'city_id')->where('cities.country_id','=',$countryid)->select('tours_forign.*')->get();
    $tours = tours_forign::join('tours', 'tours.id', '=', 'tour_id')->where('tours.offer_id','=',$offerid)->join('cities', 'cities.id', '=', 'tours_forign.City_id')->where('cities.country_id','=',$countryid)->select('tours_forign.*')->get()->unique('tour_id');;
    
  /*$tours = DB::table('tours_forign')
            -> whereExists(function($query)
            {
                $query->select(DB::raw(1))
                      ->from('tours')
                      ->whereRaw('tours.offerid = tours_forign.tours.offer_id')
                      ->take(1);
            })
            ->groupBy('tour_id')
            ->select('tours_forign.*')
            ->get()
;*/
           
       
       return view('frontend.Tours.offerstour', compact('tours','offer'));
    }
    
  public function tourgetoffers($offerid)
    {
        $offer=Offer::find($offerid);
        $tours = Tour::where('offer_id','=',$offerid)->get();
  
       
        return view('frontend.Tours.offerstour', compact('tours','offer'));
    }

}


