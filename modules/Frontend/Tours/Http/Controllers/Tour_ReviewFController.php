<?php

namespace Modules\Frontend\Tours\Http\Controllers;

use App\Entities\Tour_Review;
use Illuminate\Http\Request;
use App\Repositories\Tour_ReviewRepository;
use App\Http\Controllers\Controller;
use Modules\Frontend\Tours\Request\Tour_ReviewFRequest;
/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class Tour_ReviewFController extends Controller
{
  
    public $Tour_ReviewRepo;
   
    public function __construct()
    {
     
        $this->contactRepo = new Tour_ReviewRepository();

    }
  
    
    public function store($tourId,Tour_ReviewFRequest $request, Tour_ReviewRepository $Tour_ReviewRepo, Tour_Review $tour_review)
    {
        $request->tour_id=$tourId;
        $request->active=0;
        $Tour_ReviewRepo->postAddTour_Review($request, $tour_review);
        return redirect()->back()->with('Review','Thanks');
    }
    
}
