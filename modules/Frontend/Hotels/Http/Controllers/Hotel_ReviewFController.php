<?php

namespace Modules\Frontend\Hotels\Http\Controllers;

use App\Entities\Hotel_Review;
use Illuminate\Http\Request;
use App\Repositories\Hotel_ReviewRepository;
use App\Http\Controllers\Controller;
use Modules\Frontend\Hotels\Request\Hotel_ReviewFRequest;
/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class Hotel_ReviewFController extends Controller
{
  
    public $Hotel_ReviewRepo;
   
    public function __construct()
    {
     
        $this->Hotel_ReviewRepo = new Hotel_ReviewRepository();

    }
  
    
    public function store($hotelId,Hotel_ReviewFRequest $request, Hotel_ReviewRepository $Hotel_ReviewRepo, Hotel_Review $hotel_review)
    {
        $request->hotel_id=$hotelId;
        $request->active=0;
        $Hotel_ReviewRepo->postAddHotel_Review($request, $hotel_review);
        return redirect()->back()->with('Review','Thanks');
    }
    
}
