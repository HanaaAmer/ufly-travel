<?php

namespace Modules\Frontend\Hotels\Http\Controllers;

use App\Repositories\HotelRepository;
use App\Entities\Hotel_Image;
use App\Entities\Hotel_Review;
use App\Entities\Hotel;
use App\Entities\Cities;
use Illuminate\Http\Request;
use App\Entities\Category;
use App\Http\Controllers\Controller;
use Modules\Frontend\Hotels\Request\Hotel_FRequest;

class Hotel_FController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HotelRepository $HotelRepo)
    {
        
        $hotels = $HotelRepo->getAllHotels();
        $cities=Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
        $link = Hotel::paginate(15); 
        $selectedcity=0 ;
        $selectedcategory=0 ; 
        return view('frontend.Hotels.hotels', compact('hotels','cities','categories','link','selectedcity','selectedcategory'));
    }
  
  
    public function getAddHotel(Hotel_FRequest $request)
    {
        if ((is_numeric($request->city_id))&&(is_numeric($request->category_id))) {
        $hotels = Hotel::where(['city_id'=>$request->city_id ,'category_id'=>$request->category_id])->get(); 
        }
         else if((is_numeric($request->city_id))&&(is_string($request->category_id))){
         $hotels = Hotel::where([["city_id","=",$request->city_id ]])->get();
        }
        else if((is_string($request->city_id))&&(is_numeric($request->category_id))){
         $hotels = Hotel::where([["category_id","=",$request->category_id ]])->get();
        }
        else if((is_string($request->city_id))&&(is_string($request->category_id))){
         $hotels = Hotel::all();
        }
         $cities=Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
        $link = Hotel::paginate(15);
        $selectedcity=$request->city_id ;
        $selectedcategory=$request->category_id ;   
        return view('frontend.Hotels.hotels', compact('hotels','cities','categories','link','selectedcity','selectedcategory'));
    }
    public function getHotelById($hotelId, HotelRepository $HotelRepo)
    {
        $hotel  = $HotelRepo->getHotelById($hotelId);
        $images  = Hotel_Image::where("hotel_id","=",$hotelId )->get()->first();
        $Hotel_Reviews  = Hotel_Review::where(['hotel_id'=>$hotelId ,'active'=>1])->orderBy('created_at', 'desc')->take(5)->get()->all();
        $Hotel_city  = Hotel::where([["city_id","=",$hotel->city_id ],["id","!=",$hotelId]])->get()->all();
      return view('frontend.Hotels.selectedHotel', compact('hotel','images','Hotel_Reviews','Hotel_city'));
    }
 public function hotelscity($cityid)
    {
        $cities=Cities::find($cityid);
        $hotels = Hotel::all()->where('city_id','=',$cityid);
       
        return view('frontend.Hotels.hotelscity', compact('hotels','cities'));
    }
}


