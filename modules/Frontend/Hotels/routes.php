<?php 
Route::get('/HotelView', ['uses' => 'Hotel_FController@index', 'as' => 'gethotels']);
Route::post('/hotel_front',['uses'=>'Hotel_FController@getAddHotel','as'=>'getHotel']);
Route::get('/hotel_front/{hotelId}',['uses'=>'Hotel_FController@getHotelById','as'=>'getHotel_dById']);
Route::post('/add-hotelReview/{hotelId}',['uses'=>'Hotel_ReviewFController@store','as'=>'postAddHotelReview']);
Route::get('/hotels_city/{CityId}',['uses'=>'Hotel_FController@hotelscity','as'=>'hotelscity']);