<?php

namespace Modules\Frontend\Pages\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Entities\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function show($pageId, PageRepository $pageRepo)
    {
        $page = $pageRepo->getPageById($pageId);
        return view('front.pages.page_details', compact('page'));
    }

}
