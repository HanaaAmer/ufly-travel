<?php

namespace Modules\Backend\ServiceUFLY\Http\Controllers;
use App\Entities\TouristServices;
use Illuminate\Http\Request;
use App\Repositories\ServiceUFLYRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\ServiceUFLY\Requests\ServiceUFLYRequest;

/**
 * Class ServiceUFLYController.
 */
class ServiceUFLYController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ServiceUFLYRepository $ServiceRepo)
    {
        $Service = $ServiceRepo->getAllServicesufly();

        return view('backend.ServiceUFLY.all_servicesufly', compact('Service'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        return view('backend.ServiceUFLY.add_Servicesufly');
    }

    public function store(ServiceUFLYRequest $request, ServiceUFLYRepository $ServiceRepo, TouristServices $Service)
    {
        $ServiceRepo->postAddServiceufly($request, $Service);

        return redirect()->route('getAllServicesufly')->with('sucess', 'Content has been Add successfully!');
    }

    /**
     * @param $ServiceId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($ServiceId, ServiceUFLYRepository $ServiceRepo)
    {
        $Service = $ServiceRepo->getServiceByIdufly($ServiceId);

        return view('backend.ServiceUFLY.edit_service', compact('Service'));
    }

    /**
     * @param $ServiceId
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($ServiceId, ServiceUFLYRequest $request, ServiceUFLYRepository $ServiceRepo)
    {
        $ServiceRepo->updateServiceByIdufly($ServiceId, $request);

        return redirect()->route('getAllServicesufly')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $ServiceId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($ServiceId, ServiceUFLYRepository $ServiceRepo)
    {
        $ServiceRepo->deleteServiceByIdufly($ServiceId);

        return redirect()->route('getAllServicesufly')->with('sucess', 'Content has been Delete successfully!');
    }
}
