<?php

namespace Modules\Backend\ServiceUFLY\Http\Controllers;
use App\Entities\TouristServices_Images;
use App\Entities\TouristServices;
use Illuminate\Http\Request;
use App\Repositories\ServiceUFLYImagesRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\ServiceUFLY\Requests\ServiceUFLYRequest;
use File;
/**
 * Class Service_imagesController.
 */
class Service_imagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ServiceUFLYImagesRepository $ServiceImageRepo)
    {
        $ServiceImage = $ServiceImageRepo->getAllServiceImage();

        return view('backend.ServiceUFLY.ServicesImages.all_servicesimages', compact('ServiceImage'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        $Services = TouristServices::all(['name', 'id']);

        return view('backend.ServiceUFLY.ServicesImages.add_serviceimage', compact('Services'));
    }

    public function store(ServiceUFLYRequest $request, ServiceUFLYImagesRepository $ServiceImageRepo, TouristServices_Images $ServiceImage)
    {
        $ServiceImageRepo->postAddServiceImage($request, $ServiceImage);

        return redirect()->route('getAllServiceImage')->with('sucess', 'Content has been Add successfully!');
    }

    /**
     * @param $ServiceImageId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($ServiceImageId, ServiceUFLYImagesRepository $ServiceImageRepo)
    {
        $ServiceImage = $ServiceImageRepo->getServiceImageById($ServiceImageId);
        $Services = TouristServices::all(['name', 'id']);
        $selectedService = $ServiceImage->tourist_services_id;

        return view('backend.ServiceUFLY.ServicesImages.edit_serveceimage', compact('ServiceImage', 'Services', 'selectedService'));
    }

    /**
     * @param $ServiceImageId
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($ServiceImageId, ServiceUFLYRequest $request, ServiceUFLYImagesRepository $ServiceImageRepo)
    {
        //return $request;
        $ServiceImageRepo->updateServiceImageById($ServiceImageId, $request);

        return redirect()->route('getAllServiceImage')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $ServiceImageId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($ServiceImageId, ServiceUFLYImagesRepository $ServiceImageRepo)
    {
        $ServiceImageRepo->deleteServiceImageById($ServiceImageId);

        return redirect()->route('getAllServiceImage')->with('sucess', 'Content has been Delete successfully!');
    }
    public function deleteSingle($image, $ServiceImageId)
    {
         
   File::delete('public/assets/images/services/'.$image);
         
   $post=TouristServices_Images::find($ServiceImageId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }
}
