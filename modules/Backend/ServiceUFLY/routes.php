<?php

Route::group(['prefix' => 'TouristServices'], function () {
    //Service
    Route::get('/', ['uses' => 'ServiceUFLYController@index', 'as' => 'getAllServicesufly']);
    Route::post('/add', ['uses' => 'ServiceUFLYController@store', 'as' => 'postAddServiceufly']);
    Route::get('/add', ['uses' => 'ServiceUFLYController@create', 'as' => 'getAddServiceufly']);
    Route::get('/Service/{ServiceId}', ['uses' => 'ServiceUFLYController@edit', 'as' => 'getServiceByIdufly']);
    Route::put('/Service/{ServiceId}', ['uses' => 'ServiceUFLYController@update', 'as' => 'updateServiceByIdufly']);
    Route::post('/Service/{ServiceId}', ['uses' => 'ServiceUFLYController@delete', 'as' => 'deleteServiceByIdufly']);

    //ServiceImage
    Route::get('/ServiceImage', ['uses' => 'Service_imagesController@index', 'as' => 'getAllServiceImage']);
    Route::post('/add-ServiceImage', ['uses' => 'Service_imagesController@store', 'as' => 'postAddServiceImage']);
    Route::get('/add-ServiceImage', ['uses' => 'Service_imagesController@create', 'as' => 'getAddServiceImage']);
    Route::get('/ServiceImage/{ServiceImageId}', ['uses' => 'Service_imagesController@edit', 'as' => 'getServiceImageById']);
    Route::put('/ServiceImage/{ServiceImageId}', ['uses' => 'Service_imagesController@update', 'as' => 'updateServiceImageById']);
    Route::post('/ServiceImage/{ServiceImageId}', ['uses' => 'Service_imagesController@delete', 'as' => 'deleteServiceImageById']);
    Route::post('/images/{images}/{id}/delete',['uses'=>'Service_imagesController@deleteSingle','as'=>'deleteSingleImage']);
});
