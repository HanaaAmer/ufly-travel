<?php

namespace Modules\Backend\Hotels\Http\Controllers;

use App\Repositories\Hotel_ReviewRepository;
use App\Entities\Hotel_Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Hotels\Request\Hotel_ReviewRequest;

class Hotel_ReviewController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Hotel_ReviewRepository $hotel_ReviewRepo)
    {
        
        $hotel_reviews = $hotel_ReviewRepo->getAllHotel_Review();
        return view('backend.hotels.Review.all_hotel_review', compact('hotel_reviews'));
    }
   
    public function update(Hotel_ReviewRequest $request)
    {
        $hotel=Hotel_Review::find($request->id);
        $hotel->active=1;
        $hotel->save();
       return redirect()->route('getAllHotel_Review')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($hotel_reviewId, Hotel_ReviewRepository $hotel_ReviewRepo)
    {
        $hotel_ReviewRepo->deleteTour_ReviewById($hotel_reviewId);
        return redirect()->route('getAllHotel_Review')->with('sucess', 'delete sucessfully');
    }
}