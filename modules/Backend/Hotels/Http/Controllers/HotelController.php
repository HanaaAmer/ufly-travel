<?php

namespace Modules\Backend\Hotels\Http\Controllers;

use App\Repositories\HotelRepository;
use App\Entities\Hotel;
use App\Entities\Cities;
use App\Entities\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Hotels\Request\HotelRequest;

class HotelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HotelRepository $HotelRepo)
    {
        
        $hotels = $HotelRepo->getAllHotels();
       
        return view('backend.hotels.all_hotels', compact('hotels'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HotelRepository $HotelRepo)
    {
        $hotels = $HotelRepo->getAllHotels();
        $categories = Category::all(['id', 'category']);
        $items = Cities::all(['id', 'name']);
        return view('backend.hotels.add_hotel', compact('hotels','items','categories'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRequest $request, HotelRepository $HotelRepo, Hotel $hotel)
    {
        $HotelRepo->postAddHotel($request, $hotel);
        return redirect()->route('getAllHotels')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($hotelId, HotelRepository $HotelRepo)
    {
        $hotel  = $HotelRepo->getHotelById($hotelId);
        $items = Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
        $selecteditem=$hotel->city_id;
        $selectedcategory=$hotel->category_id;
      return view('backend.hotels.edit_hotel', compact('hotel','items','selecteditem','categories','selectedcategory'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($hotelId, HotelRequest $request, HotelRepository $HotelRepo)
    {
      
       $HotelRepo->updateHoteleById($hotelId, $request);
         return redirect()->route('getAllHotels')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($hotelId, HotelRepository $HotelRepo)
    {
        $HotelRepo->deleteHotelById($hotelId);
        return redirect()->route('getAllHotels')->with('sucess', 'delete sucessfully');
    }
    
}

