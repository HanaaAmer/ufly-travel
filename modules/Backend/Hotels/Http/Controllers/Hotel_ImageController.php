<?php

namespace Modules\Backend\Hotels\Http\Controllers;

use App\Repositories\Hotel_ImageRepository;
use App\Entities\Hotel_Image;
use App\Entities\Cities;
use App\Entities\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Hotels\Request\Hotel_ImageRequest;
use File;

class Hotel_ImageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Hotel_ImageRepository $Hotel_ImageRepo)
    {
        
        $hotel_images = $Hotel_ImageRepo->getAllHotel_Image();
       
        return view('backend.hotel_image.all_hotels_image', compact('hotel_images'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Hotel_ImageRepository $Hotel_ImageRepo)
    {
        $hotels = $Hotel_ImageRepo->getAllHotel_Image();
        $items = Cities::all(['id', 'name']);
        $hotels = Hotel::all(['id', 'name','city_id']);
        return view('backend.hotel_image.add_hotel_image', compact('hotels','items','hotels'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Hotel_ImageRequest $request, Hotel_ImageRepository $Hotel_ImageRepo, Hotel_Image $hotel)
    {
       // print_r($request);
        $Hotel_ImageRepo->postAddHotel_Image($request, $hotel);
        return redirect()->route('getAllHotel_Images')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($hotelId, Hotel_ImageRepository $Hotel_ImageRepo)
    {
        $hotel  = $Hotel_ImageRepo->getHotel_ImageById($hotelId);
        $items = Cities::all(['id', 'name']);
        $hotels = Hotel::all(['id', 'name','city_id']);
        $selecteditem=$hotel->city_id;
        $selectedhotel=$hotel->hotel_id;
     return view('backend.hotel_image.edit_hotel_image', compact('hotel','items','hotels','selecteditem','selectedhotel'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($hotelId, Hotel_ImageRequest $request, Hotel_ImageRepository $Hotel_ImageRepo)
    {
     $Hotel_ImageRepo->updateHotel_ImageById($hotelId, $request);
         return redirect()->route('getAllHotel_Images')->with('sucess', 'update sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($hotelId, Hotel_ImageRepository $Hotel_ImageRepo)
    {
        $Hotel_ImageRepo->deleteHotel_ImageById($hotelId);
        return redirect()->route('getAllHotel_Images')->with('sucess', 'delete sucessfully');
    }
      public function deleteSingle($image, $hotelImageId)
    {
         
   File::delete('public/assets/images/hotels/'.$image);
         
   $post=Hotel_Image::find($hotelImageId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }
}

