<?php
 Route::group(['prefix'=>'Booking'],function (){
    // Links
    Route::get('/Booking_Tour',['uses'=>'BookingController@bookingtour','as'=>'getAllbookingTour']);
  Route::get('/Booking_Hotel',['uses'=>'BookingController@bookinghotel','as'=>'getAllbookingHotel']);
   Route::get('/Individual_Booking',['uses'=>'BookingController@individualbooking','as'=>'getAllindividualbooking']);
    Route::post('/deletetour/{bookingId}',['uses'=>'BookingController@deletebookingtour','as'=>'deletebookingtour']);
      Route::post('/deletehotel/{bookingId}',['uses'=>'BookingController@deletebookinghotel','as'=>'deletebookinghotel']);
        Route::post('/deleteindividual/{bookingId}',['uses'=>'BookingController@deleteindividualBooking','as'=>'deleteindividualBooking']);
         Route::get('/showhotelbooking/{bookingId}',['uses'=>'BookingController@showhotel','as'=>'getshowhotelbookingById']);
           Route::get('/showbooking/{bookingId}',['uses'=>'BookingController@showtour','as'=>'getshowtourbookingById']);
          Route::get('/showindividual/{bookingId}',['uses'=>'BookingController@showindividualbooking','as'=>'getshowindividualbookingById']);
});
