<?php

namespace Modules\Backend\Booking\Http\Controllers;
use App\Entities\Booking;
use App\Entities\IndividualBooking;
use App\Entities\individual_bookingsforign;
use Illuminate\Http\Request;
use App\Repositories\bookingRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Booking\Requests\BookingRequest;
/**
 * Class BookingController
 * @package App\Http\Controllers
 */
 
class BookingController extends Controller
{
    
    /**
     * @var $contactRepository
     */
     protected $bookingRepository;
     protected $booking;
 
     public function __construct()
     {
         $this->bookingRepository=new bookingRepository();
         $this->booking=new Booking();
     }
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function bookingtour()
     {
         $bookingstour=Booking::orderBy('created_at', 'desc')->where('tour_id','!=',null)->get();
        
         return view('backend.Booking.bookingtour',compact('bookingstour'));
     }
        public function bookinghotel()
     {
         $bookingshotel=Booking::orderBy('created_at', 'desc')->where('hotel_id','!=',null)->get();
        
         return view('backend.Booking.bookinghotel',compact('bookingshotel'));
     }
     public function individualbooking()
     {
         $individualbooking=IndividualBooking::orderBy('created_at', 'desc')->get();
        
         return view('backend.Booking.individualbooking',compact('individualbooking'));
     }
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function indexFront()
     {
         return view('front.Booking.booking');
     }
 
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function showhotel($id)
     {
          $booking= Booking::find($id);
       
          return view('backend.Booking.showhotelbooking',compact('booking'));
     }
       public function showtour($id)
     {
          $booking= Booking::find($id);
       
          return view('backend.Booking.showtourbooking',compact('booking'));
     }
 
      public function showindividualbooking($id)
     {
          $booking= IndividualBooking::find($id);
          $individual = individual_bookingsforign::all()->where('individual_id','=',$id);
          return view('backend.Booking.showindividualbooking',compact('booking','individual'));
     }
     
     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function deletebookingtour($bookingId)
     {
         $this->bookingRepository->deleteBookingById($bookingId);
         return redirect()->route('getAllbookingTour');
     }
      public function deletebookinghotel($bookingId)
     {
         $this->bookingRepository->deleteBookingById($bookingId);
         return redirect()->route('getAllbookingHotel');
     }
     public function deleteindividualBooking($individualBookingId)
     {
         $this->bookingRepository->deleteindividualBookingById($individualBookingId);
         return redirect()->route('getAllindividualbooking');
     }
 }
 