<?php
Route::group(['prefix'=>'backend-Tours','middleware'=>'access','check-permission:superadmin'],function (){
    //Services
    Route::get('/tour',['uses'=>'TourController@index','as'=>'getAllTours']);
    Route::post('/add-tour',['uses'=>'TourController@store','as'=>'postAddTour']);
    Route::get('/add-tour',['uses'=>'TourController@create','as'=>'getAddTour']);
    Route::get('/tour/{tourId}',['uses'=>'TourController@edit','as'=>'getTourById']);
    Route::put('/tour/{tourId}',['uses'=>'TourController@update','as'=>'updateTourById']);
    Route::post('/tour/{tourId}',['uses'=>'TourController@delete','as'=>'deleteTourById']);
    //image
    Route::get('/images',['uses'=>'Tour_ImageController@index','as'=>'getAllTour_Images']);
    Route::post('/add-tour_image',['uses'=>'Tour_ImageController@store','as'=>'postAddTour_image']);
    Route::get('/add-tour_image',['uses'=>'Tour_ImageController@create','as'=>'getAddTour_image']);
    Route::get('/tour_image/{tourId}',['uses'=>'Tour_ImageController@edit','as'=>'getTour_imageById']);
    Route::put('/tour_image/{tourId}',['uses'=>'Tour_ImageController@update','as'=>'updateTour_imageById']);
    Route::post('/tour_image/{tourId}',['uses'=>'Tour_ImageController@delete','as'=>'deleteTour_imageById']);
        Route::post('/tour_image/{images}/{id}/delete',['uses'=>'Tour_ImageController@deleteSingle','as'=>'deleteSingleImagetour']);
     //detail
     Route::get('/details',['uses'=>'Tour_DetailController@index','as'=>'getAllTour_Details']);
     Route::post('/add-tour_detail',['uses'=>'Tour_DetailController@store','as'=>'postAddTour_Detail']);
     Route::get('/add-tour_detail',['uses'=>'Tour_DetailController@create','as'=>'getAddTour_Detail']);
     Route::get('/tour_detail/{tourId}',['uses'=>'Tour_DetailController@edit','as'=>'getTour_DetailById']);
     Route::put('/tour_detail/{tourId}',['uses'=>'Tour_DetailController@update','as'=>'updateTour_DetailById']);
     Route::post('/tour_detail/{tourId}',['uses'=>'Tour_DetailController@delete','as'=>'deleteTour_DetailById']);
     
       //Review
       Route::get('/review',['uses'=>'Tour_ReviewController@index','as'=>'getAllTour_Review']);
       Route::put('/tour_review',['uses'=>'Tour_ReviewController@update','as'=>'updateTour_ReviewById']);
       Route::post('/tour_review/{tourId}',['uses'=>'Tour_ReviewController@delete','as'=>'deleteTour_ReviewById']);
      
 //offer
    
     Route::get('/offer',['uses'=>'OffersController@index','as'=>'getAlloffer']);
    Route::post('/add-offer',['uses'=>'OffersController@store','as'=>'postAddoffer']);
    Route::get('/add-offer',['uses'=>'OffersController@create','as'=>'getAddoffer']);
    Route::get('/offer/{cityId}',['uses'=>'OffersController@edit','as'=>'getofferById']);
    Route::put('/offer/{cityId}',['uses'=>'OffersController@update','as'=>'updateofferById']);
    Route::post('/offer/{cityId}',['uses'=>'OffersController@delete','as'=>'deleteofferById']);
  
});