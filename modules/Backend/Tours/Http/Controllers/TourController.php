<?php

namespace Modules\Backend\Tours\Http\Controllers;

use App\Repositories\TourRepository;

use App\Entities\Tour;
use App\Entities\Hotel;
use App\Entities\Cities;
use App\Entities\Category;
use App\Entities\Offer;
use App\Entities\tours_forign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Tours\Request\TourRequest;

class TourController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TourRepository $TourRepo)
    {
        
        $tours = $TourRepo->getAllTours();
        return view('backend.tours.all_tours', compact('tours'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TourRepository $TourRepo)
    {
        $Tours = $TourRepo->getAllTours();
        $items = Hotel::all(['id', 'name','city_id']);
        $cities=Cities::all(['id', 'name']); 
        $categories = Category::all(['id', 'category']); 
         $offers = Offer::all(['id', 'offer']); 
       return view('backend.tours.add_tour', compact('tours','items','cities','categories','offers'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TourRequest $request, TourRepository $TourRepo, Tour $tour)
    {
       
      $TourRepo->postAddTour($request, $tour);
      return redirect()->route('getAllTours')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($tourId, TourRepository $TourRepo)
    {
        $tour  = $TourRepo->getTourById($tourId);
        $items = Hotel::all(['id', 'name','city_id']);
        $cities=Cities::all(['id', 'name']);
        $categories = Category::all(['id', 'category']);
          $offers = Offer::all(['id', 'offer']); 
          $tourforgin=tours_forign::where('tour_id','=',$tourId)->get();
        //$selecteditem=$tourforgin->hotel_id;
        //$selectedcity=$tourforgin->city_id;
        $selectedcategory=$tour->category_id;
         $selectedoffer=$tour->offer_id;
      return view('backend.tours.edit_tour', compact('tour','items','cities','tourforgin','categories','selectedcategory','offers','selectedoffer'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($tourId, TourRequest $request, TourRepository $TourRepo)
    {
          
       $TourRepo->updateToureById($tourId, $request);
         return redirect()->route('getAllTours')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($tourId, TourRepository $TourRepo)
    {
       
        $TourRepo->deleteTourById($tourId);
        return redirect()->route('getAllTours')->with('sucess', 'delete sucessfully');
    }
}

