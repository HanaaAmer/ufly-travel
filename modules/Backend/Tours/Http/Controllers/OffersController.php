<?php

namespace Modules\Backend\Tours\Http\Controllers;
use App\Entities\Offer;
use App\Entities\OfferTranslation;
use App\Entities\Tour;
use Illuminate\Http\Request;
use App\Repositories\OfferRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Tours\Request\OfferRequest;
/**
 * Class OffersController
 * @package App\Http\Controllers
 */
class OffersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(OfferRepository $offerRepo)
    {
        $offer = $offerRepo->getAlloffer();
        return view('backend.tours.Offers.offer',compact('offer'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
         return view('backend.tours.Offers.add_offer');
     }
     public function store(OfferRequest $request, OfferRepository $offerRepo, Offer $offer)
     {
        
        $offerRepo->postAddoffer($request, $offer);
         
         return redirect()->route('getAlloffer')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $offerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($offerId, OfferRepository $offerRepo)
    {
        $offer = $offerRepo->getofferById($offerId);
       
        return view('backend.tours.Offers.edit_offer',compact('offer'));
    }
    /**
     * @param $offerId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($offerId, OfferRequest $request, OfferRepository $offerRepo)
    {
        $offerRepo->updateofferById($offerId, $request);
        return redirect()->route('getAlloffer')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $offerId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($offerId, OfferRepository $offerRepo)
    {
       
        $tours= Tour::all()->where('offer_id', '=', $offerId);
       
         foreach($tours as $tour){
              return redirect()->route('getAlloffer')->with('sucess', 'error this offer table in relation with   tour table');
        }
        
        $offerRepo->deleteofferById($offerId);
      
        return redirect()->route('getAlloffer')->with('sucess', 'Content has been delete successfully!');
       
    }

}
