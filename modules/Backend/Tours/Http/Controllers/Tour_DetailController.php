<?php

namespace Modules\Backend\Tours\Http\Controllers;

use App\Repositories\Tour_DetailRepository;
use App\Entities\Tour;
use App\Entities\Cities;
use App\Entities\Tour_Detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Tours\Request\Tour_DetailRequest;

class Tour_DetailController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tour_DetailRepository $Tour_DetailRepo)
    {
        
        $tour_details = $Tour_DetailRepo->getAllTour_Details();
        return view('backend.Tour_Detail.all_tour_details', compact('tour_details'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tour_DetailRepository $Tour_DetailRepo)
    {
        $tour_details = $Tour_DetailRepo->getAllTour_Details();
        $items = Tour::all(['id', 'name','city_id']);
        $cities=Cities::all(['id', 'name']);   
       return view('backend.Tour_Detail.add_tour_detail', compact('tour_details','items','cities'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Tour_DetailRequest $request, Tour_DetailRepository $Tour_DetailRepo, Tour_Detail $tour_detail)
    {
        $Tour_DetailRepo->postAddTour_Detail($request, $tour_detail);
      return redirect()->route('getAllTour_Details')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($tour_detailId, Tour_DetailRepository $Tour_DetailRepo)
    {
        $tour_details  = $Tour_DetailRepo->getTour_DetailById($tour_detailId);
        $items = Tour::all(['id', 'name','city_id']);
        $cities=Cities::all(['id', 'name']);
        $selecteditem=$tour_details->tour_id;
        $selectedcity=$tour_details->city_id;
      return view('backend.Tour_Detail.edit_tour_detail', compact('tour_details','items','selecteditem','cities','selectedcity'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($tour_detailId, Tour_DetailRequest $request, Tour_DetailRepository $Tour_DetailRepo)
    {
       $Tour_DetailRepo->updateTour_DetailById($tour_detailId, $request);
         return redirect()->route('getAllTour_Details')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($tour_detailId, Tour_DetailRepository $Tour_DetailRepo)
    {
        $Tour_DetailRepo->deleteTour_DetailById($tour_detailId);
        return redirect()->route('getAllTour_Details')->with('sucess', 'delete sucessfully');
    }
}

