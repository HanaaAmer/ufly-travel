<?php

namespace Modules\Backend\Tours\Http\Controllers;

use App\Repositories\Tour_ImageRepository;
use App\Entities\Tour_Image;
use App\Entities\Cities;
use App\Entities\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Tours\Request\Tour_ImageRequest;
use File;
class Tour_ImageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tour_ImageRepository $Tour_ImageRepo)
    {
        
        $tour_images = $Tour_ImageRepo->getAllTour_Image();
       
        return view('backend.tour_image.all_tours_image', compact('tour_images'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tour_ImageRepository $Tour_ImageRepo)
    {
       // $tours = $Tour_ImageRepo->getAllTour_Image();
       // $items = Cities::all(['id', 'name']);
        $tours = Tour::all(['id', 'name']);
        return view('backend.tour_image.add_tour_image', compact('tours'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Tour_ImageRequest $request, Tour_ImageRepository $Tour_ImageRepo, Tour_Image $tour)
    {
       // print_r($request);
        $Tour_ImageRepo->postAddTour_Image($request, $tour);
        return redirect()->route('getAllTour_Images')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($tourId, Tour_ImageRepository $Tour_ImageRepo)
    {
        $tour  = $Tour_ImageRepo->getTour_ImageById($tourId);
        //$items = Cities::all(['id', 'name']);
        $tours = Tour::all(['id', 'name']);
       // $selecteditem=$tour->city_id;
        $selectedtour=$tour->tour_id;
      return view('backend.tour_image.edit_tour_image', compact('tour','tours','selectedtour'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($tourId, Tour_ImageRequest $request, Tour_ImageRepository $Tour_ImageRepo)
    {
      
       $Tour_ImageRepo->updateTour_ImageById($tourId, $request);
         return redirect()->route('getAllTour_Images')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($tourId, Tour_ImageRepository $Tour_ImageRepo)
    {
        $Tour_ImageRepo->deleteTour_ImageById($tourId);
        return redirect()->route('getAllTour_Images')->with('sucess', 'delete sucessfully');
    }
     public function deleteSingle($image, $tourImageId)
    {
         
   File::delete('public/assets/images/tours/'.$image);
         
   $post=Tour_Image::find($tourImageId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }
}

