<?php

namespace Modules\Backend\Tours\Http\Controllers;

use App\Repositories\Tour_ReviewRepository;
use App\Entities\Tour_Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Tours\Request\Tour_ReviewRequest;

class Tour_ReviewController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tour_ReviewRepository $Tour_ReviewRepo)
    {
        
        $tour_reviews = $Tour_ReviewRepo->getAllTour_Review();
        return view('backend.tours.Review.all_tour_review', compact('tour_reviews'));
    }
   
    public function update(Tour_ReviewRequest $request)
    {
        $tour=Tour_Review::find($request->id);
        $tour->active=1;
        $tour->save();
       return redirect()->route('getAllTour_Review')->with('sucess', 'update sucessfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($tour_reviewId, Tour_ReviewRepository $Tour_ReviewRepo)
    {
        $Tour_ReviewRepo->deleteTour_ReviewById($tour_reviewId);
        return redirect()->route('getAllTour_Review')->with('sucess', 'delete sucessfully');
    }
}