<?php
Route::group(['prefix'=>'backend-subscription-game','middleware'=>'check-permission:superadmin'],function (){
    Route::get('/',['uses'=>'SubscriptionController@index','as'=>'getAllSubscriptionGame']);
    Route::post('/subscription/{Id}',['uses'=>'SubscriptionController@delete','as'=>'deleteSubscriptionGameById']);
});