<?php

namespace Modules\Backend\Country\Http\Controllers;
use App\Entities\Cities;
use App\Entities\Country;
use Illuminate\Http\Request;
use App\Repositories\CountryRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Country\Requests\CountryRequest;
/**
 * Class CountryController
 * @package App\Http\Controllers
 */
class CountryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(CountryRepository $CountryRepo)
    {
        $Country = $CountryRepo->getAllCountry();
        return view('backend.Country.all_Countries',compact('Country'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
         return view('backend.Country.add_Country');
     }
     public function store(CountryRequest $request, CountryRepository $CountryRepo, Country $Country)
     {
         $CountryRepo->postAddCountry($request, $Country);
         return redirect()->route('getAllCountry')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $CountryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($CountryId, CountryRepository $CountryRepo)
    {
        $Country = $CountryRepo->getCountryById($CountryId);
        return view('backend.Country.edit_Country',compact('Country'));
    }
    /**
     * @param $CountryId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($CountryId, CountryRequest $request, CountryRepository $CountryRepo)
    {
        $CountryRepo->updateCountryById($CountryId, $request)->with('sucess', 'Content has been Update successfully!');
        return redirect()->route('getAllCountry');
    }

    /**
     * @param $CountryId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($CountryId, CountryRepository $CountryRepo)
    {
        $cities= Cities::where('country_id', '=', $CountryId)->get();
        foreach($cities as $city){
       
              return redirect()->route('getAllCountry')->with('sucess', 'error this country table in relation with  city table');
        }
           
      $CountryRepo->deleteCountryById($CountryId);
      
        return redirect()->route('getAllCountry')->with('sucess', 'Content has been Delete successfully!');
       
       
      
       
    }

}
