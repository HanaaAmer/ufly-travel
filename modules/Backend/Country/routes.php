<?php

Route::group(['prefix'=>'Country'],function (){
//country
Route::get('/',['uses'=>'CountryController@index','as'=>'getAllCountry']);
Route::post('/add-country',['uses'=>'CountryController@store','as'=>'postAddaCountry']);
Route::get('/add-country',['uses'=>'CountryController@create','as'=>'getAddCountry']);
    Route::get('/country/{countryId}',['uses'=>'CountryController@edit','as'=>'getCountryById']);
    Route::put('/country/{countryId}',['uses'=>'CountryController@update','as'=>'updateCountryById']);
    Route::post('/country/{countryId}',['uses'=>'CountryController@delete','as'=>'deleteCountryById']);
   
});
