<?php

Route::group(['prefix'=>'About'],function (){
    // About
    Route::get('/',['uses'=>'AboutUsController@index','as'=>'getAboutus']);
    Route::put('/update',['uses'=>'AboutUsController@update','as'=>'updateabout']);
    Route::put('/add',['uses'=>'AboutUsController@store','as'=>'addabout']);
  //images
  Route::get('/aboutusImage',['uses'=>'aboutus_imagesController@index','as'=>'getAllaboutusImages']);
  Route::put('/updateaboutImage',['uses'=>'aboutus_imagesController@update','as'=>'updateaboutusImages']);
  Route::put('/addaboutImage',['uses'=>'aboutus_imagesController@store','as'=>'addaboutusImages']);
  Route::post('/aboutusimages/{images}/{id}/delete',['uses'=>'aboutus_imagesController@deleteSingle','as'=>'deleteSingleaboutusImage']);
});
