<?php

namespace Modules\Backend\About\Http\Controllers;
use File;
use App\Entities\AboutUs;
use Illuminate\Http\Request;
use App\Repositories\AboutRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\About\Requests\AboutRequest;
/**
 * Class AboutUsController
 * @package App\Http\Controllers
 */
 
class AboutUsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */ 
    
    public function index(AboutRepository $aboutRepo)
    {
        $about = AboutUs::first();
    
        return view('backend.About.AboutUs', compact('about'));
   

    }
    public function store(AboutRequest $data, AboutUs $about)
    {
        $req=[];
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $about->{"title:$locale"}   = $data->input("title:{$locale}");
            $about->{"description:$locale"}   = $data->input("description:{$locale}");
            $about->{"vision_mission:$locale"}   = $data->input("vision_mission:{$locale}");
            $about->{"about_goals:$locale"}   = $data->input("about_goals:{$locale}");
        }
     
        if ($data->hasFile('image_description'))
        {
           
            $file= $data->file('image_description');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/About';
            $file->move($destinationPath, $picture);
           
            $about->fill(['image_description'=>$picture]);
        }
        if ($data->hasFile('image_vision'))
        {
            
           
            $file2= $data->file('image_vision');
            $filename2 = $file2->getClientOriginalName();
            $extension2 = $file2->getClientOriginalExtension();
            $picture2 = date('His').$filename2;
            $destinationPath2='public/assets/images/About';
            $file2->move($destinationPath2, $picture2);
          
            $about->fill(['image_vision'=>$picture2]);
        }
        if ($data->hasFile('image_goals'))
        {
          
            $file3= $data->file('image_goals');
            $filename3 = $file3->getClientOriginalName();
            $extension3 = $file3->getClientOriginalExtension();
            $picture3 = date('His').$filename3;
            $destinationPath3='public/assets/images/About';
            $file3->move($destinationPath3, $picture3);
           
            $about->fill(['image_goals'=>$picture3]);
        }
       
     
        
       $about->save();
        return redirect()->route('getAboutus')->with('sucess', 'Content has been Add successfully!');
    }


    public function update( AboutRequest $data)
    {
    $about = AboutUs::first();
     if ($data->hasFile('image_description'))
        {
            $photoName=$about->image_description;
            File::delete('public/assets/images/About/'.$photoName);
            $file= $data->file('image_description');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/About';
            $file->move($destinationPath, $picture);
           
            $about->fill(['image_description'=>$picture]);
        }
        if ($data->hasFile('image_vision'))
        {
            
            $photoName2=$about->image_vision;
            File::delete('public/assets/images/About/'.$photoName2);
            $file2= $data->file('image_vision');
            $filename2 = $file2->getClientOriginalName();
            $extension2 = $file2->getClientOriginalExtension();
            $picture2 = date('His').$filename2;
            $destinationPath2='public/assets/images/About';
            $file2->move($destinationPath2, $picture2);
          
            $about->fill(['image_vision'=>$picture2]);
        }
        if ($data->hasFile('image_goals'))
        {
            $photoName3=$about->image_goals;
            File::delete('public/assets/images/About/'.$photoName3);
            $file3= $data->file('image_goals');
            $filename3 = $file3->getClientOriginalName();
            $extension3 = $file3->getClientOriginalExtension();
            $picture3 = date('His').$filename3;
            $destinationPath3='public/assets/images/About';
            $file3->move($destinationPath3, $picture3);
           
            $about->fill(['image_goals'=>$picture3]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $about->{"title:$locale"}   = $data->input("title:{$locale}");
            $about->{"description:$locale"}   = $data->input("description:{$locale}");
            $about->{"vision:$locale"}   = $data->input("vision:{$locale}");
            $about->{"about_goals:$locale"}   = $data->input("about_goals:{$locale}");
        }
     
        
       $about->save();
        return redirect()->route('getAboutus')->with('sucess', 'Content has been Update successfully!');
    }

   
    public function delete($aboutusgId, AboutRepository $aboutRepo)
    {
        $aboutRepo->deleteSettingById($aboutusgId);
      
        return redirect()->route('getAboutus');
    }

}
