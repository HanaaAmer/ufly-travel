<?php

namespace Modules\Backend\About\Http\Controllers;
use App\Entities\aboutus_images;

use File;
use Illuminate\Http\Request;
use App\Repositories\aboutus_imagesRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\About\Requests\AboutRequest;
/**
 * Class aboutus_imagesController
 * @package App\Http\Controllers
 */
class aboutus_imagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index()
    {
        $aboutusImage = aboutus_images::first();
        return view('backend.About.aboutus_images',compact('aboutusImage'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     
     public function store(AboutRequest $request, aboutus_imagesRepository $aboutusImageRepo, aboutus_images $aboutusImage)
     {
         $aboutusImageRepo->postAddaboutusImages($request, $aboutusImage);
         
         return redirect()->route('getAllaboutusImages')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $aboutusImageId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($aboutusImageId, aboutus_imagesRepository $aboutusImageRepo)
    {
        $aboutusImage = $aboutusImageRepo->getaboutusImageById($aboutusImageId);
       
        return view('backend.About.aboutus_images',compact('aboutusImage'));
    }
    /**
     * @param $aboutusImageId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AboutRequest $data)
    {
        
        $req=[];
        $photoName= [];
        $aboutusImage = aboutus_images::first();
          $photoName = $aboutusImage->image;
        if( $aboutusImage->image != null)
       {
        if ($data->hasFile('image')) {
           
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/About';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
        }
        
            $aboutusImage->image = array_merge($aboutusImage->image,$req) ;
        }
        
        elseif( $aboutusImage->image == []){
           
          foreach ($photoName as $imagepath) {
                File::delete('public/assets/images/About/'.$imagepath);
            }
            foreach ($data->file('image') as $image) {
                $filename = $image->getClientOriginalName();
                $extension = $image->getClientOriginalExtension();
                $picture = date('His').$filename;
                $destinationPath = 'public/assets/images/About';
                $image->move($destinationPath, $picture);
                $req[] = $picture;
            }
            $aboutusImage->image = $req;
       }
        else {
            $aboutusImage->image = $photoName;
        }
    
      $aboutusImage->save();
        return redirect()->route('getAllaboutusImages')->with('sucess', 'Content has been updated successfully!');
   
    }

    /**
     * @param $aboutusImageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($aboutusImageId, aboutus_imagesRepository $aboutusImageRepo)
    {
        $aboutusImageRepo->deleteaboutusImageById($aboutusImageId);
      
        return redirect()->route('getAllaboutusImages')->with('sucess', 'Content has been dalete successfully!');
    }
     public function deleteSingle($image, $aboutusImageId)
    {
         
   File::delete('public/assets/images/About/'.$image);
         
   $post=aboutus_images::find($aboutusImageId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->route('getAllaboutusImages')->with('sucess', 'Content has been delete successfully!');

    }

}
