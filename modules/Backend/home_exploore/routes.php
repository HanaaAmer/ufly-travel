<?php

Route::group(['prefix'=>'Home'],function (){
    // Home
    Route::get('/',['uses'=>'Home_explooerController@index','as'=>'getExploore']);
    Route::put('/update',['uses'=>'Home_explooerController@update','as'=>'updateExploore']);
    Route::put('/add',['uses'=>'Home_explooerController@store','as'=>'addExploore']);
    //HomeAdvertising
      Route::get('/HomeAdvertising',['uses'=>'HomeAdvertisingController@index','as'=>'getHomeAdvertising']);
    Route::put('/updateHomeAdvertising',['uses'=>'HomeAdvertisingController@update','as'=>'updateHomeAdvertising']);
    Route::put('/addHomeAdvertising',['uses'=>'HomeAdvertisingController@store','as'=>'addHomeAdvertising']);
//homeimage
Route::get('/homimage',['uses'=>'home_imageController@index','as'=>'gethomeimage']);
Route::put('/updatehomimage',['uses'=>'home_imageController@update','as'=>'updatehomeimage']);
Route::put('/addhomimage',['uses'=>'home_imageController@store','as'=>'addhomeimage']);
   
});
