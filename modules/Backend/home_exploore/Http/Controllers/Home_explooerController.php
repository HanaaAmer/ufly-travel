<?php

namespace Modules\Backend\home_exploore\Http\Controllers;

use App\Entities\Home_exploore;
use Illuminate\Http\Request;
use App\Repositories\home_explooreRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\home_exploore\Requests\home_explooreRequest;
/**
 * Class Home_explooerController
 * @package App\Http\Controllers
 */
 
class Home_explooerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */ 
    
    public function index(home_explooreRepository $explooreRepo)
    {
        $exploore = Home_exploore::first();
    
        return view('backend.home_exploore.home_exploore', compact('exploore'));
   

    }
    public function store(home_explooreRequest $data, Home_exploore $exploore)
    {
        $req=[];
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $exploore->{"title:$locale"}   = $data->input("title:{$locale}");
            $exploore->{"description:$locale"}   = $data->input("description:{$locale}");
            $exploore->{"EXPLOORE:$locale"}   = $data->input("EXPLOORE:{$locale}");
            
        }
     
    
      
     
        
       $exploore->save();
        return redirect()->route('getExploore')->with('sucess', 'Content has been Add successfully!');
    }


    public function update( home_explooreRequest $data)
    {
        $exploore = Home_exploore::first();
     
        
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $exploore->{"title:$locale"}   = $data->input("title:{$locale}");
            $exploore->{"description:$locale"}   = $data->input("description:{$locale}");
            $exploore->{"EXPLOORE:$locale"}   = $data->input("EXPLOORE:{$locale}");
         
        }
     
        
       $exploore->save();
        return redirect()->route('getExploore')->with('sucess', 'Content has been Update successfully!');
    }

   
    public function delete($explooreId, home_explooreRepository $explooreRepo)
    {
        $explooreRepo->deleteSettingById($explooreId);
      
        return redirect()->route('getExploore');
    }

}
