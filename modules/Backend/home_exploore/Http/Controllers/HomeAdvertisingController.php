<?php

namespace Modules\Backend\home_exploore\Http\Controllers;

use App\Entities\Home_advertising;
use App\Entities\Home_advertisingTranslation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\home_exploore\Requests\home_explooreRequest;
/**
 * Class HomeAdvertisingController
 * @package App\Http\Controllers
 */
 
class HomeAdvertisingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */ 
    
    public function index()
    {
        $Advertising = Home_advertising::first();
    
        return view('backend.home_exploore.HomeAdvertising', compact('Advertising'));
   

    }
    public function store(home_explooreRequest $data, Home_advertising $Advertising)
    {
        $req=[];
        foreach (\Config::get('languages') as $locale=>$language) 
        {
           
            $Advertising->{"descrption:$locale"}   = $data->input("descrption:{$locale}");
           
        }
     
       
           
         $Advertising->fill(['link'=>$data->link]);
       
     
        
         $Advertising->save();
        return redirect()->route('getHomeAdvertising')->with('sucess', 'Content has been Add successfully!');
    }


    public function update( home_explooreRequest $data)
    {
        $Advertising = Home_advertising::first();
        
        foreach (\Config::get('languages') as $locale=>$language) 
        {
          
            $Advertising->{"descrption:$locale"}   = $data->input("descrption:{$locale}");
          
        }
     
          $Advertising->fill(['link'=>$data->link]);
       $Advertising->save();
        return redirect()->route('getHomeAdvertising')->with('sucess', 'Content has been Update successfully!');
    }

   
   

}
