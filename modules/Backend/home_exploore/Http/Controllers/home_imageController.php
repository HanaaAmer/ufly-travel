<?php

namespace Modules\Backend\home_exploore\Http\Controllers;
use File;
use App\Entities\home_image;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Backend\home_exploore\Requests\home_explooreRequest;
/**
 * Class home_imageController
 * @package App\Http\Controllers
 */
 
class home_imageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */ 
    
    public function index()
    {
        $homeimage = home_image::first();
    
        return view('backend.home_exploore.home_image', compact('homeimage'));
   

    }
    public function store(home_explooreRequest $data, home_image $homeimage)
    {
       
        
        if ($data->hasFile('image_slider'))
        {
           
            $file= $data->file('image_slider');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/Home';
            $file->move($destinationPath, $picture);
           
            $homeimage->fill(['image_slider'=>$picture]);
        }
        if ($data->hasFile('image_tour'))
        {
            
           
            $file2= $data->file('image_tour');
            $filename2 = $file2->getClientOriginalName();
            $extension2 = $file2->getClientOriginalExtension();
            $picture2 = date('His').$filename2;
            $destinationPath2='public/assets/images/Home';
            $file2->move($destinationPath2, $picture2);
          
            $homeimage->fill(['image_tour'=>$picture2]);
        }
        if ($data->hasFile('image_hotel'))
        {
          
            $file3= $data->file('image_hotel');
            $filename3 = $file3->getClientOriginalName();
            $extension3 = $file3->getClientOriginalExtension();
            $picture3 = date('His').$filename3;
            $destinationPath3='public/assets/images/Home';
            $file3->move($destinationPath3, $picture3);
           
            $homeimage->fill(['image_hotel'=>$picture3]);
        }
       
     
        
       $homeimage->save();
        return redirect()->route('gethomeimage')->with('sucess', 'Content has been add successfully!');
    }


    public function update( home_explooreRequest $data)
    {
        $homeimage = home_image::first();
        if ($data->hasFile('image_slider'))
        {
            $photoName=$homeimage->image_slider;
            File::delete('public/assets/images/Home/'.$photoName);
            $file= $data->file('image_slider');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/Home';
            $file->move($destinationPath, $picture);
           
            $homeimage->fill(['image_slider'=>$picture]);
        }
        if ($data->hasFile('image_tour'))
        {
            
            $photoName2=$homeimage->image_vision;
            File::delete('public/assets/images/Home/'.$photoName2);
            $file2= $data->file('image_tour');
            $filename2 = $file2->getClientOriginalName();
            $extension2 = $file2->getClientOriginalExtension();
            $picture2 = date('His').$filename2;
            $destinationPath2='public/assets/images/Home';
            $file2->move($destinationPath2, $picture2);
          
            $homeimage->fill(['image_tour'=>$picture2]);
        }
        if ($data->hasFile('image_hotel'))
        {
            $photoName3=$homeimage->image_hotel;
            File::delete('public/assets/images/Home/'.$photoName3);
            $file3= $data->file('image_hotel');
            $filename3 = $file3->getClientOriginalName();
            $extension3 = $file3->getClientOriginalExtension();
            $picture3 = date('His').$filename3;
            $destinationPath3='public/assets/images/Home';
            $file3->move($destinationPath3, $picture3);
           
            $homeimage->fill(['image_hotel'=>$picture3]);
        }
       
        
       $homeimage->save();
        return redirect()->route('gethomeimage')->with('sucess', 'Content has been Update successfully!');
    }

   
   
}
