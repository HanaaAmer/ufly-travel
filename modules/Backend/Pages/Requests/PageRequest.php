<?php

namespace Modules\Backend\Pages\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title' => 'required|max:255',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif|required|max:5120',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title' => 'required|max:255',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif|required|max:5120',
                ];
            }
            default:break;
        }
    }
}
