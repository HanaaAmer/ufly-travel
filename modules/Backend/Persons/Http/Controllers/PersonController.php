<?php

namespace Modules\Backend\Persons\Http\Controllers;

use App\Entities\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Persons\Requests\PersonRequest;
use Modules\Backend\Persons\Models\Person as PersonModel;

/**
 * Class PersonController
 * @package Modules\Backend\Persons\Http\Controllers
 */
class PersonController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(PersonModel $personModel)
    {
        $persons = $personModel->get();
        return view('backend.persons.all_persons', compact('persons'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $jobs = \App\Entities\Job::get()->pluck("title", "id");
        return view('backend.persons.add_person', compact('jobs'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PersonRequest $request, PersonModel $personModel)
    {
        $data          = $request->all();
        $image         = $personModel->uploadPersonImage("image", $request);
        $data["image"] = $image;
        $personModel->create($data);
        return redirect()->route('getAllPersons');
    }

    /**
     * @param $personId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($personId, PersonModel $personModel)
    {
        $person = $personModel->find($personId);
        $jobs = \App\Entities\Job::get()->pluck("title", "id");

        return view('backend.persons.person', compact('person', 'jobs'));
    }

    /**
     * @param $personId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($personId, PersonRequest $request, PersonModel $personModel)
    {
        $data          = $request->all();
        $person        = $personModel->find($personId);
        $image         = $personModel->uploadPersonImage("image",$request);
        $data["image"] = isset($image) ? $image : $person->image;
        $person->update($data);
        return redirect()->route('getAllPersons');
    }

    /**
     * @param $personId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($personId, PersonModel $personModel)
    {
        $person = $personModel->find($personId);
        $person->delete();
        return redirect()->route('getAllPersons');
    }
    
    public function personImage($id, PersonModel $personModel)
    {
        $person  = $personModel->find($id);
        $imagePath = base_path('uploads/persons/'.$person->image);
        ob_end_clean();
        return response()->file($imagePath);
    }
}
