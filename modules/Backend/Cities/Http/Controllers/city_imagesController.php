<?php

namespace Modules\Backend\Cities\Http\Controllers;
use App\Entities\city_images;
use App\Entities\Cities;
use File;
use Illuminate\Http\Request;
use App\Repositories\city_imagesRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Cities\Requests\CityRequest;
/**
 * Class city_imagesController
 * @package App\Http\Controllers
 */
class city_imagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(city_imagesRepository $CityImageRepo)
    {
        $CityImage = $CityImageRepo->getAllCityImages();
        return view('backend.City.Images.all_images',compact('CityImage'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
         $Cities = Cities::all(['name','id']);
         return view('backend.City.Images.add_image',compact('Cities'));
     }
     public function store(CityRequest $request, city_imagesRepository $CityImageRepo, city_images $CityImage)
     {
         $CityImageRepo->postAddCityImages($request, $CityImage);
         
         return redirect()->route('getAllCityImages')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $CityId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($CityImageId, city_imagesRepository $CityImageRepo)
    {
        $CityImage = $CityImageRepo->getCityImagesById($CityImageId);
        $Cities = Cities::all(['name','id']);
        $selectedCity=$CityImage->City_id;
        return view('backend.City.Images.edit_image',compact('CityImage','Cities','selectedCity'));
    }
    /**
     * @param $CityId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($CityImageId, CityRequest $request, city_imagesRepository $CityImageRepo)
    {
        //return $request;
        $CityImageRepo->updateCityImagesById($CityImageId, $request);
        return redirect()->route('getAllCityImages')->with('sucess', 'Content has been updated successfully!');
   
    }

    /**
     * @param $CityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($CityImageId, city_imagesRepository $CityImageRepo)
    {
        $CityImageRepo->deleteCityImagesById($CityImageId);
      
        return redirect()->route('getAllCityImages')->with('sucess', 'Content has been delete successfully!');
    }
     public function deleteSingle($image, $CityImageId)
    {
         
   File::delete('public/assets/images/Cities/'.$image);
         
   $post=city_images::find($CityImageId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }

}
