<?php

namespace Modules\Backend\Cities\Http\Controllers;
use App\Entities\tourism_places;
use App\Entities\tourism_placesTranslation;
use App\Entities\Cities;
use DB;
use File;
use Illuminate\Http\Request;
use App\Repositories\tourism_placesRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Cities\Requests\CityRequest;
/**
 * Class PlacesController
 * @package App\Http\Controllers
 */
class PlacesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(tourism_placesRepository $placeRepo)
    {
        $place = $placeRepo->getAllplaces();
        return view('backend.City.places.all_places',compact('place'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
         $Cities = Cities::all(['name','id']);
         return view('backend.City.places.add_place',compact('Cities'));
     }
     public function store(CityRequest $request, tourism_placesRepository $placeRepo, tourism_places $place)
     {
         $placeRepo->postAddplaces($request, $place);
         
         return redirect()->route('getAllplaces')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $placeId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($placeId, tourism_placesRepository $placeRepo)
    {
        $place = $placeRepo->getplacesById($placeId);
        $Cities = Cities::all(['name','id']);
        $selectedCity=$place->City_id;
        return view('backend.City.places.edit_place',compact('place','Cities','selectedCity'));
    }
    /**
     * @param $placeId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($placeId, CityRequest $request, tourism_placesRepository $placeRepo)
    {
        $placeRepo->updateplacesById($placeId, $request);
        return redirect()->route('getAllplaces')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $placeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($placeId, tourism_placesRepository $placeRepo)
    {
        $placeRepo->deleteplacesById($placeId);
      
        return redirect()->route('getAllplaces')->with('sucess', 'Content has been delete successfully!');
    }
    public function deleteSingle($image, $placeId)
    {
         
   File::delete('public/assets/images/Places/'.$image);
         
   $post=tourism_places::find($placeId);
   $images=$post->image_slider;
   $_image=[];
   $_image[]=$image;
   $post->image_slider=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }


}
