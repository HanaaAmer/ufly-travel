<?php

namespace Modules\Backend\Cities\Http\Controllers;
use App\Entities\Country;
use App\Entities\CountryTranslation;
use App\Entities\Cities;
use App\Entities\Hotel;
use App\Entities\Tour;
use DB;
use Illuminate\Http\Request;
use App\Repositories\CityRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Cities\Requests\CityRequest;
/**
 * Class CityController
 * @package App\Http\Controllers
 */
class CityController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(CityRepository $CityRepo)
    {
        $City = $CityRepo->getAllCity();
        return view('backend.City.all_Cities',compact('City'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
         $countries = Country::all(['name','id']);
         return view('backend.City.add_City',compact('countries'));
     }
     public function store(CityRequest $request, CityRepository $CityRepo, Cities $City)
     {
        
        $CityRepo->postAddCity($request, $City);
         
         return redirect()->route('getAllCity')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $CityId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($CityId, CityRepository $CityRepo)
    {
        $City = $CityRepo->getCityById($CityId);
        $countries = Country::all(['name','id']);
        $selectedcountry=$City->country_id;
        return view('backend.City.edit_City',compact('City','countries','selectedcountry'));
    }
    /**
     * @param $CityId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($CityId, CityRequest $request, CityRepository $CityRepo)
    {
        $CityRepo->updateCityById($CityId, $request);
        return redirect()->route('getAllCity')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $CityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($CityId, CityRepository $CityRepo)
    {
         $hotels= Hotel::all()->where('city_id', '=', $CityId);
        $tours= Tour::all()->where('city_id', '=', $CityId);
          foreach($hotels as $hotel){
              return redirect()->route('getAllCity')->with('sucess', 'error this city table in relation with  hotel table ');
        }
         foreach($tours as $tour){
              return redirect()->route('getAllCity')->with('sucess', 'error this city table in relation with   tour table');
        }
        
        $CityRepo->deleteCityById($CityId);
      
        return redirect()->route('getAllCity')->with('sucess', 'Content has been delete successfully!');
       
    }

}
