<?php

Route::group(['prefix'=>'City'],function (){
//city
Route::get('/',['uses'=>'CityController@index','as'=>'getAllCity']);
Route::post('/add-city',['uses'=>'CityController@store','as'=>'postAddCity']);
Route::get('/add-city',['uses'=>'CityController@create','as'=>'getAddCity']);
    Route::get('/city/{cityId}',['uses'=>'CityController@edit','as'=>'getCityById']);
    Route::put('/city/{cityId}',['uses'=>'CityController@update','as'=>'updateCityById']);
    Route::post('/city/{cityId}',['uses'=>'CityController@delete','as'=>'deleteCityById']);
  
     //images
Route::get('/images',['uses'=>'city_imagesController@index','as'=>'getAllCityImages']);
Route::post('/add-image',['uses'=>'city_imagesController@store','as'=>'postAddCityImages']);
Route::get('/add-image',['uses'=>'city_imagesController@create','as'=>'getAddCityImage']);
    Route::get('/image/{CityImageId}',['uses'=>'city_imagesController@edit','as'=>'getCityImagesById']);
    Route::put('/image/{CityImageId}',['uses'=>'city_imagesController@update','as'=>'updateCityImagesById']);
    Route::post('/image/{CityImageId}',['uses'=>'city_imagesController@delete','as'=>'deleteCityImagesById']);
    Route::post('/cityimages/{images}/{id}/delete',['uses'=>'city_imagesController@deleteSingle','as'=>'deleteSingleImagecitiies']);
    //places
Route::get('/places',['uses'=>'PlacesController@index','as'=>'getAllplaces']);
Route::post('/add-place',['uses'=>'PlacesController@store','as'=>'postAddplaces']);
Route::get('/add-place',['uses'=>'PlacesController@create','as'=>'getAddplaces']);
    Route::get('/place/{placeId}',['uses'=>'PlacesController@edit','as'=>'getplacesById']);
    Route::put('/place/{placeId}',['uses'=>'PlacesController@update','as'=>'updateplacesById']);
    Route::post('/place/{placeId}',['uses'=>'PlacesController@delete','as'=>'deleteplacesById']);
    Route::post('/deletsingleimageplace/{images}/{id}/delete',['uses'=>'PlacesController@deleteSingle','as'=>'deleteSingleimageplace']);
});
