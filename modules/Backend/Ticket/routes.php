<?php
Route::group(['prefix'=>'backend-ticket','middleware'=>'check-permission:admin|superadmin'],function (){
    Route::get('/',['uses'=>'TicketController@index','as'=>'getAllTicket']);
    Route::post('/ticket/{Id}',['uses'=>'TicketController@delete','as'=>'deleteTicketById']);
});