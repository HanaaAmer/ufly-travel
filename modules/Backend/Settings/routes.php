<?php

Route::group(['prefix'=>'settings'],function (){
    // Settings
    Route::get('/',['uses'=>'SettingController@index','as'=>'getSettings']);
    Route::put('/update',['uses'=>'SettingController@update','as'=>'updateSettings']);
    Route::put('/add',['uses'=>'SettingController@store','as'=>'addSettings']);
    Route::get('/history',['uses'=>'HistoryController@editHistory','as'=>'editHistory']);
    Route::post('/history/update',['uses'=>'HistoryController@updateHistory','as'=>'updateHistory']);
});

Route::post('api/history/store',['uses'=>'HistoryController@storeYear','as'=>'storeYear']);
Route::get('api/history/{id}/edit',['uses'=>'HistoryController@editYear','as'=>'editYear']);
Route::put('api/history/{id}/update',['uses'=>'HistoryController@updateYear','as'=>'updateYear']);
//Languages
Route::match(['get', 'post'],'/languages', 'LanguageController@languages')->name('backendLanguage');