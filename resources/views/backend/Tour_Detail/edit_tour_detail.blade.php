@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('tour.edit_tour_detail')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateTour_DetailById',['tourId'=>$tour_details->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                                @include('backend.includes.check_language')
                            
                     <!--   <div class="form-group">
                                <label class="control-label">{{trans('tour.city')}}</label>
                                <select name="city_id"  id="city_id" class="form-control" required>
                                        <option value="">{{ "please_select" }}</option>           
                                        @foreach($cities as $city)                          
                                        <option value="{{ $city->id }}"{{ $selectedcity == $city->id ? 'selected="selected"' : '' }}>{{ $city->name }}</option>
                                      @endforeach
                                  </select>
                            </div> -->
                        <div class="form-group">
                                <label class="control-label">{{trans('tour.hotel')}}</label>
                                <select name="tour_id" id="tour_id" class="form-control" required>
                                        <option value="">{{ "please_select" }}</option>           
                                    @foreach($items as $item)                      
                                      <option  value="{{ $item->id }}"{{ $selecteditem == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                                    @endforeach
                                  </select>
                            </div>
                            @foreach (\Config::get('languages') as $locale=>$language) 
                            <div id="tap_{{$locale}}">
                                <div class="form-group">
                                    <label class="control-label">{{trans('tour.title',[''],$locale)}}</label>
                                    <input name="title:{{$locale}}" value="{{$tour_details->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('tour.title',[''],$locale)}}" required>
                                    @if($errors->has('title:{{$locale}}'))
                                        <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                    @endif
                                </div> 
                                <div class="form-group">
                                    <label class="control-label">{{trans('tour.description',[''],$locale)}}</label>
                                    <textarea name="description:{{$locale}}" class="ckeditor" placeholder="{{trans('tour.description',[''],$locale)}}">{!! html_entity_decode($tour_details->translate($locale)->description) !!}</textarea>
                                    @if($errors->has('description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('description'.$locale)}}
                                        </span>
                                    @endif
                                </div>
                            </div> 
                            @endforeach                                     
                            <div class="form-group">
                                <label class="control-label">{{trans('tour.image_description')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control">  
                                    <img height="70" src="{{ASSETS}}/images/tours/{{$tour_details->image}}">                          
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllTour_Details')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input type="hidden" name="_method" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
   <!-- <script>
         var i=1; 
         var j=1; 
            var $city_id = $( '#city_id' ),
            $tour_id = $( '#tour_id' ),
            $options = $tour_id.find( 'option' );
          
            $city_id.on( 'change', function() {
                
            $tour_id.html( $options.filter( '[data="' + this.value + '"]' ) );  
            $( '#tour_id' ).val("{{$tour_details->tour_id}}");        
             } ).trigger( 'change' );
            </script> -->
@endsection