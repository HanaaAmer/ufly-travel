@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.update_country')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateCountryById',['countryId'=>$Country->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                              @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('Country.name',[''],$locale)}}</label>
                                <input name="name:{{$locale}}" value="{{ $Country->translate($locale)->name}}" type="text" class="form-control" placeholder="{{trans('Country.name',[''],$locale)}}" required>
                                @if($errors->has('name:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                @endif
                            </div>
                            </div>
                              @endforeach
                               
                             
                                 <div class="form-group">
                                    <label class="control-label">{{trans('Country.iso')}}</label>
                                    <input name="iso" value="{{ $Country->iso}}" type="text" class="form-control" placeholder="{{trans('Country.iso')}}" >
                                    @if($errors->has('title'))
                                        <span class="help-block">{{$errors->first('title')}}</span>
                                    @endif
                                </div>
                            

                          
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllCountry')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
@endsection