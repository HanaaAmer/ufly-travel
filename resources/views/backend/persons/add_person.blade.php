@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.addPerson')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddPerson')}}" enctype = "multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.job')}}</label>
                                    {{ Form::select('job_id', $jobs, isset($person) ? $person->job_id : null,['class' => "job form-control", "required"]) }}
                                    @if ($errors->has('job_id'))
                                        <span class="error text-danger">{{ $errors->first('job_id') }}</span>
                                    @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.name')}}</label>
                                <input name="name" value="{{old('name')}}" type="text" class="form-control" placeholder="{{trans('backend.name')}}" >
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.description')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="description" value="{{old('description')}}" type="text" class="form-control" placeholder="{{trans('backend.description')}}" required>
                                    @if($errors->has('description'))
                                        <span class="help-block">{{$errors->first('description')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div id="playerBlock">
                                <div class="form-group">
                                    <label class="control-label">{{trans('backend.number')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input name="number" type="number" class="form-control" placeholder="{{trans('backend.number')}}">
                                        @if($errors->has('number'))
                                            <span class="help-block">{{$errors->first('number')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">{{trans('backend.birth_date')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input name="birth_date" type="date" class="form-control" placeholder="{{trans('backend.birth_date')}}">
                                        @if($errors->has('birth_date'))
                                            <span class="help-block">{{$errors->first('birth_date')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('backend.joining_date')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input name="joining_date" type="date" class="form-control" placeholder="{{trans('backend.joining_date')}}">
                                        @if($errors->has('joining_date'))
                                            <span class="help-block">{{$errors->first('joining_date')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllPersons')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function()
    {
        var selected = $(".job :selected").text();
        if(selected == "Football Player") {
            $('#playerBlock').show();
        } else {
            $('#playerBlock').hide();
        }
        $(".job").change(function() {
            var selected = $(this).find("option:selected").text();
            if(selected == "Football Player") {
                $('#playerBlock').show();
            } else {
                $('#playerBlock').hide();
            }
          });
    })
</script>
@endsection