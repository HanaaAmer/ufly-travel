@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
     setTimeout(function () {
   $('#myModal').modal('hide');
}, 4000); 
  }
  
  
</script>
<div class="row">

    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.EXPLOORE')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if($exploore != null) 
                    <form method="post" action="{{route('updateExploore')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('City.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{$exploore->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('City.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('backend.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('backend.description',[''],$locale)}}" maxlength="120"  required>{!! html_entity_decode($exploore->translate($locale)->description) !!}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            
                            
                    <div class="form-group">
                            <label class="control-label">{{trans('backend.EXPLOORE',[''],$locale)}}</label>
                             <div class="input-group control-group increment" >
                             <table class="table table-striped table-bordered table-hover goal{{$locale}}" ><tr>
                           
                               <td>
                            
                               @for ($i = 0 ; $i < count($exploore->EXPLOORE); $i++ )
                                   
                                  <input type="text" name="EXPLOORE:{{$locale}}[]" class="form-control" placeholder="{{trans('backend.EXPLOORE',[''],$locale)}}"  value="{{$exploore->translate($locale)->EXPLOORE[$i]}}" required>
                             @endfor   
                            
                                 </td>  
                                 <td>  <button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
                                </tr></table>
                               
                              
                            
                                    
                              </div>
                        </div>
                    </div>
                        @endforeach
                            
                        
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAboutus')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                        
                          </form>
                            @endif
                           @if($exploore == null)
                           <form method="post" action="{{route('addExploore')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('City.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}"  value="{{old('title:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('City.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('backend.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('backend.description',[''],$locale)}}" maxlength="120"  required> {{old('description:'.$locale)}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            
                            
                        <div class="form-group">
                       
                            <label class="control-label">{{trans('backend.EXPLOORE',[''],$locale)}}</label>
                             <div class="input-group control-group increment" > 
                           
                             <table class="table table-striped table-bordered table-hover goal{{$locale}}" ><tr>
                           
                               <td>
                              
                                   
                             
                              <input type="text" name="EXPLOORE:{{$locale}}[]" class="form-control" placeholder="{{trans('backend.EXPLOORE',[''],$locale)}}"  value="{{old('EXPLOORE:'.$locale)}}" required>
                           
                          
                                 </td> 
                                 <td>  <button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
                                
                                </td></tr></table>
                              
                              
                                    
                              </div>
                        </div>
                </div>
                         @endforeach
                            
                    
                           
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAboutus')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                      
                    </form>   @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
  <script type="text/javascript">
        var i=1;
        var j=1;
        var x=1;

    $(document).ready(function() {
       
     $('.addar').click(function(){
       
         
          $('.goalar').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="EXPLOORE:ar[]" value="{{old('EXPLOORE:ar')}}" placeholder=" نحن نستعرض " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
          });
         $('.adden').click(function(){
          $('.goalen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="EXPLOORE:en[]" value="{{old('EXPLOORE:en')}}" placeholder=" EXPLOORE" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
     });

     $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
     });
    });
    

</script>

 @endsection