@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
     setTimeout(function () {
   $('#myModal').modal('hide');
}, 4000); 
  }
  
  
</script>
<div class="row">

    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.images')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if($homeimage != null) 
                    <form method="post" action="{{route('updatehomeimage')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-body">
                    
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.image_slider')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_slider" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/Home/{{$homeimage->image_slider}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             
                          
                             <div class="form-group">
                                <label class="control-label">{{trans('backend.image_tour')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_tour" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/Home/{{$homeimage->image_tour}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.image_hotel')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_hotel" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/Home/{{$homeimage->image_hotel}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                        </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('gethomeimage')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                        
                          </form>
                            @endif
                           @if($homeimage == null)
                           <form method="post" action="{{route('addhomeimage')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-body">
                      
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.image_slider')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_slider" type="file" class="form-control" required>
                                    
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label class="control-label">{{trans('backend.image_tour')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_tour" type="file" class="form-control" required>
                                    
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('backend.image_hotel')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_hotel" type="file" class="form-control" required>
                                   
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('gethomeimage')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                      
                    </form>   @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
  
 @endsection