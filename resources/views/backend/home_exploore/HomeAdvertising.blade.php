@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
     setTimeout(function () {
   $('#myModal').modal('hide');
}, 4000); 
  }
  
  
</script>
<div class="row">

    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.Advertising')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if($Advertising != null) 
                    <form method="post" action="{{route('updateHomeAdvertising')}}" enctype="multipart/form-data">
                       
                        {{ csrf_field() }}
                       
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('backend.long_description',[''],$locale)}}</label>
                                <textarea name="descrption:{{$locale}}" class="form-control" placeholder="{{trans('backend.long_description',[''],$locale)}}" maxlength="120"  required>{{$Advertising->translate($locale)->descrption}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                          
                       
                    </div>
                        @endforeach
                            
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.link')}}</label>
                                <input name="link" value="{{$Advertising->link}}" type="text" class="form-control" placeholder="{{trans('backend.link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getHomeAdvertising')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                        
                          </form>
                            @endif
                           @if($Advertising == null)
                           <form method="post" action="{{route('addHomeAdvertising')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('backend.long_description',[''],$locale)}}</label>
                                <textarea name="descrption:{{$locale}}" class="form-control" placeholder="{{trans('backend.long_description',[''],$locale)}}" maxlength="120"  required> {{old('descrption:'.$locale)}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            
                            
                        
                </div>
                         @endforeach
                            
                            
                              <div class="form-group">
                                <label class="control-label">{{trans('backend.link')}}</label>
                                <input name="link" value="{{old('link:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('backend.link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getHomeAdvertising')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                      
                    </form>   @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
 
 @endsection