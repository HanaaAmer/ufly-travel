@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.show')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                 
                        <div class="form-body">
                            
                            
        
                          <div class="form-group">
                                <label class="control-label">{{trans('contact.name')}}</label>
                                <div class="input-group">
                                  <input  class="form-control" type="text" value="{{$contact->firstname}}{{" "}}{{$contact->lastname}}" readonly> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('contact.email')}}</label>
                                <div class="input-group">
                                   <input  class="form-control" type="text" value="{{$contact->email}}" readonly> 
                                </div>
                            </div>
                          
                          <div class="from-group">
                           <label class="control-label">{{trans('contact.phone')}}</label>
                          
                          <div class="input-group">
                                   <input  class="form-control" type="text" value="{{$contact->phone}}" readonly> 
                                </div>
                          </div>
                       <div class="from-group">
                           <label class="control-label">{{trans('contact.message')}}</label>
                        
                          <div >
                                    <textarea  class="form-control"  readonly>{{$contact->message}}</textarea>
                                </div>
                          </div>

                          
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                             
                                <a class="btn btn-danger" href="{{route('getAllContacts')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
     
   
@endsection