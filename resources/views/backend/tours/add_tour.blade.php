@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('tour.add_tour')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddTour')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            @include('backend.includes.check_language')
                            @foreach (\Config::get('languages') as $locale=>$language) 
                            <div id="tap_{{$locale}}">
                                <div class="form-group">
                                    <label class="control-label">{{trans('tour.name',[''],$locale)}}</label>
                                    <input name="name:{{$locale}}" value="{{old('name:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('tour.name',[''],$locale)}}" required>
                                    @if($errors->has('name:{{$locale}}'))
                                        <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                    @endif
                                </div> 
                                <div class="form-group">
                                    <label class="control-label">{{trans('tour.short_description',[''],$locale)}}</label>
                                    <textarea name="short_description:{{$locale}}" class="form-control" maxlength="120"placeholder="{{trans('tour.short_description',[''],$locale)}}"required>{{old('short_description:'.$locale)}}</textarea>
                                    @if($errors->has('short_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('short_description'.$locale)}}
                                        </span>
                                    @endif
                                </div> 
                                 <div class="form-group">
                                    <label class="control-label">{{trans('tour.long_description',[''],$locale)}}</label>
                                    <textarea name="long_description:{{$locale}}" class="ckeditor" placeholder="{{trans('tour.long_description',[''],$locale)}}"required>{{old('long_description:'.$locale)}}</textarea>
                                    @if($errors->has('long_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('long_description'.$locale)}}
                                        </span>
                                    @endif
                                </div> 
                                <div class="form-group">
                                    <table class="table table-bordered dynamic_field{{$locale}}" >  
                                            <label class="control-label">{{trans('tour.included',[''],$locale)}}</label>
                                            <tr>  
                                                <td><input type="text" name="included:{{$locale}}[]" value="{{old('included:'.$locale)}}" placeholder="{{trans('tour.included',[''],$locale)}}" class="form-control name_list" required /></td>  
                                                <td><button type="button"  name="add_include" class="btn btn-success add_include">+</button></td>  
                                            </tr>
                                           
                                        </table>
                                    </div>  
                                   
                                    <div class="form-group">
                                            <table class="table table-bordered dynamic{{$locale}}" >  
                                                    <label class="control-label">{{trans('tour.not_included',[''],$locale)}}</label>
                                                    <tr>  
                                                        <td><input type="text" name="not_included:{{$locale}}[]" value="{{old('not_included:'.$locale)}}" placeholder="{{trans('tour.not_included',[''],$locale)}}" class="form-control name_list" required /></td>  
                                                        <td><button type="button" name="add"  class="btn btn-success add">+</button></td>  
                                                    </tr> 
                                                   
                                                </table>
                                            </div>                                                               
                            </div> 
                            @endforeach                                                                                                     
                    
                        <div class="form-group">
                            <label class="control-label">{{trans('tour.price')}}</label>
                            <input name="price" value="{{old('price')}}" type="number" class="form-control" placeholder="{{trans('tour.price')}}"required >
                        </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('tour.offer')}}</label>
                                <select name="category_id" id="category_id" class="form-control" required>
                                    <option value="">{{ "please_select" }}</option>
                                    @foreach($offers as $offer)                          
                                      <option  value="{{ $offer->id }}" >{{ $offer->offer }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('tour.category')}}</label>
                                <select name="category_id" id="category_id" class="form-control" required>
                                    <option value="">{{ "please_select" }}</option>
                                    @foreach($categories as $category)                          
                                      <option  value="{{ $category->id }}" >{{ $category->category }}{{ \App\Http\Controllers\HomeController::translateWord('Stars')}}</option>
                                    @endforeach
                                  </select>
                            </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('tour.start_date')}}</label>
                                <input name="start_date" value="{{old('start_date')}}" type="date" class="form-control" placeholder="{{trans('tour.start_date')}}" required>
                            </div>
                            <div class="form-group">
                                    <label class="control-label">{{trans('tour.end_date')}}</label>
                                    <input name="end_date" value="{{old('end_date')}}" type="date" class="form-control" placeholder="{{trans('tour.end_date')}}"required >
                                </div>
                                
                        <div class="form-group">
                            <label class="control-label">{{trans('tour.duration_day')}}</label>
                            <input name="duration_day" value="{{old('duration_day')}}" type="number" class="form-control" placeholder="{{trans('tour.duration_day')}}"required >
                        </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('tour.duration_night')}}</label>
                                <input name="duration_night" value="{{old('duration_night')}}" type="number" class="form-control" placeholder="{{trans('tour.duration_night')}}" required>
                            </div>
                            <div class="form-group">
                                    <label class="control-label">{{trans('tour.availability')}}</label>
                                    <input name="availability" value="{{old('availability')}}" type="number" class="form-control" placeholder="{{trans('tour.availability')}}"required >
                                </div>
                                <div class="form-group">
                                        <label class="control-label">{{trans('tour.adults')}}</label>
                                        <input name="adults" value="{{old('adults')}}" type="number" class="form-control" placeholder="{{trans('tour.adults')}}"required >
                                    </div>
                                    <div class="form-group">
                                            <label class="control-label">{{trans('tour.childern')}}</label>
                                            <input name="childern" value="{{old('childern')}}" type="number" class="form-control" placeholder="{{trans('tour.childern')}}" required>
                                        </div>  
                                         <div class="form-group">
                                            <label class="control-label">{{trans('tour.beds')}}</label>
                                            <input name="beds" value="{{old('beds')}}" type="number" class="form-control" placeholder="{{trans('tour.beds')}}" required>
                                        </div>  
                       
                       <div class="form-group">
                                <label class="control-label">{{trans('tour.city')}}</label>        
                               <select data-placeholder="{{trans('City.SelectCity')}}" multiple class="form-control chosen-select" name="City_id[]"   required>
                                   
                                    @foreach($cities as $city)                          
                                      <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        <div class="form-group">
                            <label class="control-label">{{trans('tour.hotel')}}</label>        
                            <select data-placeholder="{{ \App\Http\Controllers\HomeController::translateWord('-- Select Hotels --')}}" multiple class="form-control chosen-select" name="hotel_id[]"  required>
                                
    
                                 @foreach($cities as $item)     
                                                        
                                 <option value="{{ $item->id }}"  disabled><h1 >{{ $item->name }}</h1> </option>
                                    @foreach($items as $hotel) 
                                    @if ($hotel->city_id == $item->id)                
                                  <option value="{{$hotel->id }}">{{ $hotel->name }}</option>
                                 @endif
                                  @endforeach 
                                 @endforeach  
                              </select>
                        </div>
                        
                        
                            <div class="form-group">
                                <label class="control-label">{{trans('tour.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control"required>                            
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label">{{trans('tour.active')}}</label>
                                    <div class="input-group">
                                            <input type="hidden" name="active" value="0" />
                                        <input type="checkbox" name="active" class="checkbox" value="1">
                                       
                                    </div>
                                </div>
                             
                        </div>
                       
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllTours')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <script>
         var i=1; 
         var j=1;
         var x=1;        
      
        $('.add_include').click(function(){                       
          // var x=($('input[name=type]:checked').val());
           if(($('input[name=type]:checked').val())=="ar") { 
            i++;             
           $('.dynamic_fieldar').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text"  name="included:ar[]" value="{{old('included:ar')}}" placeholder="تشتمل علي" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
          // $('.dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text"  name="included:en[]" value="" placeholder="included " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
           }
           else{
            x++;             
           $('.dynamic_fielden').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="included:en[]" value="{{old('included:en')}}" placeholder="included" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
           }
      });


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 
      $('.add').click(function(){  
         //  i++;  
        //   $('.dynamic').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="not_included:{{$locale}}[]" value="{{old('not_included:'.$locale)}}" placeholder="{{trans('tour.not_included',[''],$locale)}}" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
           if(($('input[name=type]:checked').val())=="ar") { 
            j++;             
           $('.dynamicar').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="not_included:ar[]" value="{{old('not_included:ar')}}" placeholder="لا تشتمل علي " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
          // $('.dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text"  name="included:en[]" value="" placeholder="included " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
           }
           else{
            x++;             
           $('.dynamicen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="not_included:en[]" value="{{old('not_included:en')}}" placeholder="not_included" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
           }
      });

      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 
       
       $(".chosen-select").chosen({}); 
        </script>
@endsection