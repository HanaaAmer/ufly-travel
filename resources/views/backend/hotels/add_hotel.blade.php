@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('hotel.add_hotel')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddHotel')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}                        
                        <div class="form-body">
                            @include('backend.includes.check_language')
                            @foreach (\Config::get('languages') as $locale=>$language) 
                            <div id="tap_{{$locale}}">
                                <div class="form-group">
                                    <label class="control-label">{{trans('hotel.name',[''],$locale)}}</label>
                                    <input name="name:{{$locale}}" value="{{old('name:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('hotel.name',[''],$locale)}}" required>
                                    @if($errors->has('name:{{$locale}}'))
                                        <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('hotel.short_description',[''],$locale)}}</label>
                                    <textarea name="short_description:{{$locale}}" maxlength="120" class="form-control" placeholder="{{trans('hotel.short_description',[''],$locale)}}"required>{{old('short_description:'.$locale)}}</textarea>
                                    @if($errors->has('short_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('short_description'.$locale)}}
                                        </span>
                                    @endif
                                </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('hotel.long_description',[''],$locale)}}</label>
                                    <textarea name="long_description:{{$locale}}" class="ckeditor" placeholder="{{trans('hotel.long_description',[''],$locale)}}"required>{{old('long_description:'.$locale)}}</textarea>
                                    @if($errors->has('long_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('long_description'.$locale)}}
                                        </span>
                                    @endif
                                </div>
                                
                            <div class="form-group">
                                    <table class="table table-bordered dynamic_field{{$locale}}" >  
                                            <label class="control-label">{{trans('hotel.services',[''],$locale)}}</label>
                                            <tr>  
                                                <td><input type="text" name="services:{{$locale}}[]" value="{{old('services:'.$locale)}}" placeholder="{{trans('hotel.services',[''],$locale)}}" class="form-control name_list"  /></td>  
                                                <td><button type="button"  name="add_include" class="btn btn-success add_include">+</button></td>  
                                            </tr>
                                           
                                        </table>
                                    </div>  
                                   
                                    <div class="form-group">
                                            <table class="table table-bordered dynamic{{$locale}}" >  
                                                    <label class="control-label">{{trans('hotel.Amenities',[''],$locale)}}</label>
                                                    <tr>  
                                                        <td><input type="text" name="Amenities:{{$locale}}[]" value="{{old('Amenities:'.$locale)}}" placeholder="{{trans('hotel.Amenities',[''],$locale)}}" class="form-control name_list"  /></td>  
                                                        <td><button type="button" name="add"  class="btn btn-success add">+</button></td>  
                                                    </tr> 
                                                   
                                                </table>
                                            </div>                                                               
                            </div>    
                            @endforeach                                            
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.price')}}</label>
                            <input name="price" value="{{old('price')}}" type="number" class="form-control" placeholder="{{trans('hotel.price')}}"required >
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.category')}}</label>
                            <select name="category_id"  class="form-control"required>
                                <option value="">{{ "please_select" }}</option>
                                @foreach($categories as $category)                          
                                  <option value="{{ $category->id }}">  {{ $category->category }}{{ \App\Http\Controllers\HomeController::translateWord('Stars')}} </option>
                                @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.duration_day')}}</label>
                            <input name="duration_day" value="{{old('duration_day')}}" type="number" class="form-control" placeholder="{{trans('hotel.duration_day')}}" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.duration_night')}}</label>
                            <input name="duration_night" value="{{old('duration_night')}}" type="number" class="form-control" placeholder="{{trans('hotel.duration_night')}}" required>
                        </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('hotel.availability')}}</label>
                                <input name="availability" value="{{old('availability')}}" type="number" class="form-control" placeholder="{{trans('hotel.availability')}}" required>
                            </div>
                            <div class="form-group">
                                    <label class="control-label">{{trans('hotel.adults')}}</label>
                                    <input name="adults" value="{{old('adults')}}" type="number" class="form-control" placeholder="{{trans('hotel.adults')}}" required>
                                </div>
                                <div class="form-group">
                                        <label class="control-label">{{trans('hotel.childern')}}</label>
                                        <input name="childern" value="{{old('childern')}}" type="number" class="form-control" placeholder="{{trans('hotel.childern')}}"required >
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">{{trans('hotel.beds')}}</label>
                                        <input name="beds" value="{{old('beds')}}" type="number" class="form-control" placeholder="{{trans('hotel.beds')}}"required >
                                    </div>
                                    <div class="form-group">
                                            <label class="control-label">{{trans('hotel.capacity')}}</label>
                                            <input name="capacity" value="{{old('capacity')}}" type="number" class="form-control" placeholder="{{trans('hotel.capacity')}}"required >
                                        </div>   
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.city')}}</label>        
                            <select name="city_id"  class="form-control"required>
                                <option value="">{{ "please_select" }}</option>
                                @foreach($items as $item)                          
                                  <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                              </select>
                        </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('hotel.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control"required>                            
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                    
                                </div>
                            </div>
                              <div class="form-group">
                                <label class="control-label">{{trans('setting.map')}}</label>
                                <div class="acf-map">
			                	<div class="marker"></div>
                                </div>
                                <input  name="map_lat" value="{{old('map_lat')}}" >
                                <input  name="map_lng"  value="{{old('map_lng')}}">
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllHotels')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
   <style type="text/css">

                        .acf-map {
                            width: 100%;
                            height: 300px;
                            border: #ccc solid 1px;
                            margin: 20px 0;
                        }                       
                        .acf-map img {
                           max-width: inherit !important;
                        }
                        
                        </style>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnEX6zLgaPJTW1Eb7rJJoADHvtNzee8hs"></script>
                        <script type="text/javascript">
                        (function($) {
                        var map = null;
                        var markersArray = [];                      
                        function new_map( $el ) {
                            
                            // var
                            var $markers = $el.find('.marker');
                            
                            
                            // vars
                            var args = {
                                zoom		: 10,
                                center		: new google.maps.LatLng(30.0608866, 31.2050317),
                                mapTypeId	: google.maps.MapTypeId.ROADMAP
                            };
                        
                            // create map	        	
                            var map = new google.maps.Map( $el[0], args);
                        
                            // return
                            return map;
                            
                        }
                        
                        function placeMarker(location) {
                            deleteOverlays();
                        
                            var marker = new google.maps.Marker({
                                position: location, 
                                map: map
                            });
                        
                            markersArray.push(marker);
                        
                            map.panTo(location);
                        }
                        
                        function deleteOverlays() {
                            if (markersArray) {
                                for (i in markersArray) {
                                    markersArray[i].setMap(null);
                                }
                            markersArray.length = 0;
                            }
                        }
                    
                        $(document).ready(function(){
                        
                            $('.acf-map').each(function(){
                        
                                // create map
                                map = new_map( $(this) );
                            
                                var old_lat = "3.107245731592633";
                                var old_lng = "101.74307265624998";
                                  
                                if (old_lat !== "" && old_lng !== "") {
                                    placeMarker(new google.maps.LatLng(old_lat, old_lng));
                                }
                        
                                google.maps.event.addListener(map, 'click', function(event) {
                                    placeMarker(event.latLng);
                                    $("input[name='map_lat']").val(event.latLng.lat().toFixed(3));
                                    $("input[name='map_lng']").val(event.latLng.lng().toFixed(3));
                                }); 
                        
                            });
                        
                        });

                        })(jQuery);

         var i=1; 
         var j=1;
         var x=1;        
      
        $('.add_include').click(function(){                       
           if(($('input[name=type]:checked').val())=="ar") { 
            i++;             
           $('.dynamic_fieldar').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text"  name="services:ar[]" value="{{old('services:ar')}}" placeholder=" الخدمات" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
           }
           else{
            x++;             
           $('.dynamic_fielden').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="services:en[]" value="{{old('services:en')}}" placeholder="services" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
           }
      });


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 
      $('.add').click(function(){  
        
           if(($('input[name=type]:checked').val())=="ar") { 
            j++;             
           $('.dynamicar').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="Amenities:ar[]" value="{{old('Amenities:ar')}}" placeholder=" وسائل الراحة " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
          
           }
           else{
            x++;             
           $('.dynamicen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="Amenities:en[]" value="{{old('Amenities:en')}}" placeholder="Amenities" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
           }
      });

      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 
       
        
        </script>
@endsection