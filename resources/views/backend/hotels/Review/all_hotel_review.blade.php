@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{ \App\Http\Controllers\HomeController::translateWord('Message')}}</h4>
            </div>
            <div class="modal-body">
              {{Session::get('sucess')}}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{ \App\Http\Controllers\HomeController::translateWord('Close')}}</button>
            </div>
          </div>
          
        </div>
      </div>
    <script>
      
      var exist = '{{Session::has('sucess')}}';
      if(exist){
        $('#myModal').modal('show');
         setTimeout(function () {
       $('#myModal').modal('hide');
    }, 4000); 
      }
      
      
    </script>
<div class="row">
    <div class="col-md-12">
        
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe">  {{trans('backend.Hotel_Review')}}</i></div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('hotel.fullname')}}</th> 
                                <th>{{trans('hotel.name')}}</th>                               
                                <th>{{trans('hotel.review')}}</th>
                                <th>{{trans('hotel.Accomodation')}}</th>
                                <th>{{trans('hotel.Meals')}}</th>
                                <th>{{trans('hotel.Value_For_Money')}}</th>
                                <th>{{trans('hotel.Destination')}}</th>
                                <th>{{trans('hotel.Transport')}}</th>                               
                                <th width="1%">{{trans('backend.action')}}</th>
                            <th width="1%"></th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($hotel_reviews as $i=>$hotel_review)
                            <tr class="odd gradeX">
                                <td>{{$i+1}}</td>
                                <td>{{$hotel_review->fullname}}</td>
                                <td>{{$hotel_review->Hotels->name}}</td>
                                <td>{{$hotel_review->review}}</td>
                                <td>{{$hotel_review->Accomodation}}</td>
                                <td>{{$hotel_review->Meals}}</td>
                                <td>{{$hotel_review->Value_For_Money}}</td>
                                <td>{{$hotel_review->Destination}}</td>
                                <td>{{$hotel_review->Transport}}</td>  
                                <td>
                                     <div class="clearfix">
                                    <a data-toggle="modal" data-id="{{$hotel_review->id}}" data-name="{{$hotel_review->fullname}}" data-review="{{$hotel_review->review}}"  href="#UserDialog" class="btn green btn-outline user_dialog">Show</a>  
                                     </div>
                                </td>
                                <td>
                                <div class="clearfix">
                                    <form action="{{route('updateHotel_ReviewById')}}" method="post">
                                        {{csrf_field()}}
                                        @if($hotel_review->active==0) <input class="btn green btn-outline" type="submit" value="Confirm">@endif
                                        <input name="id" type="hidden" value="{{$hotel_review->id}}">
                                        <input type="hidden" name="_method" value="PUT">
                                    </form>
                                    
                                </div>
                                </td>
                                <td>
                                <div class="clearfix">
                                    <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteHotel_ReviewById',['hotelId'=>$hotel_review->id])}}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn red btn-outline">{{trans('backend.delete')}}
                                        </button>
                                    </form>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

 <div id="UserDialog" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
          <!-- Modal content-->
          <div class="modal-content">
                  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{ \App\Http\Controllers\HomeController::translateWord('Message')}}</h4>
            </div>
                  <div class="modal-body">
                      <div class="form-group">
                            <label >{{trans('hotel.fullname')}}</label>
                            <input type="text" class="form-control p-input" name="fullname" id="fullname"  value="">
                            
                        </div>  
                      
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{trans('hotel.review')}}</label>
                            <textarea type="text" class="form-control p-input" name="review" id="review"  value=""></textarea>
                            
                        </div>                         
                       
                  </div>
                   <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{ \App\Http\Controllers\HomeController::translateWord('Close')}}</button>
            </div>
          </div>
      </div>
  </div>
  <script>
    $(document).on("click", ".user_dialog", function () {
      var review= $(this).data('review');
      var name= $(this).data('name');
      $(".modal-body #review").val(review );
       $(".modal-body #fullname").val(name );
    
 });

</script>
@endsection
@section('js')
<script src="{{ASSETS}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ASSETS}}/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
@endsection

@section('css')
<link href="{{ASSETS}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
@endsection