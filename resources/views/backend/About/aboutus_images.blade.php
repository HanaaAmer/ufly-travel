@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
     setTimeout(function () {
   $('#myModal').modal('hide');
}, 4000); 
  }
  
  
</script>
<div class="row">

    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.image')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if($aboutusImage != null) 
                    <form method="post" action="{{route('updateaboutusImages')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-body">
                               
                           <div class="form-group">
                                <label class="control-label">{{trans('backend.image')}}</label>
                                
                                   
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image[]" type="file"  class="form-control" multiple >
                                    
                                    
                                </div> 
                              

                          
                        </div>
                        </div>
                       
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllaboutusImages')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div> 
                      
                    </form>
                     <div class="form-body">
                     <div class="album">
                        <label class="control-label">{{trans('backend.storedImages')}}</label>
                        <div class="row">
                            @foreach($aboutusImage->image as $i=>$itemimage ) 
                            <div class="col-sm-6 col-md-3">
                                <a class="thumbnail">
                               
                                    <img height="100" src="{{ASSETS}}/images/About/{{$itemimage}}">
                       
                                </a>
                                <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteSingleaboutusImage',['images'=>$itemimage,'id'=>$aboutusImage->id])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn red">{{trans('backend.delete')}}
                                    </button>
                                </form>
                            </div>
                            @endforeach
                        </div>
                           </div>
                           </div>
                            @endif
                           @if($aboutusImage == null)
                           <form method="post" action="{{route('addaboutusImages')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-body">
                               
                            
                            <div class="from-group">

                           <label class="control-label">{{trans('backend.image')}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image[]" type="file"  class="form-control" multiple required>
                                    </div>
                           </div>
                     </div>
                    
      


                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllaboutusImages')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                      
                    </form>  
                     @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
  
 @endsection