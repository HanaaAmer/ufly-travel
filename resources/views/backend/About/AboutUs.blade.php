@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
     setTimeout(function () {
   $('#myModal').modal('hide');
}, 4000); 
  }
  
  
</script>
<div class="row">

    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.aboutus')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if($about != null) 
                    <form method="post" action="{{route('updateabout')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('About.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{$about->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('About.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('About.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="ckeditor" placeholder="{{trans('About.description',[''],$locale)}}" required>{{$about->translate($locale)->description}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                  <label class="control-label">{{trans('About.description',[''],$locale)}}</label>
                                  <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('About.vision_mission',[''],$locale)}}</label>
                                <textarea name="vision:{{$locale}}" class="ckeditor" placeholder="{{trans('About.vision_mission',[''],$locale)}}" required>{{$about->translate($locale)->vision}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                  <label class="control-label">{{trans('About.description',[''],$locale)}}</label>
                                  <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                           
                            
                    <div class="form-group">
                            <label class="control-label">{{trans('backend.about_goals',[''],$locale)}}</label>
                             <div class="input-group control-group increment" >
                             <table class="table table-striped table-bordered table-hover goal{{$locale}}" ><tr>
                           
                               <td>
                            
                               @for ($i = 0; $i < count($about->translate($locale)->about_goals); $i++ )
                                   
                                  <input type="text" name="about_goals:{{$locale}}[]" class="form-control" placeholder="{{trans('backend.about_goals',[''],$locale)}}"  value="{{$about->translate($locale)->about_goals[$i]}}" required>
                                  
                             @endfor 
                         
                                 </td>  
                                  <td>  <button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
                                </tr></table>
                              
                              </div>
                        </div>
                    </div>
                        @endforeach
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('About.image_description')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_description" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/About/{{$about->image_description}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('About.image_vision')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_vision" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/About/{{$about->image_vision}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('About.image_goals')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_goals" type="file" class="form-control" >
                                     <img height="70" src="{{ASSETS}}/images/About/{{$about->image_goals}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAboutus')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                        
                          </form>
                            @endif
                           @if($about == null)
                           <form method="post" action="{{route('addabout')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('About.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}"  value="{{old('title:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('About.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('About.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="ckeditor" placeholder="{{trans('About.description',[''],$locale)}}" required> {{old('description:'.$locale)}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('About.vision_mission',[''],$locale)}}</label>
                                <textarea name="vision_mission:{{$locale}}" class="ckeditor" placeholder="{{trans('About.vision_mission',[''],$locale)}}" required>{{old('vision_mission:'.$locale)}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            
                        <div class="form-group">
                       
                            <label class="control-label">{{trans('backend.about_goals',[''],$locale)}}</label>
                             <div class="input-group control-group increment" > 
                           
                             <table class="table table-striped table-bordered table-hover goal{{$locale}}" ><tr>
                           
                               <td>
                              
                                   
                             
                              <input type="text" name="about_goals:{{$locale}}[]" class="form-control" placeholder="{{trans('backend.about_goals',[''],$locale)}}"  value="{{old('about_goals:'.$locale)}}" required>
                           
                          
                                 </td>   
                                <td><button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </td></tr></table>
                              
                              
                                    
                              </div>
                        </div>
                </div>
                         @endforeach
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('About.image_description')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_description" type="file" class="form-control" required>
                                    
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('About.image_vision')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_vision" type="file" class="form-control" required>
                                   
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('About.image_goals')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image_goals" type="file" class="form-control" required>
                                    
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAboutus')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                      
                    </form>   @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
  <script type="text/javascript">
        var i=1;
        var j=1;
        var x=1;

    $(document).ready(function() {
       
     $('.addar').click(function(){
       
         
          $('.goalar').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="about_goals:ar[]" value="{{old('about_goals:ar')}}" placeholder=" اهداف الشركة" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
          });
         $('.adden').click(function(){
          $('.goalen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="about_goals:en[]" value="{{old('about_goals:en')}}" placeholder="Company Goals" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
     });

     $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
     });
    });
    


</script>

 @endsection