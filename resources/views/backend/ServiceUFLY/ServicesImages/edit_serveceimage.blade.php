@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.update_Service')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateServiceImageById',['ServiceImageId'=>$ServiceImage->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                             
                                    
                         <div class="from-group">
                           <label class="control-label">{{trans('service.name')}}</label>
                          <select name="tourist_services_id" class="form-control" required>
                                        <option value="">--{{trans('service.SelectService')}}--</option>
                                        @foreach ($Services as $Service )
                                        <option value="{{$Service->id }}"{{ $selectedService ==$Service->id? 'selected="selected"' : '' }} >{{ $Service->name }}</option>   
                                        @endforeach
                                    </select>
                          
                          </div>
                        
                          <div class="form-group">
                                <label class="control-label">{{trans('backend.image')}}</label>
                                
                                   
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image[]" type="file"  class="form-control" multiple>
                                  
                                    
                                </div> 
                              

                          
                        </div>
                       
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllServiceImage')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <div class="album">
                        <label class="control-label">{{trans('backend.storedImages')}}</label>
                        <div class="row">
                            @foreach($ServiceImage->image as $i=>$itemimage ) 
                            <div class="col-sm-6 col-md-3">
                                <a class="thumbnail">
                                
                                    <img height="100" src="{{ASSETS}}/images/services/{{$itemimage}}">
                       
                                </a>
                                <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteSingleImage',['images'=>$itemimage,'id'=>$ServiceImage->id])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn red">{{trans('backend.delete')}}
                                    </button>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>

@endsection