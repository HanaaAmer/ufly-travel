@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.update_Service')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateServiceByIdufly',['ServiceId'=>$Service->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                               @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('service.name',[''],$locale)}}</label>
                                <input name="name:{{$locale}}" value="{{ $Service->translate($locale)->name}}" type="text" class="form-control" placeholder="{{trans('service.name',[''],$locale)}}" required>
                                @if($errors->has('name:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('service.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{$Service->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('service.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('service.short_description',[''],$locale)}}</label>
                                <textarea name="descrption:{{$locale}}" class="form-control" placeholder="{{trans('service.short_description',[''],$locale)}}" maxlength="120" required>{{$Service->translate($locale)->descrption}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('service.long_description',[''],$locale)}}</label>
                                <textarea name="long_descrption:{{$locale}}" class="ckeditor" placeholder="{{trans('service.long_description',[''],$locale)}}" required>{!! html_entity_decode($Service->translate($locale)->long_descrption) !!}</textarea>
                                @if($errors->has('long_descrption:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('long_descrption'.$locale)}}
                                    </span>
                                @endif
                            </div>
                         <div class="form-group">
                            <label class="control-label">{{trans('service.services',[''],$locale)}}</label>
                             <div class="input-group control-group increment" >
                             <table class="table table-striped table-bordered table-hover place{{$locale}}"><tr>
                           
                               <td>
                              @for ($i = 0; $i < count($Service->translate($locale)->services); $i++ )
                                   
                             
                              <input type="text" name="services:{{$locale}}[]" class="form-control" placeholder="{{trans('service.services',[''],$locale)}}" value="{{$Service->translate($locale)->services[$i]}}" >
                                 @endfor
                                 </td> 
                                  <td><button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
                                </tr></table>
                               
                              </div>
                        </div>
                    </div>
                        @endforeach
                            
        
                          <div class="form-group">
                                <label class="control-label">{{trans('service.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    <img height="100" src="{{ASSETS}}/images/services/{{$Service->image}}" >
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('service.imageservices')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="image_services" type="file" class="form-control" >
                                    <img height="100" src="{{ASSETS}}/images/services/{{$Service->image_services}}">
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                        
                          
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllServicesufly')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
     
    <script type="text/javascript">
        var i=1;
        var j=1;
        var x=1;

    $(document).ready(function() {
       
     $('.addar').click(function(){
       
         
          $('.placear').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="services:ar[]" value="{{old('services:ar')}}" placeholder=" الخدمات " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
          });
         $('.adden').click(function(){
          $('.placeen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="services:en[]" value="{{old('services:en')}}" placeholder=" Services " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
     });

     $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
     });
    });
    

</script>
@endsection