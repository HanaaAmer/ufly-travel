@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.show')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                 
                        <div class="form-body">
                            
                            
        
                          <div class="form-group">
                                <label class="control-label">{{trans('booking.fullname')}}</label>
                                <div class="input-group">
                                  <input  class="form-control" type="text" value="{{$booking->fullname}}" readonly> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('booking.email')}}</label>
                                <div class="input-group">
                                   <input  class="form-control" type="text" value="{{$booking->email}}" readonly> 
                                </div>
                            </div>
                          
                          <div class="from-group">
                           <label class="control-label">{{trans('booking.phone')}}</label>
                          
                          <div class="input-group">
                                   <input  class="form-control" type="text" value="{{$booking->phone}}" readonly> 
                                </div>
                          </div>
                          <div class="form-group">
                                <label class="control-label">{{trans('city.name')}}</label>
                                <div class="input-group">
                               @foreach ($individual as $IND )
                                    
                               
                                   <input  class="form-control" type="text" value="{{$IND->Cities->name}}" readonly> 
                                @endforeach 
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label">{{trans('hotel.name')}}</label>
                                <div class="input-group">
                                  @foreach ($individual as $IND2 )
                                    
                               
                                  <input  class="form-control" type="text" value="{{$IND2->Hotel->name}}" readonly> 
                                @endforeach
                                </div>
                            </div>
                            
                          
                          <div class="from-group">
                           <label class="control-label">{{trans('booking.TravelersNumber')}}</label>
                          
                          <div class="input-group">
                                   <input  class="form-control" type="number" value="{{$booking->Travelers_Number}}" readonly> 
                                </div>
                          </div>
                            
                          <div class="from-group">
                           <label class="control-label">{{trans('booking.NumberofDays')}}</label>
                          
                          <div class="input-group">
                                   <input  class="form-control" type="number" value="{{$booking->Number_of_Days}}" readonly> 
                                </div>
                          </div>
                           <div class="from-group">
                           <label class="control-label">{{trans('booking.ArrivalDate')}}</label>
                          
                          <div class="input-group">
                                   <input  class="form-control" type="date" value="{{$booking->Arrival_Date}}" readonly> 
                                </div>
                          </div>
                       <div class="from-group">
                           <label class="control-label">{{trans('booking.note')}}</label>
                        
                          <div >
                                    <textarea  class="form-control"  readonly>{{$booking->note}}</textarea>
                                </div>
                          </div>

                          
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                             
                                <a class="btn btn-danger" href="{{route('getAllindividualbooking')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
     
   
@endsection