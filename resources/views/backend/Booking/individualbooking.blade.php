@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
   
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>{{trans('backend.IndividualBooking')}}</div>
                    <div class="tools">               
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('booking.fullname')}}</th>
                                <th>{{trans('booking.email')}}</th>
                               
                                <th>{{trans('booking.phone')}}</th>
                               
                               <th width="1%">{{trans('backend.action')}}</th>
                              <th width="1%"></th> 

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($individualbooking as $i=>$booking)
                            <tr class="odd gradeX">
                                <td>{{$i+1}}</td>
                                <td>{{$booking->fullname}}</td>
                                <td>{{$booking->email}}</td>
                               
                                <td>{{$booking->phone}}</td>
                               
                                <td>
                                <div class="clearfix">
                                    <a class="btn green btn-outline" href="{{route('getshowindividualbookingById',['bookingId'=>$booking->id])}}">
                                        {{trans('backend.show')}}
                                    </a>
                                </div>
                                </td>
                                 <td>
                                <div class="clearfix">
                                    <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteindividualBooking',['bookingId'=>$booking->id])}}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn red btn-outline">{{trans('backend.delete')}}
                                        </button>
                                    </form>
                                </div>  
                                </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
@section('js')
<script src="{{ASSETS}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ASSETS}}/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
@endsection
@section('css')
<link href="{{ASSETS}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
@endsection