@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.update_place')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateplacesById',['placeId'=>$place->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            
                                  @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('City.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{$place->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('City.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('City.descrption',[''],$locale)}}</label>
                                <textarea name="short_description:{{$locale}}" class="form-control" placeholder="{{trans('City.descrption',[''],$locale)}}" maxlength="120" required>{{$place->translate($locale)->short_description}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('City.long_descrption',[''],$locale)}}</label>
                                <textarea name="long_description:{{$locale}}" class="ckeditor" placeholder="{{trans('City.long_descrption',[''],$locale)}}" required>{!! html_entity_decode($place->translate($locale)->long_description) !!}</textarea>
                                @if($errors->has('long_descrption:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('long_descrption'.$locale)}}
                                    </span>
                                @endif
                            </div>
                        
                    </div>
                        @endforeach
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('backend.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    <img height="100" src="{{ASSETS}}/images/Places/{{$place->image}}">
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                             
                          <div class="from-group">
                               <label class="control-label">{{trans('City.name')}}</label>
                          <select name="City_id" class="form-control">
                                        <option value="">--{{trans('City.SelectCity')}}--</option>
                                        @foreach ($Cities as $city )
                                        <option value="{{$city->id }}"{{ $selectedCity ==$city->id? 'selected="selected"' : '' }} >{{ $city->name }}</option>   
                                        @endforeach
                                    </select>
                          
                          </div>
                             <div class="form-group">
                                <label class="control-label">{{trans('backend.images')}}</label>
                                
                                   
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image_slider[]" type="file"  class="form-control" multiple >
                                    
                                    
                                </div> 
                              

                          
                        </div>
                        
                          
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllplaces')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                     <div class="album">
                        <label class="control-label">{{trans('backend.storedImages')}}</label>
                        <div class="row">
                            @foreach($place->image_slider as $i=>$itemimage ) 
                            <div class="col-sm-6 col-md-3">
                                <a class="thumbnail">
                               
                                    <img height="100" src="{{ASSETS}}/images/Places/{{$itemimage}}">
                       
                                </a>
                                <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteSingleimageplace',['images'=>$itemimage,'id'=>$place->id])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn red">{{trans('backend.delete')}}
                                    </button>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
@endsection