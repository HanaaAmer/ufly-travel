@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.add_image')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddCityImages')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                           
                            
            
                                  <div class="from-group">
                                    <label class="control-label">{{trans('City.name')}}</label>
                          <select name="City_id" class="form-control" required>
                                        <option value="">--{{trans('City.SelectCity')}}--</option>
                                        @foreach ($Cities as $City)
                                        <option value="{{$City->id }}"> {{$City->name}}</option>     
                                        @endforeach
                                    </select>
                          
                          </div>
<br>
                         <div class="from-group">

                           <label class="control-label">{{trans('backend.image')}}</label>
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image[]" type="file"  class="form-control" multiple required>
                                  
                                    
                        
                                    
                                </div> 

                    </div>
      


                        
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllCityImages')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                         </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>

@endsection