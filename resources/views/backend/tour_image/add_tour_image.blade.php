@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('tour.add_tour_images')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddTour_image')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                                             
                        <div class="form-group">
                            <label class="control-label">{{trans('tour.name')}}</label>        
                            <select name="tour_id" id="tour_id"  class="form-control" required>
                                <option value="">{{ "please_select" }}</option>
                                @foreach($tours as $itis)                          
                                  <option value="{{ $itis->id }}">{{ $itis->name }}</option>
                                @endforeach
                              </select>
                        </div>
                           
                            <div class="form-group">
 
                                        <label class="control-label">{{trans('tour.image')}}</label>
                                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-picture-o"></i>
                            </span> 
                            <input name="image[]" type="file" class="form-control" multiple required>                            
                           
                    </div>
                </div>
                        
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllTour_Images')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    
@endsection