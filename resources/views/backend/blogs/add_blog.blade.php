@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('blog.add_news')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddBlog')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label">اللغة</label>
                                <div class="radio">
                                    <label><input type="radio" name="type" value="ar"  checked="checked">Arabic</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="type" value="en">English</label>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="single-prepend-text" class="control-label">{{trans('blog.blog_category')}}</label>
                                <div class="input-group select2-bootstrap-prepend">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                    <select name="category_id" id="single-prepend-text" class="form-control select2">
                                        @foreach($categories as $i=>$category)
                                        <option value="{{$category->category_id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="tap_ar">
                                <div class="form-group">
                                    <label class="control-label">{{trans('slider.title')}}</label>
                                    <input name="title:ar" value="{{old('title:ar')}}" type="text" class="form-control" placeholder="{{trans('slider.title')}}" >
                                    @if($errors->has('title:ar'))
                                        <span class="help-block">{{$errors->first('title')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('slider.short_description')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <textarea name="short_description:ar" class="form-control" placeholder="{{trans('slider.short_description')}}">{{old('short_description:ar')}}</textarea>
                                    </div>
                                    @if($errors->has('short_description:ar'))
                                        <span class="help-block">{{$errors->first('short_description')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('slider.long_description')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <textarea name="long_description:ar" class="form-control" placeholder="{{trans('slider.long_description')}}">{{old('long_description:ar')}}</textarea>
                                    </div>
                                    @if($errors->has('long_description:ar'))
                                        <span class="help-block">{{$errors->first('long_description:ar')}}</span>
                                    @endif
                                </div>
                            </div>    
                            <div id="tap_en">
                                <div class="form-group">
                                    <label class="control-label">Tittle</label>
                                    <input name="title:en" value="{{old('title:en')}}" type="text" class="form-control" placeholder="{{trans('slider.title')}}" >
                                    @if($errors->has('title'))
                                        <span class="help-block">{{$errors->first('title:en')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Short Description</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <textarea name="short_description:en" class="form-control" placeholder="Short Description">{{old('short_description:en')}}</textarea>
                                    </div>
                                    @if($errors->has('short_description:en'))
                                        <span class="help-block">{{$errors->first('short_description:en')}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Long Description</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <textarea name="long_description:en" class="form-control" placeholder="Long Description">{{old('long_description:en')}}</textarea>
                                    </div>
                                    @if($errors->has('long_description:en'))
                                        <span class="help-block">{{$errors->first('long_description:en')}}</span>
                                    @endif
                                </div>
                                </div>      
                              
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('blog.keywards')}}</label>
                                <input name="keywards" value="{{old('keywards')}}" type="text" class="form-control"  placeholder="{{trans('blog.keywards')}}" >
                                @if($errors->has('keywards'))
                                    <span class="help-block">{{$errors->first('keywards')}}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{trans('blog.active')}}</label>
                                <div class="input-group">
                                    <input type="checkbox" name="active" class="checkbox" value="1" checked>
                                </div>    
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('blog.slider')}}</label>
                                <div class="input-group">
                                    <input type="checkbox" name="slider" class="checkbox" value="1">
                                </div>    
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('blog.breaking')}}</label>
                                <div class="input-group">
                                    <input type="checkbox" name="breaking" class="checkbox" value="1">
                                </div>    
                                
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{trans('blog.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="single-prepend-text" class="control-label">{{trans('blog.used_tags')}}</label>
                                <div class="input-group select2-bootstrap-prepend">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                    <select name="tag[]" multiple="multiple" id="single-prepend-text" class="form-control select2">
                                        <option value="">{{ trans('blog.select_tags') }}</option>
                                        @foreach($tags as $i=>$tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('blog.new_tags')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-leanpub"></i>
                                    </span>
                                    <input name="tags" value="{{old('tags')}}" type="text" class="form-control" placeholder="{{trans('blog.new_tags')}}" >
                                </div>
                                <span class="help-block">{{ trans('blog.tags_help') }}</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllBlogs')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    @endsection