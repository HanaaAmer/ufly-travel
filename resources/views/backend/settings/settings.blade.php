@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
  }
  
  
</script>
  
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('link.update_link')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if ($setting != null)
                        
                   
                    <form method="post" action="{{route('updateSettings')}}">
                        {{ csrf_field() }}
                       
                          <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('setting.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('setting.description',[''],$locale)}}" maxlength="100" required>{{$setting->translate($locale)->description}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.contactus_description',[''],$locale)}}</label>
                                <textarea name="contactus_description:{{$locale}}" class="form-control" placeholder="{{trans('setting.contactus_description',[''],$locale)}}" maxlength="100" required>{{$setting->translate($locale)->contactus_description}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            
                    
                    </div>
                        @endforeach
                            
                           
                        
                
                    
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.email')}}</label>
                                <input name="email" value="{{$setting->email}}" type="email" class="form-control" placeholder="{{trans('setting.email')}}" required>
                                @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                           
                              <div class="form-group">
                                <label class="control-label">{{trans('setting.phone')}}</label>
                                <input name="phone" value="{{$setting->phone}}" type="text" class="form-control" placeholder="{{trans('setting.phone')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                           
                             <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_number')}}</label>
                                <input name="whatsapp_number" value="{{$setting->whatsapp_number}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_number')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                            
                           <div class="form-group">
                                <label class="control-label">{{trans('setting.fax')}}</label>
                                <input name="fax" value="{{$setting->fax}}" type="text" class="form-control" placeholder="{{trans('setting.fax')}}" required>
                                @if($errors->has('fax'))
                                <span class="help-block">{{$errors->first('fax')}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.facebook_link')}}</label>
                                <input name="link_facebook" value="{{$setting->link_facebook}}" type="text" class="form-control" placeholder="{{trans('setting.facebook_link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.twitter_link')}} </label>
                                <input name="link_twitter" value="{{$setting->link_twitter}}" type="text" class="form-control" placeholder="{{trans('setting.twitter_link')}}" required>
                                @if($errors->has('twitter_link'))
                                <span class="help-block">{{$errors->first('twitter_link')}}</span>
                                @endif
                            </div>   
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_link')}} </label>
                                <input name="link_whatsapp" value="{{$setting->link_whatsapp}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_link')}}" required>
                                @if($errors->has('whatsapp'))
                                <span class="help-block">{{$errors->first('whatsapp')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                 <label class="control-label"> {{trans('setting.snapchat_link')}} </label>
                                <input name="link_snapchat" value="{{$setting->link_snapchat}}" type="text" class="form-control" placeholder="{{trans('setting.snapchat_link')}}" required>
                                @if($errors->has('youtube'))
                                <span class="help-block">{{$errors->first('youtube')}}</span>
                                @endif
                            </div>
                         
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.instagram_link')}}</label>
                                <input name="link_instagram" value="{{$setting->link_instagram}}" type="text" class="form-control" placeholder="{{trans('setting.instagram_link')}}" required>
                                @if($errors->has('instagram_link'))
                                <span class="help-block">{{$errors->first('instagram_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.linkend_link')}}</label>
                                <input name="link_linkedin" value="{{$setting->link_linkedin}}" type="text" class="form-control" placeholder="{{trans('setting.linkend_link')}}" required>
                                @if($errors->has('google_link'))
                                <span class="help-block">{{$errors->first('google_link')}}</span>
                                @endif
                            </div>
                          
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.map')}}</label>
                                <div class="acf-map">
			                	<div class="marker"></div>
                                </div>
                                <input  name="map_lat" value="{{$setting->map_lat}}"  required>
                                <input  name="map_lng"  value="{{$setting->map_lng}}" required>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getSettings')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form> 
                    @endif
                    @if ($setting == null){
                        <form method="post" action="{{route('addSettings')}}">
                        {{ csrf_field() }}
                       
                          <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('setting.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('setting.description',[''],$locale)}}" maxlength="100" required>{{old('description:'.$locale)}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.contactus_description',[''],$locale)}}</label>
                                <textarea name="contactus_description:{{$locale}}" class="form-control" placeholder="{{trans('setting.contactus_description',[''],$locale)}}" maxlength="100" required>{{old('contactus_description:'.$locale)}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            
                    
                    </div>
                        @endforeach
                            
                           
                        
                
                    
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.email')}}</label>
                                <input name="email" value="{{old('email:'.$locale)}}" type="email" class="form-control" placeholder="{{trans('setting.email')}}" required>
                                @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                           
                              <div class="form-group">
                                <label class="control-label">{{trans('setting.phone')}}</label>
                                <input name="phone" value="{{old('phone:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.phone')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                           
                             <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_number')}}</label>
                                <input name="whatsapp_number" value="{{$setting->whatsapp_number}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_number')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                            
                           <div class="form-group">
                                <label class="control-label">{{trans('setting.fax')}}</label>
                                <input name="fax" value="{{old('fax:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.fax')}}" required>
                                @if($errors->has('fax'))
                                <span class="help-block">{{$errors->first('fax')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.facebook_link')}}</label>
                                <input name="link_facebook" value="{{old('link_facebook:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.facebook_link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.twitter_link')}} </label>
                                <input name="link_twitter" value="{{old('link_twitter:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.twitter_link')}}" required>
                                @if($errors->has('twitter_link'))
                                <span class="help-block">{{$errors->first('twitter_link')}}</span>
                                @endif
                            </div>   
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_link')}} </label>
                                <input name="link_whatsapp" value="{{old('link_whatsapp:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_link')}}" required>
                                @if($errors->has('whatsapp'))
                                <span class="help-block">{{$errors->first('whatsapp')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                 <label class="control-label"> {{trans('setting.snapchat_link')}} </label>
                                <input name="link_snapchat" value="{{old('link_snapchat:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.snapchat_link')}}" required>
                                @if($errors->has('youtube'))
                                <span class="help-block">{{$errors->first('youtube')}}</span>
                                @endif
                            </div>
                         
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.instagram_link')}}</label>
                                <input name="link_instagram" value="{{old('link_instagram:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.instagram_link')}}" required>
                                @if($errors->has('instagram_link'))
                                <span class="help-block">{{$errors->first('instagram_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.linkend_link')}}</label>
                                <input name="link_linkedin" value="{{old('link_linkedin:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.linkend_link')}}" required>
                                @if($errors->has('google_link'))
                                <span class="help-block">{{$errors->first('google_link')}}</span>
                                @endif
                            </div>
                           
                          
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.map')}}</label>
                                <div class="acf-map">
			                	<div class="marker"></div>
                                </div>
                                <input  name="map_lat" value="{{old('map_lat:'.$locale)}}" required>
                                <input  name="map_lng"  value="{{old('map_lng:'.$locale)}}" required>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getSettings')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    }
                        
                    @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
  <style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnEX6zLgaPJTW1Eb7rJJoADHvtNzee8hs"></script>
<script type="text/javascript">
(function($) {
// global var
var map = null;
var markersArray = [];
/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 10,
		center		: new google.maps.LatLng(3.15, 101.74),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};

	// create map	        	
	var map = new google.maps.Map( $el[0], args);

	// return
	return map;
	
}

function placeMarker(location) {
	// first remove all markers if there are any
	deleteOverlays();

	var marker = new google.maps.Marker({
		position: location, 
		map: map
	});

	// add marker in markers array
	markersArray.push(marker);

	//map.setCenter(location);
	map.panTo(location);
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
	if (markersArray) {
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
	markersArray.length = 0;
	}
}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
@if($setting != null)
$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );
	
		var old_lat = "<?=$setting->map_lat?>";
		var old_lng = "<?=$setting->map_lng?>";
          
		if (old_lat !== "" && old_lng !== "") {
			placeMarker(new google.maps.LatLng(old_lat, old_lng));
		}

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
			$("input[name='map_lat']").val(event.latLng.lat().toFixed(3));
			$("input[name='map_lng']").val(event.latLng.lng().toFixed(3));
		}); 

	});

});
@endif
@if($setting == null)
$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );
	
		var old_lat = "3.15";
		var old_lng = "101.74";
          
		if (old_lat !== "" && old_lng !== "") {
			placeMarker(new google.maps.LatLng(old_lat, old_lng));
		}

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
			$("input[name='map_lat']").val(event.latLng.lat().toFixed(3));
			$("input[name='map_lng']").val(event.latLng.lng().toFixed(3));
		}); 

	});

});
@endif
})(jQuery);
</script>
 @endsection