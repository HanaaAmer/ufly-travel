@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('hotel.add_hotel_images')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddHotel_image')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                                <div class="form-group">
                                        <label class="control-label">{{trans('hotel.city')}}</label>        
                                        <select name="city_id"  id="city_id"  class="form-control">
                                            <option value="">{{ "please_select" }}</option>
                                            @foreach($items as $item)                          
                                              <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                          </select>
                                    </div>                        
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.name')}}</label>        
                            <select name="hotel_id" id="hotel_id"  class="form-control">
                                <option value="">{{ "please_select" }}</option>
                                @foreach($hotels as $hotel)                          
                                  <option data="{{ $hotel->city_id }}"value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                                @endforeach
                              </select>
                        </div>
                           
                            <div class="form-group">
                               
                                        <label class="control-label">{{trans('hotel.image')}}</label>
                                        
                               <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                    <input name="image[]" type="file" class="form-control" multiple required>                            
                                   
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllHotels')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
     <script>
            var i=1; 
           var $city_id = $( '#city_id' ),
           $hotel_id = $( '#hotel_id' ),
           $options = $hotel_id.find( 'option' );
         
       
           $city_id.on( 'change', function() {
           $hotel_id.html( $options.filter( '[data="' + this.value + '"]' ) );
            } ).trigger( 'change' );
           </script>
   
@endsection