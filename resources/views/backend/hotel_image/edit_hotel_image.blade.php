@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('hotel.edit_hotel')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateHotel_imageById',['hotelId'=>$hotel->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">                        
                            
                        <div class="form-group">
                            <label class="control-label">{{trans('hotel.city')}}</label>
                            <select name="city_id" id="city_id" class="form-control">
                                    <option value="">{{ "please_select" }}</option>           
                                @foreach($items as $item)                      
                                  <option value="{{ $item->id }}"{{ $selecteditem == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                                @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                                <label class="control-label">{{trans('hotel.name')}}</label>
                                <select name="hotel_id"  id="hotel_id"class="form-control">
                                        <option value="">{{ "please_select" }}</option>           
                                    @foreach($hotels as $selec)                      
                                      <option data={{$selec->city_id }} value="{{ $selec->id }}"{{ $selectedhotel == $selec->id ? 'selected="selected"' : '' }}>{{ $selec->name }}</option>
                                    @endforeach
                                  </select>
                                </div>
                           
                            <div class="form-group">
                                        <label class="control-label">{{trans('hotel.image')}}</label>
                                         <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span> 
                                  
                                    <input name="image[]" type="file"  class="form-control" multiple >
                                    
                                </div> 
                        </div>
                        </form>
                     <div class="album">
                        <label class="control-label">{{trans('backend.storedImages')}}</label>
                        <div class="row">
                            @foreach($hotel->image as $i=>$its ) 
                            <div class="col-sm-6 col-md-3">
                                <a class="thumbnail">
                               
                                    <img height="100" src="{{ASSETS}}/images/hotels/{{$its}}">
                       
                                </a>
                                <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteSingleImagehotel',['images'=>$its,'id'=>$hotel->id])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn red">{{trans('backend.delete')}}
                                    </button>
                                </form>
                            </div>
                            @endforeach
                            </div>                   
                                   
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllHotel_Images')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input type="hidden" name="_method" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <script>
            var i=1; 
               var $city_id = $( '#city_id' ),
               $hotel_id = $( '#hotel_id' ),
               $options = $hotel_id.find( 'option' );
              
        
               $city_id.on( 'change', function() {
                   
               $hotel_id.html( $options.filter( '[data="' + this.value + '"]' ) );  
               $( '#hotel_id' ).val("{{$hotel->hotel_id}}");        
                } ).trigger( 'change' );
               </script>
@endsection