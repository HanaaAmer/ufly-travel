@extends('frontend.layouts.defult')
@section('content')  
@if($place != null)

    <div class="page-head">
            <div class="container">
                <h1>{{$place->title}}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('cities')}}">{{ \App\Http\Controllers\HomeController::translateWord('TOURIST CITIES')}}</a>
                   
                      <a href="{{route('citydetails',['CityId'=>$place->City_id,'countryId'=>$place->cities->country_id])}}">{{$place->cities->name}}</a>
                    <a >{{$place->title}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- City Details -->
      
        <div class="city-details container">
           <div class="row">
           <div class="col-12 col-l-6">
           <h2>{{$place->title}}</h2>
            {!! html_entity_decode($place->long_description) !!}
           </div>
           <div class="col-12 col-l-6">
                <div class="single-slider slider-component">
                @foreach ($place->image_slider as $image)
                    
              
                    <a href="{{ASSETS}}/images/Places/{{$image}}" data-src="{{ASSETS}}/images/Places/{{$image}}" style="display:block;padding-bottom:75%;" target="_blank"></a>
                 @endforeach 
                 </div>
           </div>
           
           
           </div>            
            
            
            
          
        </div>
       @endif
        

@endsection