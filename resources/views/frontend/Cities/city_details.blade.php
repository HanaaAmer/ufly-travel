@extends('frontend.layouts.defult')
@section('content')  
@if($Cities != null)
@foreach ($Cities as $City)
    <div class="page-head">
            <div class="container">
                <h1>{{$City->name}}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('cities')}}">{{ \App\Http\Controllers\HomeController::translateWord('TOURIST CITIES')}}</a>
                    <a >{{$City->name}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- City Details -->
      
        <div class="city-details container">
           
            <img  src="{{ASSETS}}/images/Cities/{{$City->image}}" alt="" class="block-lvl cover" stly="height:10%" >
            <h2>{{$City->title}}</h2>
            {!! html_entity_decode($City->long_descrption) !!}
            
            
            
            <!-- Gallery Shots -->
            <div class="media-sliderx row">
           @if($images != null)
             @foreach ($images->image as $itemimage)
                <!-- Image -->
                <div class="col-6 col-m-3"><a href="{{ASSETS}}/images/Cities/{{$itemimage}}" data-lightbox="gallery-1" class="light-btn ti-search"><img src="{{ASSETS}}/images/Cities/{{$itemimage}}" alt="title"></a></div>
                @endforeach
            @endif
            </div>
          
        </div>
       
        <!-- // City Details -->

        <!-- More Details -->
         <div class="container page-content"> 
          @if($place != null)
         <h2 class="section-title">
              
                 <span>{{ \App\Http\Controllers\HomeController::translateWord('Touer Places')}} {{ \App\Http\Controllers\HomeController::translateWord('in')}} : {{$City->name}} </span>
            </h2>
            <!-- Grid -->
          
            <div class="row">
                <!-- City Block -->
                @foreach ($place as $itemplace)
                    
                
                <div class="col-12 col-m-6 col-l-4 city-block">
                    <div class="content-box">
                        <a href="{{route('placesdetails',['placeId'=>$itemplace->id])}}" class="image" data-src="{{ASSETS}}/images/Places/{{$itemplace->image}}"></a>
                        <a href="{{route('placesdetails',['placeId'=>$itemplace->id])}}"><h3>{{$itemplace->title}}</h3></a>
                
                         <a href="{{route('citydetails',['CityId'=>$itemplace->City_id,'countryId'=>$itemplace->cities->country_id])}}">{{ \App\Http\Controllers\HomeController::translateWord('location')}} : {{$City->name}}</a>
                        <p> {{$itemplace->short_description}}</p>
                        <a href="{{route('placesdetails',['placeId'=>$itemplace->id])}}" class="read-more">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                    </div>
                </div>
               @endforeach
            </div>
            <!-- Pagination -->
            {{ $place->links() }}
            @endif
        </div>
        <!-- // More Details -->
  @endforeach
 @endif
        <!-- Page content -->
        <div class="container page-content">
            <!-- Smiliar Tours --> 
             @if ($similercity != null)
            <h2 class="section-title">
                <i>{{ \App\Http\Controllers\HomeController::translateWord('ReadAboutMore')}}</i>
                 <span>{{ \App\Http\Controllers\HomeController::translateWord('SimiliarCities')}}</span>
            </h2>
            <!-- Grid -->
            <div class="row tours-slider">
                <!-- City Block -->
              
                    
               
                @foreach ($similercity as $similer)
                    
                
                <div class="col-12 col-m-6 col-l-4 city-block">
                    <div class="content-box">
                        <a href="{{route('citydetails',['CityId'=>$similer->id,'countryId'=>$similer->country_id])}}" class="image" data-src="{{ASSETS}}/images/Cities/{{$similer->image}}"></a>
                        <a href="{{route('citydetails',['CityId'=>$similer->id,'countryId'=>$similer->country_id])}}"><h3>{{$similer->name}}</h3></a>
                      <p> {{$City->descrption}}</p>
                        <a href="{{route('citydetails',['CityId'=>$similer->id,'countryId'=>$similer->country_id])}}" class="read-more">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                    </div>
                </div>
               @endforeach
                @endif
               
            </div>
        </div>
        <!-- // Page content -->

@endsection