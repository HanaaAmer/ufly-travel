@extends('frontend.layouts.defult')
@section('content')
    <div class="page-head">
            <div class="container">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('tourist_city')}}</h1>
                <div class="breadcrumb">
                     <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('cities')}}">{{ \App\Http\Controllers\HomeController::translateWord('tourist_city')}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Page content -->
        <div class="container page-content">
            <!-- Grid -->
            @if($Cities != null)
            <div class="row">
                <!-- City Block -->
                @foreach ($Cities as $City)
                    
                
                <div class="col-12 col-m-6 col-l-4 city-block">
                    <div class="content-box">
                        <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}" class="image" data-src="{{ASSETS}}/images/Cities/{{$City->image}}"></a>
                        <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}"><h3>{{$City->name}}</h3></a>
                        <p> {{$City->descrption}}</p>
                        <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}" class="read-more">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                    </div>
                </div>
               @endforeach
            </div>
            <!-- Pagination -->
            {{ $Cities->links() }}
            @endif
        </div>
@endsection