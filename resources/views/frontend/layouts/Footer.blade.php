
        <footer class="main-footer">
         <?php $home_exploore = \App\Repositories\home_explooreRepository::getAllHOMEEXPLOORE(); 
      $LINK = \App\Repositories\SettingRepository::getAllLINK();
      $cities = \App\Repositories\CityRepository::getcites(); 
               $services = \App\Repositories\ServiceUFLYRepository::getservicesufly();
                $offers = \App\Repositories\OfferRepository::getoffers(); 
                 $countries = \App\Repositories\CountryRepository::getcountry();
      
        ?>
            <div class="container">
                <div class="row">
                     @foreach ($countries as $country )
                    <div class="col-6 box-5x1">
                        <h2> {{ \App\Http\Controllers\HomeController::translateWord('offers')}} {{ \App\Http\Controllers\HomeController::translateWord('in')}} {{$country->name}}</h2>
                         <ul class="links-list">
                             
                            
                                   
                                    @foreach ($offers as $item )
                                   
                             
                                   <li><a href="{{route('touroffers',['offerId'=>$item->id,'countryId'=>$country->id])}}">{{$item->offer}}</a></li>
                                    @endforeach
                                   </ul>
                                  
                    </div> @endforeach
                    <div class="col-6 box-5x1">
                        <h2>{{ \App\Http\Controllers\HomeController::translateWord('hotels')}}</h2>
                        <ul class="links-list"> 
    
                           @foreach ($cities as $item )
                            
                      
                            <li><a href="{{route('hotelscity',['CityId'=>$item->id])}}">{{$item->name}}</a></li>
                             @endforeach
                        </ul>
                    </div>
                    
                    <div class="col-6 box-5x1">
                        <h2>{{ \App\Http\Controllers\HomeController::translateWord('TOURIST CITIES')}}</h2>
                        <ul class="links-list"> 
    
                           @foreach ($cities as $item )
                            
                      
                            <li><a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}">{{$item->name}}</a></li>
                             @endforeach
                        </ul>
                    </div>
                     <div class="col-6 box-5x1">
                        <h2>{{ \App\Http\Controllers\HomeController::translateWord('services')}}</h2>
                        <ul class="links-list"> 
    
                          @foreach($services as $Service )
                                <li><a href="{{route('Servicesdetails',['ServiceId'=>$Service->id])}}">{{$Service->name}}</a></li>
                                @endforeach
                        </ul>
                    </div>
                    
                    <!--<div class="col-12 col-l-5">-->
                    <!--    <h2>{{ \App\Http\Controllers\HomeController::translateWord('Get in Touch')}}</h2>-->
                    <!--    <div class="row no-gutter">-->
                    <!--        <div class="contact-info col-12 col-l-7">-->
                    <!--            <h3 class="ti-phone-classic">{{$LINK->phone}}</h3>-->
                    <!--            <h3 class="ti-mail">{{$LINK->email}}</h3>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            </div>
            <!-- Copyrights -->
            <div class="copyrights">
                <div class="container">
                    <span>{{ \App\Http\Controllers\HomeController::translateWord('All Copyrights Reserved for U.Flay')}}</span>
                    <div class="social">
                        <a target="_blank" href="{{ $LINK->link_facebook}}" class="ti-facebook"></a>
                        <a target="_blank" href="{{ $LINK->link_twitter}}" class="ti-twitter"></a>
                        <a target="_blank" href="{{$LINK->link_snapchat}}" class="ti-snapchat"></a>
                        <a target="_blank" href="{{$LINK->link_instagram}}" class="ti-instagram"></a>
                        <a target="_blank" target="_blank" href="{{$LINK->link_linkedin}}" class="ti-linkedin"></a>
                        <a  target="_blank" href="{{$LINK->link_whatsapp}}" class="whatsapp-btn ti-whatsapp"></a>
                       
                    </div>
                    <div class="mahacode-copyrights">
                        <a href="http://mahacode.com/" target="_blank" class="logo"><img src="{{ASSETS}}/img/mahacode.png" alt=""></a>
                        <div class="mc-tooltip">
                            <h3>{{ \App\Http\Controllers\HomeController::translateWord('Designed and Developed by Maha Code')}}</h3>
                            <h4 class="ti-mail">info@mahacode.com</h4>
                            <h4 class="ti-phone">+02686 4621312 14849 8789</h4>
                            <div class="btns-icons">
                                <a href="http://mahacode.com/" target="_blank" class="ti-home"></a>
                                <a href="https://www.linkedin.com/company/10801558" target="_blank" class="ti-linkedin"></a>
                                <a href="https://api.whatsapp.com/send?phone=00201093678012" target="_blank" class="ti-whatsapp-line"></a>
                                <a href="https://www.behance.net/mahacode" target="_blank" class="ti-behance"></a>
                                <a href="https://www.instagram.com/maha.code/" target="_blank" class="ti-instagram"></a>
                                <a href="http://www.twitter.com/mahacode" target="_blank" class="ti-twitter"></a>
                                <a href="https://www.facebook.com/MahaCode/" class="ti-facebook"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a href="#top" class="scroll-up ti-arrow-up-c"></a>
       <a href="https://api.whatsapp.com/send?phone={{$LINK->whatsapp_number}}" class="whatsapp-btn ti-whatsapp"></a>
        <!-- Chatra {literal} -->
<script>
    (function(d, w, c) {
        w.ChatraID = 'GpKLzKEBKknrfzuRn';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = 'https://call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->
