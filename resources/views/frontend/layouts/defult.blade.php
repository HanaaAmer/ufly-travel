<!DOCTYPE html>
<html lang="en" dir="rtl">
    <head>
        <!-- Required Meta Tags -->
        <meta name="language" content="ar">
        <meta http-equiv="x-ua-compatible" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="The Description of This Page Goes Right Here and its !Important" />
        <meta name="keywords" content="keywords,goes,here,for,this,web,site,its,!important,and,keep,it,dynamic" />
        <title>U.Flay | Travel & Tours | SDN.BHD</title>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Open Graph Meta Tags -->
        <meta property="og:title" content="This Page title Goes Here" />
        <meta property="og:description" content="The Description of This Page Goes Right Here and its !Important" />
        <meta property="og:url" content="http://domain.com/page-url/" />
        <meta property="og:image" content="{{ASSETS}}/img/logo.png" />
        <!-- Twitter Card Meta Tags -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="This Page title Goes Here">
        <meta name="twitter:description" content="The Description of This Page Goes Right Here and its !Important">
        <meta name="twitter:image" content="{{ASSETS}}/img/logo.png">
        <!-- Facebook Card Meta Tags -->
        <meta name="facebook:image" content="{{ASSETS}}/img/logo.png"/>
        <meta name="facebook:title" content="This Page title Goes Here" />
        <meta name="facebook:description" content="The Description of This Page Goes Right Here and its !Important" />
        <!-- Other Meta Tags -->
        <meta name="robots" content="index, follow" />
        <meta name="copyright" content="Sitename Goes Here">
		<link rel="shortcut icon" type="image/png"  href="{{ASSETS}}/img/logo.png">
        <!-- Required CSS Files -->
          @if(App::getLocale()=='ar')
<link rel="stylesheet" href="{{ASSETS}}/css/tornado-rtl.min.css">
@else
<link rel="stylesheet" href="{{ASSETS}}/css/tornado.min.css">
@endif
    
        
      
 
  
 
    </head>
    <body id="top">
          <?php $cities = \App\Repositories\CityRepository::getcites(); 
               $services = \App\Repositories\ServiceUFLYRepository::getservicesufly();
               $contactus = \App\Repositories\SettingRepository::getAllLINK();
                $offers = \App\Repositories\OfferRepository::getoffers();
                $countries = \App\Repositories\CountryRepository::getcountry();
      
        ?>
        <!-- Main Header -->
        <header class="tornado-header main-header wow" data-sticky>
            <div class="container">
                <a href="{{ route('gethome') }}" class="logo" style="background-size:100% auto;"></a>
                <!-- Navigation Menu -->
                <div class="navigation-menu" data-id="main-menu" data-logo="assets/img/logo.png">
                    <ul>
                            <li><a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('home_page')}}</a></li>
                            <li><a href="{{route('gettours')}}">{{ \App\Http\Controllers\HomeController::translateWord('Offers')}}</a>
                            <ul>
                             
                            
                             @foreach ($countries as $country )
                             <li><a href="#">{{$country->name}}</a>
                      

                          <ul> 
                             @foreach ($offers as $item )
                            
                      
                            <li><a href="{{route('touroffers',['offerId'=>$item->id,'countryId'=>$country->id])}}">{{$item->offer}}</a></li>
                             @endforeach
                            </ul>
                            </li>
                             @endforeach
                              </ul>
                            </li>
                            <li><a href="{{route('gethotels')}}">{{ \App\Http\Controllers\HomeController::translateWord('hotels')}}</a>
                            <ul>
                               @foreach($cities as $City )
                                <li><a href="{{route('hotelscity',['CityId'=>$City->id])}}">{{$City->name}}</a></li>
                               @endforeach
                            </ul>
                            </li>
                        <li><a href="{{route('cities')}}">{{ \App\Http\Controllers\HomeController::translateWord('TOURIST CITIES')}}</a>
                           <ul>
                               @foreach($cities as $City )
                                <li><a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}">{{$City->name}}</a></li>
                               @endforeach
                            </ul>
                        </li>
                        <li><a href="{{route('Services')}}">{{ \App\Http\Controllers\HomeController::translateWord('services')}}</a>
                          <ul>
                                @foreach($services as $Service )
                                <li><a href="{{route('Servicesdetails',['ServiceId'=>$Service->id])}}">{{$Service->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @php $links=\App\Repositories\LinkRepository::getAllFrontLinks()->where('active',1) @endphp
                        @foreach($links as $i=>$link)
                        @if(Route::has($link->route))
                        <li><a href="{{route($link->route)}}">{{$link->title}}</a></li>
                        @else
                        <li><a href="{{$link->route}}">{{$link->title}}</a></li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                <!-- Action Buttons -->
                <div class="action-btns">
                    <!-- Menu button -->
                    <a href="#" class="icon-btn menu-btn ti-menu-round" data-id="main-menu"></a>
                    <!-- Search Button -->
                  
                    <!-- Language Button -->
                   
                    @foreach (Config::get('languages') as $lang => $language)                     
                    @if ($lang != App::getLocale())                     
                    <a href="{{ route('lang.switch', $lang) }}" class="icon-btn ti-earth" data-title="{{$language}}"></a>                     @endif                     
                    @endforeach
                    <!-- Contact Button -->
                    <div class="dropdown">
                        <a href="#" class="icon-btn ti-phone-in-talk dropdown-btn"></a>
                        <ul class="dropdown-list contact-us">
                            <li><h3>{{ \App\Http\Controllers\HomeController::translateWord('contactus')}}</h3></li>
                            <li class="contact-item ti-mail">
                                <span class="title">{{ \App\Http\Controllers\HomeController::translateWord('email')}}</span>
                                <a href="mailto:{{$contactus->email}}">{{$contactus->email}}</a>
                                <p>{{ \App\Http\Controllers\HomeController::translateWord('Responding in 24 Hours')}}</p>
                            </li>
                            <li class="contact-item ti-whatsapp">
                                <span class="title">{{ \App\Http\Controllers\HomeController::translateWord('Whatsapp')}}</span>
                                <a target="_blank" href="https://api.whatsapp.com/send?phone={{$contactus->whatsapp_number}}">{{$contactus->whatsapp_number}}</a>
                                <p>{{ \App\Http\Controllers\HomeController::translateWord('Available 24h a day')}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- // Main Header -->
       @yield('content')
        <!-- // Page content -->


      <!-- Footer -->
  @include('frontend.layouts.Footer')
       <!-- Required JS Files -->
        <script src="{{ASSETS}}/js/jquery-3.3.1.min.js"></script>
        <script src="{{ASSETS}}/js/tornado.min.js"></script>
    </body>
</html>
