@extends('frontend.layouts.defult')
@section('content')
   @if(Session::has('sucess'))
                         
                          <div class="alert alert-success" >
 
                     {{ \App\Http\Controllers\HomeController::translateWord('booking_message')}}
                           </div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif
 <h1 class="hidden">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}} | {{ \App\Http\Controllers\HomeController::translateWord('Travel Tours / Hotels Category')}} | SDN.BHD</h1>
        <!-- Home Slider -->
        <div class="home-hero" data-src="{{ASSETS}}/images/Home/{{$homeimage->image_slider}}">
            <!-- Main Header -->
           
    <!-- Home Slider -->  
    
            <div class="container home-slider slider-component" >
                <!-- Item -->
              @if ($tour != null)
                  
              
                    @foreach ($tour as $item)
               
                <div class="item">
                    <div class="row align-center-z">
                        <!-- Content -->
                        <div class="col-12 col-m-7">
                            <a href="{{route('getTour_dById',['tourId'=>$item->tour_id])}}"><h3>{{$item->tours->name}}</h3></a>
                           <p> {{$item->tours->short_description}}</p>
                            <h4>{{ \App\Http\Controllers\HomeController::translateWord('Touer Places')}}</h4>
                            <!-- Gallery Shots -->
                           
                            <div class="row ">
                                <!-- Image -->
                                
                                    
                               
                                @foreach ($item->image as $image )
                                    
                              
                                <a href="{{ASSETS}}/images/tours/{{ $image }}" data-lightbox="gallery-1" class="col-6 col-s-4 col-l-3 light-btn ti-search"><img src="{{ASSETS}}/images/tours/{{ $image }}" alt="title"></a>
                             
                               @endforeach 
                            </div>
                        </div>
                        <!-- Form -->
                        <div class="col-12 col-m-5">
                        <form class="form-ui form-box small rounde-controls" method="post" action="{{route('postgetbooking')}}" enctype="multipart/form-data">
                             {{ csrf_field() }}
                         <input type="hidden" name="tour_id" value="{{$item->tour_id}}">
                         <h3>{{ \App\Http\Controllers\HomeController::translateWord('Request for Quotation Now')}}</h3>
                         <br/>
                          <div class="row">
                                    <!-- Control -->
                        <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}</label>
                        <input type="text" name="fullname"  value="{{old('fullname')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}" required>
                         </div>
                                    <!-- Control -->
                         <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('email')}}</label>
                        <input type="text"  name="email" value="{{old('email')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('email')}}" required>
                        </div>
                                    <!-- Control -->
                    <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('phone')}}</label>
                        <input type="text"  name="phone" value="{{old('phone')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('phone')}}" required>
                          </div>
                                    <!-- Control -->
                                    <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}</label>
                        <div class="quantity-input">
                            <i class="ti-arrow-up-b increase" ></i>
                            <input type="number"  name="Adult_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}" required>
                            <i class="ti-arrow-down-b dicrease"></i>
                        </div>
                         </div>
                                    <!-- Control -->
                                    <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}</label>
                        <div class="quantity-input">
                            <i class="ti-arrow-up-b increase"></i>
                            <input type="number"  name="Childerns_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}" required>
                            <i class="ti-arrow-down-b dicrease"></i>
                        </div>
                         </div>
                                    <!-- Control -->
                                    <div class="col-12 col-s-6">
                        <label>{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}</label>
                        <div class="quantity-input">
                            <i class="ti-arrow-up-b increase"></i>
                            <input type="number"  name="Infant_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}" required>
                            <i class="ti-arrow-down-b dicrease"></i>
                        </div>
                        </div>
                        <div class="col-12" >
                         <label>{{ \App\Http\Controllers\HomeController::translateWord('note')}}</label>
                        <textarea placeholder="{{ \App\Http\Controllers\HomeController::translateWord('note')}}" name="note" required>{{old('note')}}</textarea>
                        <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Submit Quotation')}}" class="btn block-lvl primary">
                    </div>
                     
                    

                    </div>
                            </form>
                        </div>
                    </div>
                </div> 
                <!-- // Item -->
                
           @endforeach
           @endif
        </div>
        </div>
        <!-- // Home Slider -->

       

        <!-- Search Section -->
        <div class="search-section">
            <div class="container">
                <form class="row form-ui small rounde-controls" method="post" action="{{route('postaddindividualbooking')}}" enctype="multipart/form-data">
                             {{ csrf_field() }}
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <input type="text"  name="fullname"  value="{{old('fullname')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}" required>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <input type="text"  name="email"  value="{{old('email')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('email')}}" required>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <input type="text"  name="phone"  value="{{old('phone')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('phone')}}" required>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <input type="number"  name="Travelers_Number"  value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('TravelersNumber')}}" required>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <input type="number"  name="Number_of_Days"  value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('NumberofDays')}}" required>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                      
                        <select data-placeholder="{{ \App\Http\Controllers\HomeController::translateWord('-- Select Destenation --')}}" name="City_id[]" id="placeholder" multiple required>
                        
                                            @foreach($items as $item)                          
                                              <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                          </select>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                           <select data-placeholder="{{ \App\Http\Controllers\HomeController::translateWord('-- Select Hotels --')}}" name="hotel_id[]" multiple required>
                                
    
                                 @foreach($items as $item)     
                                                        
                                 <option value="{{ $item->id }}"  disabled><h1 >{{ $item->name }}</h1> </option>
                                    @foreach($hotels as $hotel) 
                                    @if ($hotel->city_id == $item->id)                
                                  <option value="{{$hotel->id }}">{{ $hotel->name }}</option>
                                 @endif
                                  @endforeach 
                                 @endforeach  
                              </select>
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-s-6 col-l-3">
                        <div class="control-icon ti-event">
                            <input type="date" id="today"  name="Arrival_Date"  value="{{old('Arrival_Date')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('ArrivalDate')}}" required>
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="col-12" style="margin-bottom: 25px;">
                        <textarea placeholder="{{ \App\Http\Controllers\HomeController::translateWord('note')}}" name="note"  required>{{old('note')}}</textarea>
                        <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Ask for your program')}}" class="btn block-lvl primary">
                    </div>
                    
                </form>
            </div>
        </div>

        <!-- Exploore Section -->
        <div class="section exploore-section">
            <div class="container">
                <div class="row">
                    <!-- Content -->
                    <div class="col-12 col-l-7">
                        <!-- Section Title -->

                        <h2 class="section-title wow fadeInUp">
                            @if($exploore !=null)
                           <span>{{$exploore->title}}</span>
                        </h2>
                        <p class="wow fadeInUp">{{$exploore->description}}</p>
                        <ul class="links-list wow">
                        @foreach ($exploore->EXPLOORE as $item )
                            
                      
                            <li><a >{{$item}}</a></li>
                             @endforeach
                        </ul>
                        @endif
                    </div>
                    <!-- Design Vector -->
                    <div class="hidden-m-down col-l-5 design-vector">
                        <span class="car"></span>
                        <span class="earth-plant"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Exploore Section -->

        <!-- Top Tours -->
        <div class="section top-tours" data-src="{{ASSETS}}/images/Home/{{$homeimage->image_tour}}">
            <div class="container">
                <!-- Section Title -->
                
                <h3 class="section-title wow fadeInUp">
                    <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                     <span>{{ \App\Http\Controllers\HomeController::translateWord('AWESOME TOURS')}}</span>
                </h3>
                <!-- Grid -->
                <div class="row tours-slider wow fadeInUp">
                    <!-- Travel Block -->
                    @if($tourview != null)
                    @foreach ($tourview as $view )
                        
                  
                    <div class="travel-block col-12 col-m-6 col-l-4">
                        <div class="content-box">
                            <!-- Image & Title -->
                            <div class="image" data-src="{{ASSETS}}/images/tours/{{ $view->image }}">
                                <a href="{{route('getTour_dById',['tourId'=>$view->id])}}" class="overlay-link"></a>
                             
                               
                                <a href="{{route('getTour_dById',['tourId'=>$view->id])}}"><h3>{{$view->name}}</h3></a>
                        <div class="stars">
                         @for ($x = 0; $x < $view->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $view->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                            </div>
                            <!-- Main Details -->
                            <h4>{{$view->duration_day}} {{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$view->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                            <h2 class="price">{{$view->price}}Rm</h2>
                             <p>{{$view->short_description}}</p>
                            <!-- Other Details -->
                            <div class="action-area">
                               <span class="ti-bed">{{$view->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                                <span class="ti-businessman">{{$view->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                                <span class="ti-person">{{$view->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                                <a href="{{route('getTour_dById',['tourId'=>$view->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                                <a href="{{route('getTour_dById',['tourId'=>$view->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div> 
        
        <!-- // Top Tours -->

        <!-- Call to Action -->
        <div class="cta-section">
            <div class="container">
                    @if($Advertising != null)
                <div class="content col-12 col-m-8 col-l-9">
                    <h2 class="section-title wow fadeInUp">
                        <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                        <span>{{ \App\Http\Controllers\HomeController::translateWord('ADVENTURE TOURS')}}</span>
                    </h2>
                    <p class="wow fadeInUp">{{$Advertising->descrption}}</p>
                </div>
                <a href="{{$Advertising->link}}" class="btn dark large col-m-4 col-l-3 round-corner wow fadeInEnd">{{ \App\Http\Controllers\HomeController::translateWord('ADVENTURE TOURS')}}</a>
            </div>
            @endif
        </div>
        <!-- // Call to Action -->

        <!-- Top Hotels -->
        <div class="section top-hotels" data-src="{{ASSETS}}/images/Home/{{$homeimage->image_hotel}}">
            <div class="container">
                <!-- Section Title -->
                <h2 class="section-title wow fadeInUp">
                    <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                     <span>{{ \App\Http\Controllers\HomeController::translateWord('Best Hotels')}}</span>
                </h2>
                <!-- Grid -->
                <div class="row tours-slider wow fadeInUp">
                    <!-- Hoel Block -->
                     @if($hotelview != null)
                     @foreach ($hotelview as $hotview )
                   <div class="travel-block col-12 col-m-6 col-l-4">
                        <div class="content-box">
                            <!-- Image & Title -->
                            <div class="image" data-src="{{ASSETS}}/images/hotels/{{ $hotview->image }}">
                                <a href="{{route('getHotel_dById',['hotelId'=>$hotview->id])}}" class="overlay-link"></a>
                                
                               
                              
                        <div class="stars">
                         @for ($x = 0; $x < $hotview->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $hotview->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                            </div>
                              <a href="{{route('getHotel_dById',['hotelId'=>$hotview->id])}}"><h4>{{$hotview->name}}</h4></a>
                            <!-- Main Details -->
                         <!--   <h4>{{$hotview->duration_day}} {{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$hotview->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4> -->
                           <!-- <h2 class="price">{{$hotview->price}}$</h2> -->
                            <p> {{$hotview->short_description}}</p>
                            <!-- Other Details -->
                           <!-- <div class="action-area">
                               <span class="ti-bed">{{$hotview->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                                <span class="ti-businessman">{{$hotview->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                                <span class="ti-person">{{$hotview->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                                <a href="{{route('getHotel_dById',['hotelId'=>$hotview->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                                <a href="{{route('getHotel_dById',['hotelId'=>$hotview->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                            </div> -->
                        </div>
                    </div>
                    
                    @endforeach
                   @endif
                   
                </div>
            </div>
        </div>
        
        <!-- // Top Hotels -->

        <!-- Tourist Cities -->
        <div class="section cities-section">
            <div class="container">
                <!-- Section Title -->
                <h2 class="section-title wow fadeInUp">
                    <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                     <span>{{ \App\Http\Controllers\HomeController::translateWord('TOURIST CITIES')}}</span>
                </h2>
                <!-- Grid -->
                <div class="row tours-slider wow fadeInUp">
                    <!-- City Block -->
                       @if($Cities != null)
                    @foreach ($Cities as $City )
                        
                    
                    <div class="col-12 col-m-6 col-l-4 city-block">
                        <div class="content-box">
                            <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}" class="image" data-src="{{ASSETS}}/images/Cities/{{$City->image}}"></a>
                           <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}"><h3>{{$City->name}}</h3></a>
                           <p> {{$City->descrption}}</p>
                           <a href="{{route('citydetails',['CityId'=>$City->id,'countryId'=>$City->country_id])}}" class="read-more">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                        </div>
                    </div>
                    <!-- // City Block -->
                  @endforeach
                  @endif
                </div>
            </div>
        </div>
        <!-- // Tourist Cities -->
   <script>
   document.querySelector("#today").valueAsDate = new Date();
   </script>
           
@endsection