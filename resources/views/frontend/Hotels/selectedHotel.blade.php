@extends('frontend.layouts.defult')
@section('content')
    <!-- Page Head -->
    


        <div class="row media-slider">
                @if($images != null)
                  @foreach ($images->image as $itemimage)
                     <!-- Image -->
                    
                     <a href="{{ASSETS}}/images/hotels/{{$itemimage}}" data-lightbox="gallery-1" class=" light-btn ti-search"><img src="{{ASSETS}}/images/hotels/{{$itemimage}}" alt="title"></a>
                    
                     @endforeach
                 @endif
         </div>
        <div class="primary-details container page-content">
                <!-- Title/Breadcrumb -->
                <div class="head">
                <h1>{{$hotel->name}}</h1>
                    
                </div>
                <!-- Price -->
               <!-- <h4 class="price">{{$hotel->price}}Rm <span>{{ \App\Http\Controllers\HomeController::translateWord('per_person')}}</span></h4> -->
                <!-- Details -->
                <div class="row">
                    <!-- Image -->
                    <div class="col-12 col-l-6">
                        <img src="{{ASSETS}}/images/hotels/{{$hotel->image}}" alt="title" class="cover">
                    </div>
                    <!-- Content -->
                    <div class="col-12 col-l-6">
                        <h3>{{ \App\Http\Controllers\HomeController::translateWord('hotel_description')}}</h3>
                        <p>{!! html_entity_decode($hotel->long_description) !!}</p>
                        <!-- Options Form -->
                       
                    </div>
                </div>
            </div>
           <!-- <div class="tour-features section">
                    <div class="container">
                        <div class="row align-center-x">
                            
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-2.png" alt=""></i>
                                <h3>{{$hotel->availability}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Availability')}}</h4>
                            </div>
                         
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-3.png" alt=""></i>
                                <h3>{{$hotel->adults}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</h4>
                            </div>
                           
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-4.png" alt=""></i>
                                <h3>{{$hotel->childern}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</h4>
                            </div>
                         
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-7.png" alt=""></i>
                                <h3>{{$hotel->capacity}}m²</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Capacity')}}</h4>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="tour-details">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-l-6">
                                    <!-- Section Title -->
                                    <h2 class="section-title"><i>{{ \App\Http\Controllers\HomeController::translateWord('Hotel')}}</i> {{ \App\Http\Controllers\HomeController::translateWord('Amenities')}}</h2>
                                    <!-- Check List -->
                                    <ul class="check-list">
                                            @foreach($hotel->Amenities as $amenity) 
                                    <li class="ti-checkmark">{{$amenity}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-12 col-l-6">
                                    <!-- Section Title -->
                                    <h2 class="section-title"><i>{{ \App\Http\Controllers\HomeController::translateWord('Hotel')}}</i> {{ \App\Http\Controllers\HomeController::translateWord('Services')}}</h2>
                                    <!-- Check List -->
                                    <ul class="check-list">
                                            @foreach($hotel->services as $service) 
                                        <li class="ti-checkmark">{{$service}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="container page-content">
                <div class="row">
                        <!-- Book Form -->
                        <div class="col-12 col-m-3 col-l-4">
                                <form class="form-ui form-box small rounde-controls" method="post" action="{{route('postgetbooking')}}" enctype="multipart/form-data">
                                     {{ csrf_field() }}
                                    <input type="hidden" name="hotel_id" value="{{$hotel->id}}">
                                       @if(Session::has('sucess'))
                         
                          <div class="alert alert-success" >
 
                     {{ \App\Http\Controllers\HomeController::translateWord('booking_message')}}
                           </div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif
                                    <h3>{{ \App\Http\Controllers\HomeController::translateWord('Request for Quotation Now')}}</h3>
                            <label>{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}</label>
                               <input type="text" name="fullname"  value="{{old('fullname')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('email')}}</label>
                               <input type="text"  name="email" value="{{old('email')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('email')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('phone')}}</label>
                               <input type="text"  name="phone" value="{{old('phone')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('phone')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase" ></i>
                                   <input type="number"  name="Adult_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase"></i>
                                   <input type="number"  name="Childerns_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase"></i>
                                   <input type="number"  name="Infant_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <div class="col-12" >
                                <label>{{ \App\Http\Controllers\HomeController::translateWord('note')}}</label>
                               <textarea placeholder="{{ \App\Http\Controllers\HomeController::translateWord('note')}}" name="note"required>{{old('note')}}</textarea>
                                   <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Submit Quotation')}}" class="btn block-lvl primary">
                               </div>
                                       </form>
                        </div>
                        <div class="col-12 col-m-9 col-l-8">
                            <!-- Reviews Summary -->
                            <form class="main-box form-ui"method="post" action="{{route('postAddHotelReview',['hotelId'=>$hotel->id])}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                  @if(Session::has('Review'))
                         
                          <div class="alert alert-success" >
 
                     {{ \App\Http\Controllers\HomeController::translateWord('Review_message')}}
                           </div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif
                                <h3>{{ \App\Http\Controllers\HomeController::translateWord('This Hotel Has 5 Reviews')}}</h3>
                                <label>{{trans('hotel.fullname')}}</label>
                                <input name="fullname"  type="text" class="form-control"  required>
                                <ul class="reviews-rate">
                                    <li><span>{{trans('hotel.Accomodation')}}</span>
                                        <div class="stars rating-stars" id="rating-stars">
                                            <i class="ti-star" data-value="1"></i>
                                            <i class="ti-star" data-value="2"></i>
                                            <i class="ti-star" data-value="3"></i>
                                            <i class="ti-star" data-value="4"></i>
                                            <i class="ti-star" data-value="5"></i>
                                        </div>
                                        <input type="hidden" class="rating-value" name="Accomodation">
                                    </li>
                                    <li><span>{{trans('hotel.Destination')}}</span>
                                        <div class="stars rating-stars">
                                            <i class="ti-star" data-value="1"></i>
                                            <i class="ti-star" data-value="2"></i>
                                            <i class="ti-star" data-value="3"></i>
                                            <i class="ti-star" data-value="4"></i>
                                            <i class="ti-star" data-value="5"></i>
                                        </div>
                                        <input type="hidden" class="rating-value" name="Destination">
                                    </li>
                                    <li><span>{{trans('hotel.Meals')}}</span>
                                        <div class="stars rating-stars">
                                            <i class="ti-star" data-value="1"></i>
                                            <i class="ti-star" data-value="2"></i>
                                            <i class="ti-star" data-value="3"></i>
                                            <i class="ti-star" data-value="4"></i>
                                            <i class="ti-star" data-value="5"></i>
                                        </div>
                                        <input type="hidden" class="rating-value" name="Meals">
                                    </li>
                                    <li><span>{{trans('hotel.Transport')}}</span>
                                        <div class="stars rating-stars">
                                            <i class="ti-star" data-value="1"></i>
                                            <i class="ti-star" data-value="2"></i>
                                            <i class="ti-star" data-value="3"></i>
                                            <i class="ti-star" data-value="4"></i>
                                            <i class="ti-star" data-value="5"></i>
                                        </div>
                                        <input type="hidden" class="rating-value" name="Transport">
                                    </li>
                                    <li><span>{{trans('hotel.Value_For_Money')}}</span>
                                        <div class="stars rating-stars">
                                            <i class="ti-star" data-value="1"></i>
                                            <i class="ti-star" data-value="2"></i>
                                            <i class="ti-star" data-value="3"></i>
                                            <i class="ti-star" data-value="4"></i>
                                            <i class="ti-star" data-value="5"></i>
                                        </div>
                                        <input type="hidden" class="rating-value" name="Value_For_Money">
                                    </li>                             
                                </ul>
                                <label>{{trans('hotel.review')}}</label>
                                <textarea name="review" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('write your review.')}}"required></textarea>
                                <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Add_Review')}}" class="btn primary block-lvl round-corner">
                            </form>
                             
                            @foreach ($Hotel_Reviews as $Hotel_Review)               
                            <div class="review-block">
                                    <img src="{{ASSETS}}/img/avatar.png" alt="" class="image">
                                    <div class="info">
                                        <h3>{{$Hotel_Review->fullname}}</h3>
                                        <h4>{{ \App\Http\Controllers\HomeController::translateWord('Overall')}}
            <?php $i=(($Hotel_Review->Accomodation+$Hotel_Review->Meals+$Hotel_Review->Value_For_Money+$Hotel_Review->Destination+$Hotel_Review->Transport)/5);
                       ?>                    
                                           <div class="stars">
                                                @for ($x = 0; $x < $i ; $x++)                        
                                                <i class="ti-star active"></i>                                                       
                                             @endfor 
                                             @for ($x = 5; $x > $i ; $x--)                       
                                             <i class="ti-star" ></i>
                                             @endfor 
                                            </div>
                                        </h4>
                                        <p>{{$Hotel_Review->review}}</p>
                                    </div>
                                </div>
                                @endforeach
                            </div> 
                        </div> 
                           <h2 class="section-title">
                <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                <span>{{ \App\Http\Controllers\HomeController::translateWord('Similiar Hotels')}}</span>
            </h2>
                        <div class="row tours-slider">
                                @foreach ($Hotel_city as $Hotel_citye) 
                                <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                                    <div class="content-box">
                                        <!-- Image & Title -->
                                        <div class="image" data-src="{{ASSETS}}/images/hotels/{{$Hotel_citye->image}}">
                                            <a href="{{route('getHotel_dById',['tourId'=>$Hotel_citye->id])}}" class="overlay-link"></a>
                                            
                                          
                                            <div class="stars">
                                             @for ($x = 0; $x < $Hotel_citye->category->category ; $x++)                        
                                                <i class="ti-star active"></i>                                                       
                                             @endfor 
                                             @for ($x = 5; $x > $Hotel_citye->category->category ; $x--)                       
                                             <i class="ti-star" ></i>
                                             @endfor 
                                            </div>
                                        </div>
                                        <!-- Main Details -->
                                          <a href="{{route('getHotel_dById',['tourId'=>$Hotel_citye->id])}}"><h4>{{$Hotel_citye->name}}</h4></a>
                                      <!--  <h4>{{$Hotel_citye->duration_day}} {{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$Hotel_citye->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                                        <h2 class="price">{{$Hotel_citye->price}}$</h2> -->
                                        <p>{{$Hotel_citye->short_description}}</p>
                                        <!-- Other Details -->
                                      <!--  <div class="action-area">
                                             <span class="ti-bed">{{$Hotel_citye->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                                                <span class="ti-businessman">{{$Hotel_citye->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                                                <span class="ti-person">{{$Hotel_citye->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                                                <a href="{{route('getHotel_dById',['tourId'=>$Hotel_citye->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                                                <a href="{{route('getHotel_dById',['tourId'=>$Hotel_citye->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                                            </div>   -->   
                                    </div>              
                                </div>
                                @endforeach  
                                    <!-- // Travel Block -->
                                </div>
                            </div>
                        </div>    
                       
        @endsection