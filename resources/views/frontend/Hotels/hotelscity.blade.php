@extends('frontend.layouts.defult')
@section('content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <h1>{{ \App\Http\Controllers\HomeController::translateWord('hotels')}} {{ \App\Http\Controllers\HomeController::translateWord('in')}} {{$cities->name}}</h1>
            <div class="breadcrumb">
                <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>
                
            </div>
        </div>
    </div>

    <!-- // Page Head -->

    <!-- Search Section -->
    <div class="search-section">
        <div class="container">
            <h2>{{ \App\Http\Controllers\HomeController::translateWord('hotels')}} {{ \App\Http\Controllers\HomeController::translateWord('in')}} {{$cities->name}}</h2>
        </div>
    </div>
    <!-- // Search Section -->

    <!-- Page content -->
    <div class="container page-content">
        <!-- Grid -->
      
        <div class="row">
        @if($hotels != null)
            <!-- Travel Block -->
           @foreach($hotels as $hotel)
            <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                <div class="content-box">
                    <!-- Image & Title -->
                    <div class="image" data-src="{{ASSETS}}/images/hotels/{{$hotel->image}}">
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="overlay-link"></a>
                       
                      
                        <div class="stars">
                         @for ($x = 0; $x < $hotel->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $hotel->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                    </div>  
                    <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}"><h4>{{$hotel->name}}</h4></a>
                    <!-- Main Details -->
                  <!--<h4>{{$hotel->duration_day}} {{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$hotel->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                    <h2 class="price">{{$hotel->price}}</h2>-->
                    <p>{{$hotel->short_description}}</p>
                    <!-- Other Details -->
                  <!--  <div class="action-area">
                             <span class="ti-bed">{{$hotel->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                            <span class="ti-businessman">{{$hotel->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                            <span class="ti-person">{{$hotel->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>                        
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                    </div>  -->     
                </div>              
            </div>
            @endforeach  
            @endif
        </div>    
        <!-- Pagination -->
      
       
    </div>

 
@endsection