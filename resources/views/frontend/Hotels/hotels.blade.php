@extends('frontend.layouts.defult')
@section('content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <h1>{{ \App\Http\Controllers\HomeController::translateWord('Hotels')}}</h1>
            <div class="breadcrumb">
                <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>
                <a href="{{route('gethotels')}}">{{ \App\Http\Controllers\HomeController::translateWord('hotels')}}</a>
            </div>
        </div>
    </div>

    <!-- // Page Head -->

    <!-- Search Section -->
    <div class="search-section">
        <div class="container">
            <form  class="row form-ui small rounde-controls"method="post" action="{{route('getHotel')}}" enctype="multipart/form-data">
                <!-- Control -->
                {{ csrf_field() }}
                <div class="col-12 col-m-6 col-l-4">
                    <label>{{trans('hotel.Destination')}}</label>       
                    <select name="city_id"  id="city_id" class="form-control">
                        <option>{{ \App\Http\Controllers\HomeController::translateWord('-- Select Option --')}}</option>
                        @foreach($cities as $city)                          
                          <option value="{{ $city->id }}"{{ $selectedcity == $city->id  ? 'selected="selected"' : '' }}>{{ $city->name }}</option>
                        @endforeach
                      </select>
                </div>              
                <!-- Control -->
                <div class="col-12 col-m-6 col-l-4">
                    <label>{{trans('hotel.category')}}</label>
                    <select name="category_id"  id="category_id" class="form-control">
                        <option>{{ \App\Http\Controllers\HomeController::translateWord('-- Select Option --')}}</option>
                        @foreach($categories as $category)                          
                          <option value="{{ $category->id }}" {{ $selectedcategory == $category->id  ? 'selected="selected"' : '' }}>{{ $category->category }} {{ \App\Http\Controllers\HomeController::translateWord('Stars')}}</option>
                        @endforeach
                      </select>
                </div>
                <!-- Control -->
                <div class="col-12 col-m-6 col-l-4">
                    <label class="fake-m-label">Submit Hidden Text</label>            
                            <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Search Now')}}" class="btn primary block-lvl">
                      
                    </a>
                    
                </div>
            </form>
        </div>
    </div>
    <!-- // Search Section -->

    <!-- Page content -->
    <div class="container page-content">
        <!-- Grid -->
      
        <div class="row">
        
            <!-- Travel Block -->
            @foreach($hotels as $hotel)
            <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                <div class="content-box">
                    <!-- Image & Title -->
                    <div class="image" data-src="{{ASSETS}}/images/hotels/{{$hotel->image}}">
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="overlay-link"></a>
                       
                      
                        <div class="stars">
                         @for ($x = 0; $x < $hotel->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $hotel->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                    </div>  
                    <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}"><h4>{{$hotel->name}}</h4></a>
                    <!-- Main Details -->
                  <!--<h4>{{$hotel->duration_day}} {{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$hotel->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                    <h2 class="price">{{$hotel->price}}</h2>-->
                    <p>{{$hotel->short_description}}</p>
                    <!-- Other Details -->
                  <!--  <div class="action-area">
                             <span class="ti-bed">{{$hotel->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                            <span class="ti-businessman">{{$hotel->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                            <span class="ti-person">{{$hotel->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>                        
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                        <a href="{{route('getHotel_dById',['hotelId'=>$hotel->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                    </div>  -->     
                </div>              
            </div>
            @endforeach  
        </div>    
        <!-- Pagination -->
        {{$link->links()}}
       
    </div>

 
@endsection