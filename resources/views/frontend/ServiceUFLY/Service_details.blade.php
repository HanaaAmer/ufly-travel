@extends('frontend.layouts.defult')
@section('content')
@if($Services != null)
@foreach ($Services as $Service)
    <div class="page-head">
            <div class="container">
                   <h1>{{$Service->name}} </h1>
                <div class="breadcrumb">
                     <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('Services')}}">{{ \App\Http\Controllers\HomeController::translateWord('services')}}</a>
                   <a >{{$Service->name}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- City Details -->
       
        <div class="city-details container">
           
            <img  src="{{ASSETS}}/images/services/{{$Service->image}}" alt="" class="block-lvl cover" stly="height:10%" >
            <h2>{{$Service->title}}</h2>
            {!! html_entity_decode($Service->long_descrption) !!}
            
            
            
            <!-- Gallery Shots -->
            <div class="row media-slider">
           @if($images != null)
             @foreach ($images->image as $itemimage)
                <!-- Image -->
                <a href="{{ASSETS}}/images/services/{{$itemimage}}" data-lightbox="gallery-1" class="col-12 col-m-6 col-l-3 light-btn ti-search"><img src="{{ASSETS}}/images/services/{{$itemimage}}" alt="title"></a>
                @endforeach
            @endif
            </div>
          
        </div>
        <!-- // City Details -->
 @if($Service->image_services != null)
        <!-- More Details -->
        <div class="tour-details">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-l-6">
                        <img src="{{ASSETS}}/images/services/{{$Service->image_services}}" alt="" class="block-lvl">
                    </div>
                    <div class="col-12 col-l-6">
                        <!-- Section Title -->
                        <h2 class="section-title">  <span>{{ \App\Http\Controllers\HomeController::translateWord('services')}}</span></h2>
                        <!-- Check List -->
                        <ul class="check-list">
                        @for ($i = 0; $i < count($Service->services); $i++ )
                            <li class="ti-checkmark-bold">{{$Service->services[$i]}}</li>
                              @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <!-- // More Details -->
 @endforeach
       
@endif
@endsection