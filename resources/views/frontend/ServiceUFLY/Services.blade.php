@extends('frontend.layouts.defult')
@section('content')
    <div class="page-head">
            <div class="container">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('services')}}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('Services')}}">{{ \App\Http\Controllers\HomeController::translateWord('services')}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Page content -->
        <div class="container page-content">
            <!-- Grid -->
            @if($Services != null)
            <div class="row">
                <!-- City Block -->
                @foreach ($Services as $Service)
                    
                
                <div class="col-12 col-m-6 col-l-4 city-block">
                    <div class="content-box">
                        <a href="{{route('Servicesdetails',['ServiceId'=>$Service->id])}}" class="image" data-src="{{ASSETS}}/images/services/{{$Service->image}}"></a>
                        <a href="{{route('Servicesdetails',['ServiceId'=>$Service->id])}}"><h3>{{$Service->name}}</h3></a>
                       <p> {{$Service->descrption}}</p>
                        <a href="{{route('Servicesdetails',['ServiceId'=>$Service->id])}}" class="read-more">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                    </div>
                </div>
               @endforeach
            </div>
            <!-- Pagination -->
            {{ $Services->links() }}
            @endif
        </div>
@endsection