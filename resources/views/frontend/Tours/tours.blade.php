@extends('frontend.layouts.defult')
@section('content')

    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <h1>{{ \App\Http\Controllers\HomeController::translateWord('TRAVEL TOURS')}}</h1>
            <div class="breadcrumb">
                <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>
                <a href="{{route('gettours')}}">{{ \App\Http\Controllers\HomeController::translateWord('TRAVEL TOURS')}}</a>
            </div>
        </div>
    </div>

    <!-- // Page Head -->

    <!-- Search Section -->
    <div class="search-section">
        <div class="container">
            <form  class="row form-ui small rounde-controls"method="post" action="{{route('getTour')}}" enctype="multipart/form-data">
                <!-- Control -->
                {{ csrf_field() }}
                <div class="col-12 col-m-6 col-l-4">
                    <label>{{trans('tour.Destination')}}</label>       
                    <select name="city_id"  id="city_id" class="form-control">
                        <option>{{ \App\Http\Controllers\HomeController::translateWord('-- Select Option --')}}</option>
                        @foreach($cities as $city)                          
                          <option value="{{ $city->id }}"{{ $selectedcity == $city->id  ? 'selected="selected"' : '' }}>{{ $city->name }}</option>
                        @endforeach
                      </select>
                </div>   
               
                <!-- Control -->
                <div class="col-12 col-m-6 col-l-4">
                    <label>{{trans('tour.category')}}</label>
                    <select name="category_id"  id="category_id" class="form-control">
                        <option>{{ \App\Http\Controllers\HomeController::translateWord('-- Select Option --')}}</option>
                        @foreach($categories as $category)                          
                          <option value="{{ $category->id }}" {{ $selectedcategory == $category->id  ? 'selected="selected"' : '' }}>{{ $category->category }} {{ \App\Http\Controllers\HomeController::translateWord('Stars')}}</option>
                        @endforeach
                      </select>
                </div>
            
                <!-- Control -->
                <div class="col-12 col-m-6 col-l-4">
                    <label class="fake-m-label">Submit Hidden Text</label>            
                            <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Search Now')}}" class="btn primary block-lvl">
                      
                    </a>
                    
                </div>
            </form>
        </div>
    </div>
    <!-- // Search Section -->

    <!-- Page content -->
    <div class="container page-content">
        <!-- Grid -->
      
        <div class="row">
        
            <!-- Travel Block -->
            @foreach($tours as $tour)
            <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                <div class="content-box">
                    <!-- Image & Title -->
                    <div class="image" data-src="{{ASSETS}}/images/tours/{{$tour->image}}">
                        <a href="{{route('getTour_dById',['tourId'=>$tour->id])}}" class="overlay-link"></a>
                      
                        <a href="{{route('getTour_dById',['tourId'=>$tour->id])}}"><h3>{{$tour->name}}</h3></a>
                        <div class="stars">
                         @for ($x = 0; $x < $tour->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $tour->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                    </div>
                    <!-- Main Details -->
                    <h4>{{$tour->duration_day}}{{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$tour->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                       <h2 class="price">{{$tour->price}}Rm</h2>
                    <p>{{$tour->short_description}}</p>
                    <!-- Other Details -->
                    <div class="action-area">
                        <span class="ti-bed">{{$tour->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                            <span class="ti-businessman">{{$tour->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                            <span class="ti-person">{{$tour->childern}}{{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                            <a href="{{route('getTour_dById',['tourId'=>$tour->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                            <a href="{{route('getTour_dById',['tourId'=>$tour->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                        </div>      
                </div>              
            </div>
            @endforeach  
        </div>     
        <!-- Pagination -->

        {{$link->links()}}
       
    </div>
  <script>
    document.querySelector("#start_date").valueAsDate = new Date();
          document.querySelector("#end_date").valueAsDate = new Date();
alert(("#end_date").valueAsDate);
       
           </script>
 
@endsection