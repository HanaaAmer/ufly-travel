@extends('frontend.layouts.defult')
@section('content')
    <!-- Page Head -->
    
  
    <div class="row media-slider">
            @if($images != null)
              @foreach ($images->image as $itemimage)
                 <!-- Image -->
                 
                 <a href="{{ASSETS}}/images/tours/{{$itemimage}}" data-lightbox="gallery-1" class=" light-btn ti-search"><img src="{{ASSETS}}/images/tours/{{$itemimage}}" alt="title"></a>
                 
                 @endforeach
             @endif
             </div>
        <div class="primary-details container page-content">
                <!-- Title/Breadcrumb -->
                <div class="head">
                <h1>{{$tour->name}}</h1>
                    
                </div>
                <!-- Price -->
                <h4 class="price">{{$tour->price}} <span>{{ \App\Http\Controllers\HomeController::translateWord('per_person')}}</span></h4>
                <!-- Details -->
                <div class="row">
                    <!-- Image -->
                    <div class="col-12 col-l-6">
                        <img src="{{ASSETS}}/images/tours/{{$tour->image}}" alt="title" class="cover">
                    </div>
                    <!-- Content -->
                    <div class="col-12 col-l-6">
                        <!--<h3>{{ \App\Http\Controllers\HomeController::translateWord('The Tour Description')}}</h3>-->
                        <!--<p>{!! html_entity_decode($tour->long_description) !!}</p>-->
                        <!-- Options Form -->
                        <!-- Section Title -->
                        <h2 class="section-title">
                            <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                            <span>{{ \App\Http\Controllers\HomeController::translateWord('Other Notes')}}</span>
                        </h2>
                        <!-- Information List -->
                        <ul class="info-list"> 
                        <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('City Name')}}:</strong>
                                <div class="inclouded-info">
                                      @foreach($tourforgin as $forgin) 
                                    <span class="ti-checkmark">{{$forgin->Cities->name}}</span>
                                   @endforeach
                                </div>
                            </li>
                            <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Hotel Name')}}:</strong>
                                <div class="inclouded-info">
                                      @foreach($tourforgin as $forgin2) 
                                    <span class="ti-checkmark">{{$forgin2->Hotel->name}}</span>
                                   @endforeach
                                </div>
                            </li>
                            <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Start Date')}}:</strong>{{$tour->start_date}}</li>
                            <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('End Date')}}:</strong>{{$tour->end_date}}</li>
                            <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Included')}}:</strong>
                                <div class="inclouded-info">
                                      @foreach($tour->included as $include) 
                                    <span class="ti-checkmark">{{$include}}</span>
                                   @endforeach
                                </div>
                            </li>
                            <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Not Included')}}:</strong>
                                <div class="inclouded-info">
                                        @foreach($tour->not_included as $not_include) 
                                    <span class="ti-close">{{$not_include}}</span>
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                       
                    </div>
                </div>
            </div>
            <!--<div class="tour-details">-->
            <!--    <div class="container">-->
            <!--        <div class="row">-->
            <!--            <div class="col-12 col-l-6">-->
                            <!-- Section Title -->
            <!--                <h2 class="section-title">-->
            <!--                    <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>-->
            <!--                    <span>{{ \App\Http\Controllers\HomeController::translateWord('Other Notes')}}</span>-->
            <!--                </h2>-->
                            <!-- Information List -->
            <!--                <ul class="info-list"> -->
            <!--                <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('City Name')}}:</strong>-->
            <!--                        <div class="inclouded-info">-->
            <!--                              @foreach($tourforgin as $forgin) -->
            <!--                            <span class="ti-checkmark">{{$forgin->Cities->name}}</span>-->
            <!--                           @endforeach-->
            <!--                        </div>-->
            <!--                    </li>-->
            <!--                    <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Hotel Name')}}:</strong>-->
            <!--                        <div class="inclouded-info">-->
            <!--                              @foreach($tourforgin as $forgin2) -->
            <!--                            <span class="ti-checkmark">{{$forgin2->Hotel->name}}</span>-->
            <!--                           @endforeach-->
            <!--                        </div>-->
            <!--                    </li>-->
            <!--                    <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Start Date')}}:</strong>{{$tour->start_date}}</li>-->
            <!--                    <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('End Date')}}:</strong>{{$tour->end_date}}</li>-->
            <!--                    <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Included')}}:</strong>-->
            <!--                        <div class="inclouded-info">-->
            <!--                              @foreach($tour->included as $include) -->
            <!--                            <span class="ti-checkmark">{{$include}}</span>-->
            <!--                           @endforeach-->
            <!--                        </div>-->
            <!--                    </li>-->
            <!--                    <li><strong>{{ \App\Http\Controllers\HomeController::translateWord('Not Included')}}:</strong>-->
            <!--                        <div class="inclouded-info">-->
            <!--                                @foreach($tour->not_included as $not_include) -->
            <!--                            <span class="ti-close">{{$not_include}}</span>-->
            <!--                            @endforeach-->
            <!--                        </div>-->
            <!--                    </li>-->
            <!--                </ul>-->
            <!--            </div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <div class="tour-features section">
                    <div class="container">
                        <div class="row align-center-x">
                           
                            <!-- Feature Block -->
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-2.png" alt=""></i>
                                <h3>{{$tour->availability}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Availability')}}</h4>
                            </div>
                            <!-- Feature Block -->
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-3.png" alt=""></i>
                                <h3>{{$tour->adults}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</h4>
                            </div>
                            <!-- Feature Block -->
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-4.png" alt=""></i>
                                <h3>{{$tour->childern}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</h4>
                            </div>
                            <!-- Feature Block -->
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-5.png" alt=""></i>
                                <h3>{{$tour->duration_day}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Days')}}</h4>
                            </div>
                            <!-- Feature Block -->
                            <div class="feature-block col-6 col-m-4 col-l-2">
                                <i><img src="{{ASSETS}}/img/icon-6.png" alt=""></i>
                                <h3>{{$tour->duration_night}}</h3>
                                <h4>{{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                            </div>
                        </div>
                    </div>
            </div>
                <div class="container page-content">
                     <h2 class="section-title">
                                    
                                    <span>{{ \App\Http\Controllers\HomeController::translateWord('Details')}}</span>
                                </h2>
                        <!-- Program Block -->
                        @foreach($Tour_Details as $Tour_Detail) 
                        <div class="program-block">
                            <div class="row">
                                <div class="col-12 col-m-4 col-l-5"> <img src="{{ASSETS}}/images/tours/{{$Tour_Detail->image}}" alt="" class="block-lvl"> </div>
                                <div class="col-12 col-m-8 col-l-7 align-self-center">
                                    <h3>{{$Tour_Detail->title}}</h3>
                                     {!! html_entity_decode($Tour_Detail->description) !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                
                <div class="row">
                    <!-- Book Form -->
                    <div class="col-12 col-m-3 col-l-4">
                            <form class="form-ui form-box small rounde-controls" method="post" action="{{route('postgetbooking')}}" enctype="multipart/form-data">
                                 {{ csrf_field() }}
                                 
                                <input type="hidden" name="tour_id" value="{{$tour->id}}">
                                   @if(Session::has('sucess'))
                         
                          <div class="alert alert-success" >
 
                     {{ \App\Http\Controllers\HomeController::translateWord('booking_message')}}
                           </div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif
                                <h3>{{ \App\Http\Controllers\HomeController::translateWord('Request for Quotation Now')}}</h3>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}</label>
                               <input type="text" name="fullname"  value="{{old('fullname')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('fullname')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('email')}}</label>
                               <input type="text"  name="email" value="{{old('email')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('email')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('phone')}}</label>
                               <input type="text"  name="phone" value="{{old('phone')}}" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('phone')}}"required>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase" ></i>
                                   <input type="number"  name="Adult_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('AdultNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase"></i>
                                   <input type="number"  name="Childerns_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('ChildernsNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <label>{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}</label>
                               <div class="quantity-input">
                                   <i class="ti-arrow-up-b increase"></i>
                                   <input type="number"  name="Infant_Number" value="" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('InfantNumber')}}" required>
                                   <i class="ti-arrow-down-b dicrease"></i>
                               </div>
                               <div class="col-12" >
                                <label>{{ \App\Http\Controllers\HomeController::translateWord('note')}}</label>
                               <textarea placeholder="{{ \App\Http\Controllers\HomeController::translateWord('note')}}" name="note"required>{{old('note')}}</textarea>
                               <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Submit Quotation')}}" class="btn block-lvl primary">
                           </div>
                                   </form>
                    </div>
                    <div class="col-12 col-m-9 col-l-8">
                        <!-- Reviews Summary -->
                        <form class="main-box form-ui"method="post" action="{{route('postAddTourReview',['tourId'=>$tour->id])}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                @if(Session::has('Review'))
                         
                          <div class="alert alert-success" >
 
                     {{ \App\Http\Controllers\HomeController::translateWord('Review_message')}}
                           </div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif
                            <h3>{{ \App\Http\Controllers\HomeController::translateWord('This Tour Has 5 Reviews')}}</h3>
                            <label>{{trans('hotel.fullname')}}</label>
                            <input name="fullname"  type="text" class="form-control"  required>
                            <ul class="reviews-rate">
                                <li><span>{{trans('hotel.Accomodation')}}</span>
                                    <div class="stars rating-stars" id="rating-stars" required>
                                        <i class="ti-star" data-value="1"></i>
                                        <i class="ti-star" data-value="2"></i>
                                        <i class="ti-star" data-value="3"></i>
                                        <i class="ti-star" data-value="4"></i>
                                        <i class="ti-star" data-value="5"></i>
                                    </div>
                                    <input type="hidden" class="rating-value" name="Accomodation">
                                </li>
                                <li><span>{{trans('hotel.Destination')}}</span>
                                    <div class="stars rating-stars" required>
                                        <i class="ti-star" data-value="1"></i>
                                        <i class="ti-star" data-value="2"></i>
                                        <i class="ti-star" data-value="3"></i>
                                        <i class="ti-star" data-value="4"></i>
                                        <i class="ti-star" data-value="5"></i>
                                    </div>
                                    <input type="hidden" class="rating-value" name="Destination">
                                </li>
                                <li><span>{{trans('hotel.Meals')}}</span>
                                    <div class="stars rating-stars">
                                        <i class="ti-star" data-value="1"></i>
                                        <i class="ti-star" data-value="2"></i>
                                        <i class="ti-star" data-value="3"></i>
                                        <i class="ti-star" data-value="4"></i>
                                        <i class="ti-star" data-value="5"></i>
                                    </div>
                                    <input type="hidden" class="rating-value" name="Meals">
                                </li>
                                <li><span>{{trans('hotel.Transport')}}</span>
                                    <div class="stars rating-stars">
                                        <i class="ti-star" data-value="1"></i>
                                        <i class="ti-star" data-value="2"></i>
                                        <i class="ti-star" data-value="3"></i>
                                        <i class="ti-star" data-value="4"></i>
                                        <i class="ti-star" data-value="5"></i>
                                    </div>
                                    <input type="hidden" class="rating-value" name="Transport">
                                </li>
                                <li><span>{{trans('hotel.Value_For_Money')}}</span>
                                    <div class="stars rating-stars">
                                        <i class="ti-star" data-value="1"></i>
                                        <i class="ti-star" data-value="2"></i>
                                        <i class="ti-star" data-value="3"></i>
                                        <i class="ti-star" data-value="4"></i>
                                        <i class="ti-star" data-value="5"></i>
                                    </div>
                                    <input type="hidden" class="rating-value" name="Value_For_Money">
                                </li>                             
                            </ul>
                            <label>{{trans('hotel.review')}}</label>
                            <textarea name="review" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('write your review.')}}"required></textarea>
                            <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('Add_Review')}} " class="btn primary block-lvl round-corner">
                        </form>
                   
                @foreach ($Tour_Reviews as $Tour_Review)               
                <div class="review-block">
                        <img src="{{ASSETS}}/img/avatar.png" alt="" class="image">
                        <div class="info">
                            <h3>{{$Tour_Review->fullname}}</h3>
                            <h4>{{ \App\Http\Controllers\HomeController::translateWord('Overall')}}
<?php $i=(($Tour_Review->Accomodation+$Tour_Review->Meals+$Tour_Review->Value_For_Money+$Tour_Review->Destination+$Tour_Review->Transport)/5);
           ?>                    
                               <div class="stars">
                                    @for ($x = 0; $x < $i ; $x++)                        
                                    <i class="ti-star active"></i>                                                       
                                 @endfor 
                                 @for ($x = 5; $x > $i ; $x--)                       
                                 <i class="ti-star" ></i>
                                 @endfor 
                                </div>
                            </h4>
                            <p>{{$Tour_Review->review}}</p>
                        </div>
                    </div>
                    @endforeach
                </div> 
            </div> 
            <h2 class="section-title">
                <i>{{ \App\Http\Controllers\HomeController::translateWord('Ready For More')}}</i>
                <span>{{ \App\Http\Controllers\HomeController::translateWord('Similiar Tours')}}</span>
            </h2>
            <div class="row tours-slider">
            @foreach ($Tour_city as $Tour_citye) 
            <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                <div class="content-box">
                    <!-- Image & Title -->
                    <div class="image" data-src="{{ASSETS}}/images/tours/{{$Tour_citye->image}}">
                        <a href="{{route('getTour_dById',['tourId'=>$Tour_citye->id])}}" class="overlay-link"></a>
                        <span class="floating-badge">Top 10</span>
                        <a href="{{route('getTour_dById',['tourId'=>$Tour_citye->id])}}"><h3>{{$Tour_citye->name}}</h3></a>
                        <div class="stars">
                         @for ($x = 0; $x < $Tour_citye->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $Tour_citye->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                    </div>
                    <!-- Main Details -->
                    <h4>{{$Tour_citye->duration_day}}{{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$Tour_citye->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                      <h2 class="price">{{$tour->price}}Rm</h2>
                    <p>{{$Tour_citye->short_description}}</p>
                    <!-- Other Details -->
                    <div class="action-area">
                        <span class="ti-bed">{{$Tour_citye->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                            <span class="ti-businessman">{{$Tour_citye->adults}}{{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                            <span class="ti-person">{{$Tour_citye->childern}} {{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                            <a href="{{route('getTour_dById',['tourId'=>$Tour_citye->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                            <a href="{{route('getTour_dById',['tourId'=>$Tour_citye->id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                        </div>      
                </div>              
            </div>
            @endforeach  
                <!-- // Travel Block -->
            </div>
        </div>
    </div>  
        </div>          
                       
        @endsection