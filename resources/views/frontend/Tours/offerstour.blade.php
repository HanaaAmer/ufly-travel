@extends('frontend.layouts.defult')
@section('content')
{!! \App\Entities\Tracker::hit() !!}
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <h1>{{ \App\Http\Controllers\HomeController::translateWord('offer')}} {{$offer->offer}}</h1>
            <div class="breadcrumb">
               <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>
                
            </div>
        </div>
    </div>

    <!-- // Page Head -->

    <!-- Search Section -->
    <div class="search-section">
        <div class="container">
               <h2>{{ \App\Http\Controllers\HomeController::translateWord('offer')}} {{$offer->offer}}</h2>
        </div>
    </div>
    <!-- // Search Section -->

    <!-- Page content -->
    <div class="container page-content">
        <!-- Grid -->
      
        <div class="row">
        
            <!-- Travel Block -->
            @foreach($tours as $tour)
            <div class="travel-block wow col-12 col-m-6 col-l-4">                     
                <div class="content-box">
                    <!-- Image & Title -->
                    <div class="image" data-src="{{ASSETS}}/images/tours/{{$tour->Tour->image}}">
                        <a href="{{route('getTour_dById',['tourId'=>$tour->tour_id])}}" class="overlay-link"></a>
                        <span class="floating-badge">Top 10</span>
                        <a href="{{route('getTour_dById',['tourId'=>$tour->tour_id])}}"><h3>{{$tour->Tour->name}}</h3></a>
                        <div class="stars">
                         @for ($x = 0; $x < $tour->Tour->category->category ; $x++)                        
                            <i class="ti-star active"></i>                                                       
                         @endfor 
                         @for ($x = 5; $x > $tour->Tour->category->category ; $x--)                       
                         <i class="ti-star" ></i>
                         @endfor 
                        </div>
                    </div>
                    <!-- Main Details -->
                    <h4>{{$tour->Tour->duration_day}}{{ \App\Http\Controllers\HomeController::translateWord('Days')}} {{$tour->Tour->duration_night}} {{ \App\Http\Controllers\HomeController::translateWord('Nights')}}</h4>
                      <h2 class="price">{{$tour->Tour->price}}Rm</h2>
                    <p>{{$tour->Tour->short_description}}</p>
                    <!-- Other Details -->
                    <div class="action-area">
                        <span class="ti-bed">{{$tour->Tour->beds}} {{ \App\Http\Controllers\HomeController::translateWord('bed')}}</span>
                            <span class="ti-businessman">{{$tour->Tour->adults}} {{ \App\Http\Controllers\HomeController::translateWord('Adults')}}</span>
                            <span class="ti-person">{{$tour->Tour->childern}}{{ \App\Http\Controllers\HomeController::translateWord('Childerns')}}</span>
                            <a href="{{route('getTour_dById',['tourId'=>$tour->tour_id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Read More')}}</a>
                            <a href="{{route('getTour_dById',['tourId'=>$tour->tour_id])}}" class="btn">{{ \App\Http\Controllers\HomeController::translateWord('Booking Now')}}</a>
                        </div>      
                </div>              
            </div>
            @endforeach  
        </div>     
    

       
       
    </div>
  <script>
    document.querySelector("#start_date").valueAsDate = new Date();
          document.querySelector("#end_date").valueAsDate = new Date();
alert(("#end_date").valueAsDate);
       
           </script>
 
@endsection