@extends('frontend.layouts.defult')
@section('content')
    
        <!-- Page Head -->
        <div class="page-head">
            <div class="container">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('contactus')}}</h1>
                <div class="breadcrumb">
                   <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a href="{{route('ContactUS')}}">{{ \App\Http\Controllers\HomeController::translateWord('contactus')}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Page Content -->
        <div class="container page-content" >
            <div class="row">
                <div class="col-12 col-m-8 col-l-9">
                    <!-- Message Form -->
                    <form class="form-ui row order-form" method="post" action="{{route('addmessage')}}" enctype="multipart/form-data">
                     {{ csrf_field() }}
                        <!-- Head -->
                        <div class="col-12">
                            <h3>{{ \App\Http\Controllers\HomeController::translateWord('Sendusamessage')}}</h3>
                            <p>{{$Contact->contactus_description}}</p>
                        </div>

                        <!-- Form Control -->
                          <div class="col-12 col-m-6">
                            <input type="text" name="firstname" value="{{old('firstname')}}"  placeholder="{{ \App\Http\Controllers\HomeController::translateWord('firstname')}}" required>
                        </div>

                        <!-- Form Control -->
                        <div class="col-12 col-m-6">
                            <input type="text" name="lastname" value="{{old('lastname')}}"  placeholder="{{ \App\Http\Controllers\HomeController::translateWord('lastname')}}" required>
                        </div>


                        <!-- Form Control -->
                        <div class="col-12 col-m-6">
                            <input type="text" name="phone" value="{{old('phone')}}"  placeholder="{{ \App\Http\Controllers\HomeController::translateWord('phone')}}" required>
                        </div>

                        <!-- Form Control -->
                        <div class="col-12 col-m-6">
                            <input type="text" name="email" value="{{old('email')}}"  placeholder="{{ \App\Http\Controllers\HomeController::translateWord('email')}}" required>
                        </div>
                        
                        <!-- Form Controls -->
                        <div class="col-12">
                            <textarea placeholder="{{ \App\Http\Controllers\HomeController::translateWord('contact_message')}}" name="message" value="{{old('message')}}" required></textarea>
                            
                           
</div>
<div class="col-12 col-m-6">
                     <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('SendMessage')}}" class="btn primary">
</div>
                           @if(Session::has('addContactUS'))
                           <div class="col-12 col-m-6">
                          <div class="alert alert-success" style="width:50%">
 
                     {{ \App\Http\Controllers\HomeController::translateWord('booking_message')}}
                           </div></div>
                           <script>
                     $(document).ready(function(){ 
                     $(".alert-success").fadeTo(2000, 2000).slideUp(2000, function(){
                       $(".alert-success").slideUp(2000);
                                   });
                                     });
                            </script> @endif


                    </form>  
                
                    <!-- // Message Form -->
                </div>
                
                <div class="col-12 col-m-4 col-l-3 contact-info">
                        @if($Contact != null)
                 <h3>{{ \App\Http\Controllers\HomeController::translateWord('CONTACTINFO')}}</h3></h3>
                    <p>{{$Contact->description}}</p>
                    <!-- Contact Block -->
                    <div class="contact-block ti-phone-classic"><i class="fa fa-phone">{{ $Contact->phone}}</i></div>
                    <!-- Contact Block -->
                    <div class="contact-block ti-mail">{{ $Contact->email}}</div>
                    <!-- Contact Block -->
                    <div class="contact-block ti-fax">{{ $Contact->fax}}</div>
                    @endif
                </div>
            </div>
        </div>
        <!-- // Page Content -->

        <!-- Map -->
        <div class="responsive-map">
            <div id="map" style="width: 100%;height:700px "></div>  
        </div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnEX6zLgaPJTW1Eb7rJJoADHvtNzee8hs"></script>
<script type="text/javascript">
function initialize() {
   var latlng = new google.maps.LatLng(<?=$Contact->map_lat?>, <?=$Contact->map_lng?>);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 10
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: false,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var infowindow = new google.maps.InfoWindow();   
    google.maps.event.addListener(marker, 'click', function() {
      var iwContent = '<div id="iw_container">' +
      '<div class="iw_title"><b>Location</b> : Noida</div></div>';
      // including content to the infowindow
      infowindow.setContent(iwContent);
      // opening the infowindow in the current map and at the current marker location
      infowindow.open(map, marker);
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection

 