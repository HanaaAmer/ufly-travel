@extends('frontend.layouts.defult')
@section('content')

        <!-- Page Head -->
        <div class="page-head">
            <div class="container">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('about_us')}}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	


                    <a href="{{route('AboutUs')}}">{{ \App\Http\Controllers\HomeController::translateWord('about_us')}}</a>

                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Details -->
        @if($about != null)
        <div class="city-details container">
            <img src="{{ASSETS}}/images/About/{{$about->image_description}}" alt="" class="block-lvl cover">
            <h2>{{$about->title}}</h2>
            <p>{!! html_entity_decode($about->description)!!}</p>
            
        </div>
        <!-- // Details -->

        <!-- Vision -->
        <div class="tour-details">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-l-6">
                        <img src="{{ASSETS}}/images/About/{{$about->image_vision}}" alt="" class="block-lvl">
                    </div>
                    <div class="col-12 col-l-6">
                        <!-- Section Title -->
                        <h2 class="section-title"><i>{{ \App\Http\Controllers\HomeController::translateWord('Our')}}</i> <span>{{ \App\Http\Controllers\HomeController::translateWord('VISIONandMISSION')}}</span></h2>
                        <p>{!! html_entity_decode($about->vision)!!}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Vision -->

        <!-- Goals -->
        <div class="tour-details">
            <div class="container">
                <div class="row row-reverse">
                    <div class="col-12 col-l-6">
                        <img src="{{ASSETS}}/images/About/{{$about->image_goals}}" alt="" class="block-lvl">
                    </div>
                    <div class="col-12 col-l-6">
                        <!-- Section Title -->
                        <h2 class="section-title"><i>{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</i><span>{{ \App\Http\Controllers\HomeController::translateWord('Goals')}}</span> </h2>
                        <!-- Check List -->
                        <ul class="check-list">
                          @for ($i = 0; $i < count($about->about_goals); $i++ )
                            <li class="ti-checkmark-bold">{{$about->about_goals[$i]}}</li>
                             @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </div>
  
    <!-- Gallery Shots -->
    @if ($aboutusimage != null)
        
   
    <div class="container">
    <h2 class="section-title"> <span>{{ \App\Http\Controllers\HomeController::translateWord('Company documents')}}</span></h2>
       <div class="row">
       
           <!-- Image -->
           @foreach ($aboutusimage->image as $image)
               
          
           <div class="box-5x1"><a href="{{ASSETS}}/images/About/{{$image}}" data-lightbox="gallery-1" class="light-btn ti-search"><img src="{{ASSETS}}/images/About/{{$image}}" alt="title"></a></div>
           @endforeach
       </div>
        </div>
        @endif  
        @endif
@endsection