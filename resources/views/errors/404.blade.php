 @extends('frontend.layouts.defult')
@section('content')
  <!-- Page Head -->
        <div class="page-head">
            <div class="container">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('Error 404')}}</h1>
                <div class="breadcrumb">
                     <a href="{{ route('gethome') }}">{{ \App\Http\Controllers\HomeController::translateWord('U.Flay')}}</a>	
                    <a >{{ \App\Http\Controllers\HomeController::translateWord('Error 404')}}</a>
                </div>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Page Content -->
        <div class="container page-content">
            <div class="row align-center-z row-reverse error-404">
                <div class="col-12 col-m-6 col-l-7">
                    <img src="{{ASSETS}}/img/error.png" alt="error" class="block-lvl">
                </div>
                <div class="col-12 col-m-6 col-l-5">
                    <div class="content">
                        <span>{{ \App\Http\Controllers\HomeController::translateWord('Error Page Not Found')}}</span>
                        <h2>404</h2>
                        <p>{{ \App\Http\Controllers\HomeController::translateWord('The Page Your Looking for Does Not Exist.')}}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Page Content -->
@endsection