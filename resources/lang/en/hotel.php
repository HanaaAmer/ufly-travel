<?php
return [
     //hotels
    'hotels'=>'Hotels',
    'add_hotel'=>'Add Hotel',
    'edit_hotel'=>'Edit Hotel',
    'deleteHotel'=>'Delete Hotel',
    'all_hotels'=>'All Hotels',
    'name'=>'Name',
    'price'=>'Price',
    'duration_day'=>'Days',
    'duration_night'=>'Nights',
    'image'=>'Image',
      'short_description'=>'Short Description',
    'long_description'=>'Long Description',
    'city'=>'City',
    'images'=>'Images',
    'add_hotel_images'=>'Add Hotel Images',
    'services'=>'Services',
    'availability'=>'Availability ',
    'adults'=>' Adults',
    'childern'=>'Childern',
    'Amenities'=>'Amenities',
    'capacity'=>'Capacity',
     //Review
    'fullname'=>'Full Name ',
    'review'=>'Review',
    'Accomodation'=>'Accomodation',
    'Meals'=>'Meals',
    'Value_For_Money'=>'Value For Money ',
    'Destination'=>'Destination',
    'Transport'=>'Transport',
     'category'=>'Category',
     'beds'=>'Beds',
 
];