
<?php

return [
    'About'=>'About Us',
	'title' => 'Title',
	'description' => 'Description',
	'vision_mission' => 'Vision And Mission',
    'image_description' => 'Image Of Description',
    'image_vision' => 'Image Of Vision',
    'image_goals' => 'Image Of Goals',
    'add_goal'=>'Add Goal',
    'all_goals'=>'All Goals',
    'goal'=>'Goal',
];