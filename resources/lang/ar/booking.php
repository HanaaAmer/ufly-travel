<?php
return [
    'fullname'=>'الاسم',
    'phone'=>'رقم التلفون',
    'email'=>'البريد الالكترونى',
    'AdultNumber'=>'عدد الكبار',
    'note'=>'الملاحظات',
    'all_booking'=>'كل الحجوزات',
    'ChildernsNumber'=>'عدد الاطفال',
    'InfantNumber'=>'عدد الرضع',
    'TravelersNumber'=>' عدد المسافرين ',
     'NumberofDays'=>'عدد الأيام',
      'ArrivalDate'=>'تاريخ الوصول',

];
