
<?php

return [
    'About'=>'عن الشركه',
	'title' => 'اسم الشركه',
	'description' => 'وصف الشركه',
	'vision_mission' => 'الرؤية والرسالة',
    'image_description' => 'صورة الوصف',
    'image_vision' => 'صورة الرؤيه',
    'image_goals' => 'صورة الاهداف',
    'add_goal'=>'اضافة هدف جديد',
    'all_goals'=>'أهداف الشركة',
    'goal'=>'هدف',
	
];