<?php
return [
     //tours
    'tours'=>'الرحلات',
    'add_tour'=>'إضافة رحله جديده',
    'edit_tour'=>'تعديل الرحله',
    'deleteTour'=>'حذف الرحله',
    'all_tours'=>'كل الرحلات',
    'name'=>'اسم الرحله',
    'price'=>'السعر',
    'duration'=>'المده',
    'image'=>'صورةالرحله ',
    'short_description'=>'الوصف القصير',
    'long_description'=>'الوصف الطويل',
    'description'=>'الوصف',
    'start_date'=>'تبدأ من',
    'end_date'=>'تنتهي في',
    'hotel'=>'الفندق',
    'city'=>'المدينه',
    'category'=>'الفئه',
    'duration_day'=>'عدد الايام',
    'duration_night'=>'عدد الليالي',
    'availability'=>'العدد المتاح',
    'adults'=>'عدد البالغين ',
    'childern'=>' عدد الاطفال',
    'included'=>'تشتمل علي',
    'not_included'=>'لا تشتمل',
    'images'=>'صور الرحله',
    'add_tour_images'=>'إضافه صور رحله ',
    'title'=>'العنوان',
    'image_description'=>'صوره التفاصيل',
    'add_detail'=>'إضافه تفاصيل جديده',
    'all_detail'=>'كل التفاصيل',
    'edit_tour_detail'=>'تعديل تفاصيل الرحله',
    'active'=>'مفعل',
     'offer'=>'العروض',
    //Review
    'fullname'=>'الاسم كامل',
    'review'=>'الرأي',
    'Accomodation'=>'الاقامه',
    'Meals'=>'الوجبات',
    'Value_For_Money'=>'قيمه االمال',
    'Destination'=>'الموقع',
    'Transport'=>'المواصلات',
    'beds'=>'عدد السراير',

 
];