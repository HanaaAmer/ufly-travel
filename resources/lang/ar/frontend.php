<?php
return [
    'HOMEPAGE' => 'الصفحه الرئيسيه',
     'TRAVELTOURS' => 'الرحلات السياحية',
     'HOTELS'=>'الفنادق',
     'TOURISTCITYS'=>'المدن السياحية',
     'SERVICES'=>'الخدمات',
     'ABOUTUS'=>'عن الشركة',
     'CONTACT US'=>'الاتصال بنا',
     'TOURISMPLACES'=>'الاماكن السياحية',
     'TOURISMPLACES'=>'الاماكن السياحية',
     'SIMILIARCITIES'=>' المدن المشابهة',
     'VISION&MISSION' => 'الرؤية و الرسالة',
     'GOALS' => 'الأهداف',
     'THECITY'=>'المدينة',
     'READABOUTMORE'=>'اقرأ عنا أكثر',
     'READMORE'=>'اقرأ أكثر',
];
